# Site Report

#### Package description: Package statistic for 123job sites 

## Installation

Install via composer
```bash
composer require workable/site-report
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Workable\SiteReport\SiteReportServiceProvider" --tag="config"
```

## Usage

#### Step 1:<br> - Thêm file credential.json trong thư mục root của package  vào thư mục public của dự án
#### Step 2:<br> - Tiến hành thêm các site cần lấy dữ liệu vào file config/api.php
#### Step 3:<br> - Thêm command: `worker:get-data-traffic` vào kernel.php của dự án và set thời gian chạy 1 ngày / 1 lần
#### Step 4:<br> - Tiến hành chạy npm: ` npm run packages-dev --name=site-report` 





