<?php

namespace Workable\SiteReport\Http\Controllers\Traffic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Workable\ManagerSite\Enum\SiteStatusEnum;
use Workable\ManagerSite\Services\ManagerSiteService;
use Workable\Support\Traits\FilterBuilderTrait;
use Workable\Support\Traits\FlashMessages;
use Workable\Support\Traits\ResponseHelperTrait;

class TrafficHomeController extends Controller
{
    use ResponseHelperTrait;
    use FlashMessages;
    use FilterBuilderTrait;

    protected $siteService;

    public function __construct(
        ManagerSiteService   $siteService
    )
    {
        $this->siteService = $siteService;
    }

    public function index(Request $request)
    {
        $this->setFilter($request, 'site_name', 'LIKE');
        $this->setFilter($request, 'continent', 'LIKE');

        $filter = $this->getFilter();
        $sites = $this->siteService->getSiteActive($filter);

        $viewData = [
            'sites' => $sites,
            'continents' => SiteStatusEnum::$continents,
            'page' => 'traffic_home',
            'query' => $request->all()
        ];

        return view('packages.site-report::pages.traffic.traffic_home.index')->with($viewData);
    }

}

