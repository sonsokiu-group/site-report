<?php

namespace Workable\SiteReport\Http\Controllers\Traffic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Workable\ManagerSite\Services\ManagerSiteService;
use Workable\SiteReport\Enum\ReportTrafficTypeEnum;
use Workable\SiteReport\Services\ReportTrafficService;
use Workable\SiteReport\Utils\ReportTrafficTransformData;
use Workable\Support\Traits\FilterBuilderTrait;
use Workable\Support\Traits\FlashMessages;
use Workable\Support\Traits\ResponseHelperTrait;

class TrafficRedirectController extends Controller
{
    use ResponseHelperTrait;
    use FlashMessages;
    use FilterBuilderTrait;

    protected $siteService;
    protected $reportTrafficService;
    protected $reportTrafficUtil;

    public function __construct(
        ManagerSiteService   $siteService,
        ReportTrafficService $reportTrafficService
    )
    {
        $this->siteService = $siteService;
        $this->reportTrafficService = $reportTrafficService;
        $this->reportTrafficUtil = new ReportTrafficTransformData();
    }

    public function index(Request $request, $id)
    {
        $site = $this->siteService->findById($id);
        $sites = $this->siteService->getSiteActive([], false);
        $dataTableGroup = $this->reportTrafficService
            ->getDataTableRedirect($id, ReportTrafficTypeEnum::CHART_GROUP_REDIRECT);
        $dataTableDetail = $this->reportTrafficService
            ->getDataTableRedirect($id, null);

        $listChartGroupRedirect = [
            [
                "chartName" => "Chính",
                "chartType" => ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_MAIN
            ],
            [
                "chartName" => "keyword",
                "chartType" => ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_KEYWORD
            ],
            [
                "chartName" => "category",
                "chartType" => ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_CATEGORY
            ],
            [
                "chartName" => "remote",
                "chartType" => ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_REMOTE
            ]
        ];

        $viewData = [
            "sites" => $sites,
            "site" => $site,
            "tabMain" => "redirect",
            'query' => $request->all(),
            "dataTableGroup" => $dataTableGroup,
            "dataTableDetail" => $dataTableDetail,
            "listChartGroupRedirect" => $listChartGroupRedirect
        ];

        return view('packages.site-report::pages.traffic.traffic_detail_redirect.index')->with($viewData);
    }
}
