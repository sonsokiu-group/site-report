<?php

namespace Workable\SiteReport\Http\Controllers\Traffic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Workable\ManagerSite\Services\ManagerSiteService;
use Workable\SiteReport\Enum\ReportTrafficTypeEnum;
use Workable\SiteReport\Services\ReportTrafficService;
use Workable\Support\Traits\FilterBuilderTrait;
use Workable\Support\Traits\FlashMessages;
use Workable\Support\Traits\ResponseHelperTrait;

class TrafficOverviewController extends Controller
{
    use ResponseHelperTrait;
    use FlashMessages;
    use FilterBuilderTrait;

    protected $siteService;
    protected $reportTrafficService;

    public function __construct(
        ManagerSiteService   $siteService,
        ReportTrafficService $reportTrafficService
    )
    {
        $this->siteService          = $siteService;
        $this->reportTrafficService = $reportTrafficService;
    }

    public function index(Request $request, $id)
    {
        $site        = $this->siteService->findById($id);
        $sites       = $this->siteService->getSiteActive([], false);
        $dataTable   = $this->reportTrafficService->getDataTableOverview($request, $site->id);
        $overViewTab = ReportTrafficTypeEnum::CONFIG_KEY_CHART_OVERVIEW;

        $listChart = [
            [
                "routeDetail" => "get.traffic_redirect.index",
                "chartName"   => "Traffic theo nhóm trang",
                "chartType"   => ReportTrafficTypeEnum::CHART_GROUP_REDIRECT
            ],
            [
                "routeDetail" => "get.traffic_referer.index",
                "chartName"   => "Traffic theo nguồn referer",
                "chartType"   => ReportTrafficTypeEnum::CHART_GROUP_REFERER
            ]
        ];

        $viewData = [
            "site"        => $site,
            "dataTable"   => $dataTable,
            "tabMain"     => "overview",
            'query'       => $request->all(),
            'overViewTab' => $overViewTab,
            'sites'       => $sites,
            "listChart"   => $listChart
        ];

        return view('packages.site-report::pages.traffic.traffic_detail_overview.index')->with($viewData);
    }
}
