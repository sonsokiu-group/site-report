<?php

namespace Workable\SiteReport\Http\Controllers\Traffic;

use Illuminate\Http\Request;
use Workable\ManagerSite\Services\ManagerSiteService;
use Workable\SiteReport\Enum\ReportTrafficTypeEnum;
use Workable\SiteReport\Services\ReportTrafficService;
use Workable\Support\Traits\FilterBuilderTrait;
use Workable\Support\Traits\FlashMessages;
use Workable\Support\Traits\ResponseHelperTrait;

class TrafficRefererController
{
    use ResponseHelperTrait;
    use FlashMessages;
    use FilterBuilderTrait;

    protected $siteService;
    protected $reportTrafficService;

    public function __construct(
        ManagerSiteService $siteService,
        ReportTrafficService $reportTrafficService
    )
    {
        $this->siteService = $siteService;
        $this->reportTrafficService = $reportTrafficService;
    }

    public function index(Request $request, $id)
    {
        $site = $this->siteService->findById($id);
        $sites = $this->siteService->getSiteActive([], false);

        $viewData = [
            "site" => $site,
            "sites" => $sites,
            "tabMain" => "referer",
            "query" => $request->all(),
            "dataTableGroup" => $this->reportTrafficService->getDataTableReferer($request, $id, ReportTrafficTypeEnum::CHART_GROUP_REDIRECT),
            "dataTableDetail" => $this->reportTrafficService->getDataTableReferer($request, $id, null)
        ];

        return view('packages.site-report::pages.traffic.traffic_detail_referer.index')->with($viewData);
    }

}
