<?php

namespace Workable\SiteReport\Http\Controllers\Request;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Workable\ManagerSite\Services\ManagerSiteService;
use Workable\RequestReport\Services\RobotRequestReportService;

class RobotRequestController extends Controller
{
    protected $siteService;
    protected $robotRequestReportService;

    public function __construct(ManagerSiteService $siteService, RobotRequestReportService $robotRequestReportService)
    {
        $this->siteService = $siteService;
        $this->robotRequestReportService = $robotRequestReportService;
    }

    public function request(Request $request, $id)
    {
        $site = $this->siteService->findById($id);

        if (!$site) {
            abort(404);
        }
        $sites = $this->siteService->getSiteActive([], false);

        $dataSearch["dayAgoTable"] = $request->day_ago_table ?? 7;
        $dataSearch["type"]        = $request->type ?? "fast";
        $dataSearch["domain"]      = $site->site_url;

        $datas = $this->robotRequestReportService->getDataTable($dataSearch);

        $viewData = [
            "site"    => $site,
            "datas"   => $datas,
            'query'   => $request->all(),
            'sites'   => $sites,
            "tabMain" => "request",
        ];

        return view('packages.site-report::pages.request.site_report_request')->with($viewData);
    }
}
