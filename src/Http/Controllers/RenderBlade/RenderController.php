<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 3/6/24
 * Time: 17:24
 */

namespace Workable\SiteReport\Http\Controllers\RenderBlade;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Workable\ManagerSite\Services\ManagerSiteService;
use Workable\SiteReport\Core\QueryChartData\ChartQueryData;
use Workable\SiteReport\Utils\ReportTimeUtils;
use Workable\Support\Traits\FilterBuilderTrait;
use Workable\Support\Traits\FlashMessages;
use Workable\Support\Traits\ResponseHelperTrait;

class RenderController extends Controller
{
    use ResponseHelperTrait;
    use FlashMessages;
    use FilterBuilderTrait;

    protected $siteService, $reportTimeUtil;

    public function __construct(
        ManagerSiteService $siteService,
        ReportTimeUtils    $reportTimeUtil
    )
    {
        $this->siteService    = $siteService;
        $this->reportTimeUtil = $reportTimeUtil;
    }

    public function apiDataChart(Request $request)
    {
        $time    = $request->get('day') ?? 7;
        $service = $request->get('nameService');
        $site    = $request->get('site_id');
        $arrayQuery =  $request->get('query');
        if($site != 0){
            $arrayQuery['filter'][0][2] = $site;
        }
        list($class, $method) = explode('@', $service);
        $paramQueryChart = new ChartQueryData($arrayQuery);

        $app             = app($class);
        $data            = $app->setParamQueryChart($paramQueryChart)->$method($time);

        return $this->respondSuccess('success', $data);
    }

}
