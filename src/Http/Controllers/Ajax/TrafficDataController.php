<?php

namespace Workable\SiteReport\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use Workable\SiteReport\Enum\ReportTrafficTypeEnum;
use Workable\SiteReport\Services\ReportTrafficService;
use Workable\Support\Traits\FilterBuilderTrait;
use Workable\Support\Traits\FlashMessages;
use Workable\Support\Traits\ResponseHelperTrait;

class TrafficDataController
{
    use ResponseHelperTrait;
    use FlashMessages;
    use FilterBuilderTrait;

    protected $reportTrafficService;

    public function __construct(
        ReportTrafficService $reportTrafficService
    )
    {
        $this->reportTrafficService = $reportTrafficService;
    }

    public function getDataLineChart(Request $request)
    {
        $dataChart = $this->reportTrafficService->getDataChart($request, "line");

        return $this->respondSuccess("success", $dataChart);
    }

    public function getDataPieChart(Request $request)
    {
        $referer = $request->get('referer') ?? null;
        $filter = [];

        if ($referer)
        {
            $filter[] = ["referer", "=", $referer];
        }

        $dataChart = $this->reportTrafficService
            ->getDataChart($request, "pie", $filter);

        return $this->respondSuccess('success', $dataChart, 'success');
    }

    public function getDataBarChart(Request $request)
    {
        $filter = [];

        $dataChart = $this->reportTrafficService
            ->getDataChart($request, "bar", $filter);

        return $this->respondSuccess('success', $dataChart, 'success');
    }

    public function getDataChartAllSite(Request $request)
    {
        $dataRtn = $this->reportTrafficService
            ->getDataChartAllSite($request);

        return $this->respondSuccess('success', $dataRtn);
    }

    public function getDataChartOverview(Request $request)
    {
        $dataChart = $this->reportTrafficService
            ->getDataChartOverView($request);

        return $this->respondSuccess('success', $dataChart);
    }

    public function getDataChartDetailRedirect(Request $request)
    {
        $dataChart = $this->reportTrafficService
            ->getDataChartDetailRedirect($request);

        return $this->respondSuccess('success', $dataChart);
    }

    public function getDataChartDetailReferer(Request $request)
    {
        $dataChart = $this->reportTrafficService
            ->getDataChartDetailReferer($request);

        return $this->respondSuccess('success', $dataChart);
    }
}
