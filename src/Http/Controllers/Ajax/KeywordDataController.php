<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 3/12/24
 * Time: 11:03
 */

namespace Workable\SiteReport\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Workable\SiteReport\Services\ReportKeywordService;
use Workable\Support\Traits\ResponseHelperTrait;

class KeywordDataController extends Controller
{
    use ResponseHelperTrait;

    protected $reportKeywordService;

    public function __construct(
        ReportKeywordService $reportKeywordService
    )
    {
        $this->reportKeywordService = $reportKeywordService;
    }

    public function getDataChartKeywordLevel(Request $request)
    {
        $dataRtn = $this->reportKeywordService->chartKeywordLevel($request);

        return $this->respondSuccess("Success", $dataRtn);
    }

    public function chartKeywordPublicHome(Request $request)
    {
        $dataRtn = $this->reportKeywordService->chartKeywordPublicHome($request);

        return $this->respondSuccess("Success", $dataRtn);
    }

    public function chartLocationExtract(Request $request)
    {
        $dataRtn = $this->reportKeywordService->chartLocationExtract($request);

        return $this->respondSuccess("Success", $dataRtn);
    }

    public function chartOverviewExtract(Request $request)
    {
        $dataRtn = $this->reportKeywordService->chartOverviewExtract($request);

        return $this->respondSuccess("Success", $dataRtn);
    }

}
