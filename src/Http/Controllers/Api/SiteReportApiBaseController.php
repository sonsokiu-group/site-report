<?php
namespace Workable\SiteReport\Http\Controllers\Api;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class SiteReportApiBaseController extends Controller
{
    public function index()
    {
        return 'Welcome to controller api: '. __("packages.site-report::app.name");
    }
}
