<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 3/6/24
 * Time: 14:19
 */

namespace Workable\SiteReport\Http\Controllers\Keyword;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Workable\ManagerSite\Enum\SiteStatusEnum;
use Workable\ManagerSite\Services\ManagerSiteService;
use Workable\SiteReport\Services\ReportKeywordService;
use Workable\Support\Traits\FilterBuilderTrait;
use Workable\Support\Traits\FlashMessages;

class LocationPublicController extends Controller
{
    use FlashMessages;
    use FilterBuilderTrait;

    protected $siteService,$reportKeywordService,$reportKeywordUtil;

    public function __construct(
        ManagerSiteService   $siteService,
        ReportKeywordService $reportKeywordService
    )
    {
        $this->siteService = $siteService;
        $this->reportKeywordService = $reportKeywordService;
    }

    public function index(Request $request)
    {
        $this->setFilter($request, 'site_name', 'LIKE');
        $this->setFilter($request, 'continent', 'LIKE');

        $filter = $this->getFilter();
        $sites = $this->siteService->getSiteActive($filter);
        $arraySites = $sites->pluck("site_name", "id")->toArray();
        $dataTableReport = $this->reportKeywordService->dataTableLocationPublicHome($arraySites);

        $viewData = [
            'sites' => $sites,
            'continents' => SiteStatusEnum::$continents,
            'query' => $request->all(),
            'dataTableReport' => $dataTableReport
        ];

        return view('packages.site-report::pages.keyword.location_public_home.index')->with($viewData);
    }

    public function detail(Request $request, $id)
    {
        $site = $this->siteService->findById($id);
        $sites = $this->siteService->getSiteActive([], false);

        $arraySites = $site->where("id", $id)->pluck("site_name", "id")->toArray();
        $dataTableReport = $this->reportKeywordService->dataTableLocationPublicHome($arraySites);

        $viewData = [
            "site" => $site,
            "sites" => $sites,
            "tabMain" => "data-public",
            "tabSub" => "location-public",
            'query' => $request->all(),
            'dataTableReport' => $dataTableReport
        ];

        return view('packages.site-report::pages.keyword.location_public_detail.index')->with($viewData);
    }

}
