<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 3/6/24
 * Time: 17:24
 */

namespace Workable\SiteReport\Http\Controllers\Keyword;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Workable\ManagerSite\Enum\SiteStatusEnum;
use Workable\ManagerSite\Services\ManagerSiteService;
use Workable\SiteReport\Services\ReportKeywordService;
use Workable\Support\Traits\FilterBuilderTrait;
use Workable\Support\Traits\FlashMessages;

class KeywordExtractController extends Controller
{
    use FlashMessages;
    use FilterBuilderTrait;

    protected $siteService;
    protected $reportKeywordService;
    protected $reportKeywordUtil;

    public function __construct(
        ManagerSiteService   $siteService,
        ReportKeywordService $reportKeywordService
    )
    {
        $this->siteService          = $siteService;
        $this->reportKeywordService = $reportKeywordService;
    }

    public function index(Request $request)
    {

        $this->setFilter($request, 'site_name', 'LIKE');
        $this->setFilter($request, 'continent', 'LIKE');

        $filter = $this->getFilter();
        $sites  = $this->siteService->getSiteActive($filter);

        $viewData = [
            'query'       => $request->all(),
            'continents'  => SiteStatusEnum::$continents,
            'sites'       => $sites,
            "tabMain"     => __FUNCTION__,
            "isFilterDay" => true
        ];

        return view('packages.site-report::pages.keyword.kw_extract_home.index')->with($viewData);
    }

    public function keyword(Request $request)
    {
        $this->setFilter($request, 'site_name', 'LIKE');
        $this->setFilter($request, 'continent', 'LIKE');

        $filter          = $this->getFilter();
        $sites           = $this->siteService->getSiteActive($filter);
        $arraySites      = $sites->pluck("site_name", "id")->toArray();
        $dayReport       = $request->get("day", 7);
        $sort            = $request->get("sort", null);
        $dataReportTable = $this->reportKeywordService->dataTableKwExtractHome($arraySites, $dayReport, $sort);

        $viewData = [
            'query'           => $request->all(),
            'continents'      => SiteStatusEnum::$continents,
            'sites'           => $sites,
            "tabMain"         => __FUNCTION__,
            "isFilterDay"     => true,
            "isFilterSort"    => true,
            "dataReportTable" => $dataReportTable
        ];

        return view('packages.site-report::pages.keyword.kw_extract_home.home_keyword')->with($viewData);
    }

    public function location(Request $request)
    {
        $this->setFilter($request, 'site_name', 'LIKE');
        $this->setFilter($request, 'continent', 'LIKE');

        $filter = $this->getFilter();
        $sites  = $this->siteService->getSiteActive($filter);
        $tabSub = $request->get("tabSub", "table-month");
        $dayReport = $request->get("day", 7);
        $request->merge(["day" => $dayReport]);
        $dataTableReport = ($tabSub !== "chart") ? $this->reportKeywordService->dataTableLocationExtract($request, $sites) : [];

        $viewData = [
            'query'           => $request->all(),
            'continents'      => SiteStatusEnum::$continents,
            'sites'           => $sites,
            "tabMain"         => __FUNCTION__,
            "tabSub"          => $tabSub,
            "dataTableReport" => $dataTableReport,
            "isFilterDay"     => true
        ];

        return view('packages.site-report::pages.keyword.kw_extract_home.home_location')->with($viewData);
    }

    public function overviewDetail(Request $request, $id)
    {
        $site  = $this->siteService->findById($id);
        $sites = $this->siteService->getSiteActive([], false);

        $request->merge(["site_id" => $site->id]);
        $dataTableReport = $this->reportKeywordService->chartOverviewExtract($request);

        $viewData = [
            "query"           => $request->all(),
            "sites"           => $sites,
            "site"            => $site,
            "dataTableReport" => $dataTableReport,
            "tabMain"         => "data-extract",
            "tabSub"          => "overview-extract"
        ];

        return view('packages.site-report::pages.keyword.kw_extract_detail.overview')->with($viewData);
    }

    public function keywordDetail(Request $request, $id)
    {
        $site  = $this->siteService->findById($id);
        $sites = $this->siteService->getSiteActive([], false);
        $request->merge(["site_id" => $site->id]);
        $dataTableReport = $this->reportKeywordService->chartOverviewExtract($request);

        $viewData = [
            "site"            => $site,
            'query'           => $request->all(),
            'sites'           => $sites,
            "dataTableReport" => $dataTableReport,
            "tabMain"         => "data-extract",
            "tabSub"          => "keyword-extract"
        ];

        return view('packages.site-report::pages.keyword.kw_extract_detail.keyword_detail')->with($viewData);
    }

    public function locationDetail(Request $request, $id)
    {
        $site            = $this->siteService->findById($id);
        $sites           = $this->siteService->getSiteActive([], false);
        $dataTableReport = $this->reportKeywordService->dataTableLocationExtract($request, [$site]);

        $viewData = [
            "site"            => $site,
            'query'           => $request->all(),
            'sites'           => $sites,
            "tabMain"         => "data-extract",
            "tabSub"          => "location-extract",
            "dataTableReport" => $dataTableReport
        ];

        return view('packages.site-report::pages.keyword.kw_extract_detail.location_detail')->with($viewData);
    }

}
