<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 3/6/24
 * Time: 11:35
 */

namespace Workable\SiteReport\Http\Controllers\Keyword;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Workable\ManagerSite\Enum\SiteStatusEnum;
use Workable\ManagerSite\Services\ManagerSiteService;
use Workable\SiteReport\Services\ReportKeywordService;
use Workable\Support\Traits\FilterBuilderTrait;

class KeywordLevelController extends Controller
{
    use FilterBuilderTrait;

    protected $siteService;
    protected $reportKeywordService;

    public function __construct(
        ManagerSiteService   $siteService,
        ReportKeywordService $reportKeywordService
    )
    {
        $this->siteService          = $siteService;
        $this->reportKeywordService = $reportKeywordService;
    }

    public function index(Request $request)
    {
        $this->setFilter($request, 'site_name', 'LIKE');
        $this->setFilter($request, 'continent', 'LIKE');

        $filter          = $this->getFilter();
        $sites           = $this->siteService->getSiteActive($filter);
        $tab             = $request->get("tab", "chart");
        $dataTableReport = ($tab == "table") ?
            $this->reportKeywordService->dataTableKeywordLevelHome($sites) : [];

        $viewData = [
            'sites'           => $sites,
            'continents'      => SiteStatusEnum::$continents,
            'tab'             => $tab,
            'query'           => $request->all(),
            'dataTableReport' => $dataTableReport
        ];

        return view('packages.site-report::pages.keyword.kw_level_home.index')->with($viewData);
    }

    public function detail(Request $request, $id)
    {
        $site  = $this->siteService->findById($id);
        $sites = $this->siteService->getSiteActive([], false);

        $viewData = [
            "site"    => $site,
            "sites"   => $sites,
            "tabMain" => "data-public",
            "tabSub"  => "keyword-level",
            'query'   => $request->all(),
        ];

        return view('packages.site-report::pages.keyword.kw_level_detail.index')->with($viewData);
    }

}