<?php
namespace Workable\SiteReport;

use Illuminate\Support\ServiceProvider;
use Workable\Base\Supports\Helper;
use Workable\Base\Traits\LoadAndPublishDataTrait;
use Workable\SiteReport\Command\WorkerCrawlDataKeywordCommand;
use Workable\SiteReport\Command\WorkerGetDataTraffic;


class SiteReportServiceProvider extends ServiceProvider
{
    use LoadAndPublishDataTrait;

    public function boot()
    {
        Helper::autoload(__DIR__ . '/../helper');
        $this->setNamespace('packages/site-report')
            ->loadAndPublishConfigurations(['config', 'api', 'spider_keyword'])
            ->loadAndPublishViews()
            ->loadMigrations()
            ->loadRoutes(['web', 'api']);
    }
    public function registerRepository()
    {
         $models             = [
            "NameModel"
        ];
        $namespace = "Workable\\SiteReport\\Repository\\";
        foreach ($models as $model)
        {
            $this->app->singleton(
                $namespace .$model . "\\" . $model . 'RepositoryInterface',
                $namespace .$model . "\\" . $model . 'Repository'
            );
        }
    }

    public function register()
    {
        $this->registerRepository();
        $this->registerWorker();
        $this->app->bind('site-report', function () {
            return new SiteReport();
        });
    }

    protected function registerWorker()
    {
        $this->commands([
            WorkerGetDataTraffic::class,
            WorkerCrawlDataKeywordCommand::class
        ]);
    }
}
