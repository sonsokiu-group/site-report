<?php

namespace Workable\SiteReport\Services;

use Illuminate\Support\Facades\DB;
use Workable\SiteReport\Enum\ReportTrafficTypeEnum;
use Workable\SiteReport\Utils\ReportHelperUtil;
use Workable\SiteReport\Utils\ReportTimeUtils;
use Workable\SiteReport\Utils\ReportTrafficDataTable;
use Workable\SiteReport\Utils\ReportTrafficTransformData;
use Workable\Support\Traits\ScopeRepositoryTrait;

class ReportTrafficService
{
    use ScopeRepositoryTrait;

    protected $reportTimeUtil;
    protected $reportTrafficTransformData;
    protected $reportHelperUtil;
    protected $reportTrafficDataTable;

    public function __construct()
    {
        $this->reportTimeUtil = new ReportTimeUtils();
        $this->reportTrafficTransformData = new ReportTrafficTransformData();
        $this->reportHelperUtil = new ReportHelperUtil();
        $this->reportTrafficDataTable = new ReportTrafficDataTable();
    }

    public function getRawData($request, $type = null, $filter = []): array
    {
        $site_id = $request->get('site_id');
        $dayReport = $request->get('day') ?? 30;
        $configChart = $this->__getConfigChart($type);
        $tableConfig = $configChart["table"];
        $columnsConfig = $configChart["columns"];

        // Get dataCurrent ( by $dayReport )
        list($monthQuery, $dates, $dayAgo) = $this->reportTimeUtil->getDayAgo($dayReport);
        $dataOriginal = $this->__getDataBySiteId($site_id, $tableConfig, $monthQuery, $filter);

        // Get dataCompare ( 7 ngày || 14 ngày || 30 ngày )
        list($monthQueryBefore, $dateBefore, $dayAgoBefore) = $this->reportTimeUtil->getDayAgo($dayReport, true);
        $dataOriginalBefore = $this->__getDataBySiteId($site_id, $tableConfig, $monthQueryBefore, $filter);

        return [
            "dates" => array_reverse($dates),
            "dataOriginal" => $dataOriginal,
            "dateBefore" => array_reverse($dateBefore),
            "dataOriginalBefore" => $dataOriginalBefore,
            "columns" => $columnsConfig,
            "type" => $type
        ];
    }

    // Use for line chart, pie chart, bar chart,...
    public function getDataChart($request, $chart, $filter = []): array
    {
        $typeChart = (int)$request->get("type");
        $rawData = $this->getRawData($request, $typeChart, $filter);

        switch ($chart)
        {
            case "line":
                $dataChartCurrent = $this->reportTrafficTransformData
                    ->transformDataLineChart($rawData["dataOriginal"], $rawData["dates"], $rawData["columns"], $rawData["type"]);
                $dataChartBefore = $this->reportTrafficTransformData
                    ->transformDataLineChart($rawData["dataOriginalBefore"], $rawData["dateBefore"], $rawData["columns"], $rawData["type"]);
                break;

            case "pie":
                $dataChartCurrent = $this->reportTrafficTransformData
                    ->transformDataPieChart($rawData["dataOriginal"], $rawData["dates"], $rawData["columns"], $rawData["type"]);
                $dataChartBefore = $this->reportTrafficTransformData
                    ->transformDataPieChart($rawData["dataOriginalBefore"], $rawData["dateBefore"], $rawData["columns"], $rawData["type"]);
                break;

            case "bar":
                $dataChartCurrent = $this->reportTrafficTransformData
                    ->transformDataBarChart($rawData["dataOriginal"], $rawData["dates"], $rawData["columns"], $rawData["type"]);
                $dataChartBefore = $this->reportTrafficTransformData
                    ->transformDataBarChart($rawData["dataOriginalBefore"], $rawData["dateBefore"], $rawData["columns"], $rawData["type"]);
                break;

            default:
                $dataChartCurrent = [];
                $dataChartBefore = [];
        }

        $dataCompareChart = $this->reportHelperUtil->compareDataChart($dataChartCurrent, $dataChartBefore);

        return [
            "dates" => $rawData["dates"],
            "dataChart" => $dataChartCurrent,
            "dateBefore" => $rawData["dateBefore"],
            "dataChartBefore" => $dataChartBefore,
            "dataCompare" => $dataCompareChart
        ];
    }

    public function getDataChartAllSite($request): array
    {
        $days = $request->get('day');
        $sites = (array)$request->get('sites');
        $siteIds = array_keys($sites);

        // Get dataCurrent ( by $dayReport )
        list($monthQuery, $dates, $dayAgo) = $this->reportTimeUtil->getDayAgo($days);
        $dataCurrent = $this->__getDataMultiSite($siteIds, $monthQuery);
        $trafficCurrent = $this->reportTrafficTransformData->transformDataChartAllSite($dataCurrent, $dates, $siteIds);

        // Get dataCompare ( 7 ngày || 14 ngày || 30 ngày )
        list($monthQueryBefore, $datesBefore, $dayAgo) = $this->reportTimeUtil->getDayAgo($days, true);
        $dataBefore = $this->__getDataMultiSite($siteIds, $monthQueryBefore);
        $trafficBefore = $this->reportTrafficTransformData->transformDataChartAllSite($dataBefore, $datesBefore, $siteIds);

        $trafficBefore = array_map(function ($value) {
            return -$value;
        }, $trafficBefore);

        return [
            "label" => array_values($sites),
            "dataChart" => array_values($trafficCurrent),
            "dataChartBefore" => array_values($trafficBefore),
            "dataSort" => $this->reportHelperUtil->sortByTrafficAll($trafficCurrent, $sites)
        ];
    }

    private function __getDataMultiSite($siteIds, $monthQuery): array
    {
        $query = DB::table('site_traffic_overview')
        ->whereIn("site_id", $siteIds);

        $query->where(function ($q) use ($monthQuery) {
            foreach ($monthQuery as $year => $monthArr) {
                $q->orWhere(function ($q) use ($year, $monthArr) {
                    $q->Where("year", $year)
                        ->whereIn("month", $monthArr);
                });
            }
        });

        return $query->get()->toArray();
    }

    public function getDataChartOverView($request): array
    {
        $typeChart = (int)$request->get("type");
        $rawData = $this->getRawData($request, $typeChart, []);

        $dataChartCurrent = $this->reportTrafficTransformData
            ->transformDataChartOverview($rawData["dataOriginal"], $rawData["dates"], $rawData["columns"], $rawData["type"]);
        $dataChartBefore = $this->reportTrafficTransformData
            ->transformDataChartOverview($rawData["dataOriginalBefore"], $rawData["dateBefore"], $rawData["columns"], $rawData["type"]);

        $dataCompareChart = $this->reportHelperUtil->compareDataChart($dataChartCurrent, $dataChartBefore);

        return [
            "dates" => $rawData["dates"],
            "dataChart" => $dataChartCurrent,
            "dateBefore" => $rawData["dateBefore"],
            "dataChartBefore" => $dataChartBefore,
            "dataCompare" => $dataCompareChart
        ];
    }

    public function getDataChartDetailRedirect($request): array
    {
        $redirect = $request->get('redirect');
        $typeChart = array_search($redirect, ReportTrafficTypeEnum::MAPPING_KEY_CHART_GROUP);

        if ($typeChart)
        {
            $request->merge(['type' => $typeChart]);

            return $this->getDataChartOverView($request);
        }

        $site_id = $request->get('site_id');
        $dayReport = $request->get('day') ?? 30;

        $tableConfig = "sites_traffic";
        $columnsConfig = [
            [
                "name" => $redirect,
            ]
        ];

        $filter = [
          ["redirect", "=", $redirect]
        ];

        // Get dataCurrent ( by $dayReport )
        list($monthQuery, $dates, $dayAgo) = $this->reportTimeUtil->getDayAgo($dayReport);
        $dates = array_reverse($dates);
        $dataOriginal = $this->__getDataBySiteId($site_id, $tableConfig, $monthQuery, $filter);
        $dataChartCurrent = $this->reportTrafficTransformData
            ->transformDataChartOverview($dataOriginal, $dates, $columnsConfig, $redirect);

        // Get dataCompare ( 7 ngày || 14 ngày || 30 ngày )
        list($monthQueryBefore, $dateBefore, $dayAgoBefore) = $this->reportTimeUtil->getDayAgo($dayReport, true);
        $dateBefore = array_reverse($dateBefore);
        $dataOriginalBefore = $this->__getDataBySiteId($site_id, $tableConfig, $monthQueryBefore, $filter);
        $dataChartBefore = $this->reportTrafficTransformData
            ->transformDataChartOverview($dataOriginalBefore, $dateBefore, $columnsConfig, $redirect);

        $dataCompareChart = $this->reportHelperUtil->compareDataChart($dataChartCurrent, $dataChartBefore);

        return [
            "dates" => $dates,
            "dataChart" => $dataChartCurrent,
            "dateBefore" => $dateBefore,
            "dataChartBefore" => $dataChartBefore,
            "dataCompare" => $dataCompareChart
        ];
    }

    public function getDataChartDetailReferer($request): array
    {
        $redirect = $request->get('redirect');
        $groupChart = array_search($redirect, ReportTrafficTypeEnum::MAPPING_KEY_CHART_GROUP);
        $rawData = $this->getRawData($request, $groupChart, []);

        $dataChartCurrent = $this->reportTrafficTransformData
            ->transformDataChartDetailReferer($rawData["dataOriginal"], $rawData["dates"], $rawData["columns"]);

        $dataChartBefore = $this->reportTrafficTransformData
            ->transformDataChartDetailReferer($rawData["dataOriginalBefore"], $rawData["dateBefore"], $rawData["columns"]);

        $dataCompareChart = $this->reportHelperUtil->compareDataChart($dataChartCurrent, $dataChartBefore);

        return [
            "dates" => $rawData["dates"],
            "dataChart" => $dataChartCurrent,
            "dateBefore" => $rawData["dateBefore"],
            "dataChartBefore" => $dataChartBefore,
            "dataCompare" => $dataCompareChart
        ];
    }

    public function getDataTableOverview($request, $site_id, $filter = []): array
    {
        $dayReport = $request->get('day') ?? 30;
        $tableConfig = "sites_traffic";

        list($monthQuery, $dates, $dayAgo) = $this->reportTimeUtil->getDayAgo($dayReport);
        $dataOriginal = $this->__getDataBySiteId($site_id, $tableConfig, $monthQuery, $filter);

        return $this->reportTrafficTransformData
            ->transformDataTableOverview($dataOriginal, $dates);
    }

    public function getDataTableRedirect($site_id, $type): array
    {
        list($currentPeriod, $previousPeriod, $timePeriod) = $this->reportTimeUtil->getArrayPeriod(3);

        $dataCurrentPeriod  = $this->__getDataStatistic($currentPeriod, $site_id, $type);
        $dataPreviousPeriod = $this->__getDataStatistic($previousPeriod, $site_id, $type);

        return [
            "timePeriod" => $timePeriod,
            "data"       => $this->reportTrafficDataTable->makeDataTableRedirect($dataCurrentPeriod, $dataPreviousPeriod)
        ];
    }

    public function getDataTableReferer($request, $site_id, $type = null): array
    {
        $dayReport = $request->get('day') ?? 30;
        $tableConfig = "sites_traffic";

        list($monthQuery, $dates, $dayAgo) = $this->reportTimeUtil->getDayAgo($dayReport);
        $dataOriginal = $this->__getDataBySiteId($site_id, $tableConfig, $monthQuery, []);
        $dataCurrent = $this->reportTrafficTransformData->transformDataTableReferer($dataOriginal,$dates, $type);

        list($monthQueryBefore, $dateBefore, $dayAgoBefore) = $this->reportTimeUtil->getDayAgo($dayReport, true);
        $dataOriginalBefore = $this->__getDataBySiteId($site_id, $tableConfig, $monthQueryBefore, []);
        $dataBefore = $this->reportTrafficTransformData->transformDataTableReferer($dataOriginalBefore,$dateBefore, $type);

        return $this->reportTrafficDataTable->makeDataTableReferer($dataCurrent, $dataBefore);
    }

    private function __getDataBySiteId($site_id, $table, $monthQuery, $filter): array
    {
        $query = DB::table($table)
            ->where('site_id', $site_id);

        if ($filter) {
            $query = $this->scopeFilter($query, $filter);
        }

        $query->where(function ($q) use ($monthQuery) {
            foreach ($monthQuery as $year => $monthArr) {
                $q->orWhere(function ($q) use ($year, $monthArr) {
                    $q->Where("year", $year)
                        ->whereIn("month", $monthArr);
                });
            }
        });

        return $query->get()->toArray();
    }

    private function __getConfigChart($typeChart): array
    {
        $configChart = ReportTrafficTypeEnum::CONFIG_TYPE_CHART;
        return $configChart[$typeChart];
    }

    private function __getDataStatistic($arrayTime, $site_id, $type): array
    {
        $dataRtn = [];

        foreach ($arrayTime as $month => $year) {
            $month = (int)$month;
            $dataOriginal = DB::table("sites_traffic")
                ->where("site_id", $site_id)
                ->where("year", $year)
                ->where("month", $month)
                ->get();

            $month = ($month < 10) ? ("0" . $month) : $month;
            $keyRtn = $month . "/" . $year;
            $dataRtn[$keyRtn] = $this->reportTrafficTransformData->transformDataTableRedirect($dataOriginal, $type);
        }

        return $dataRtn;
    }
}
