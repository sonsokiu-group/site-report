<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 3/13/24
 * Time: 10:15
 */

namespace Workable\SiteReport\Services;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Workable\SiteReport\Enum\ReportKeywordEnum;
use Workable\SiteReport\Utils\ReportHelperUtil;
use Workable\SiteReport\Utils\ReportKeywordTransformData;
use Workable\SiteReport\Utils\ReportTimeUtils;
use Workable\Support\Traits\ScopeRepositoryTrait;

class ReportKeywordService
{
    use ScopeRepositoryTrait;

    protected $reportTimeUtil;
    protected $reportHelperUtil;
    protected $reportKeywordTransformData;

    public function __construct()
    {
        $this->reportTimeUtil             = new ReportTimeUtils();
        $this->reportKeywordTransformData = new ReportKeywordTransformData();
        $this->reportHelperUtil           = new ReportHelperUtil();
    }

    public function chartKeywordLevel(Request $request): array
    {
        $siteId       = $request->get("site_id");
        $filter       = [
            ["type", "=", ReportKeywordEnum::TYPE_KEYWORD_LEVEL]
        ];
        $dataOriginal = $this->__getDataBySiteId($siteId, [], $filter);
        $dataChart    = $this->reportKeywordTransformData->chartKeywordLevel($dataOriginal);

        return [
            "dataChart" => $dataChart,
            "dataCompare" => $this->reportHelperUtil->compareDataChart($dataChart, [])
        ];
    }

    public function dataTableKeywordLevelHome($sites): array
    {
        $arraySites = $sites->pluck("site_name", "id")->toArray();
        $siteIds    = array_keys($arraySites);

        $items = DB::table("keyword_statistics")
            ->whereIn("site_id", $siteIds)
            ->where("type", ReportKeywordEnum::TYPE_KEYWORD_LEVEL)
            ->get();

        return $this->reportKeywordTransformData->tableKeywordLevel($items, $arraySites);
    }

    public function chartKeywordPublicHome(Request $request): array
    {
        $siteId    = $request->get('site_id');
        $dayReport = $request->get('day', 30);
        $filter    = [
            ["type", "=", ReportKeywordEnum::TYPE_KEYWORD_PUBLIC]
        ];
        $columns   = ReportKeywordEnum::KEYWORD_PUBLIC_KEY_TYPE;

        list($monthQuery, $dates, $dayAgo) = $this->reportTimeUtil->getDayAgo($dayReport);
        $data      = $this->__getDataBySiteId($siteId, $monthQuery, $filter);
        $dates     = array_reverse($dates);
        $dataChart = $this->reportKeywordTransformData->chartKeywordPublicHome($data, $dates, $columns);

        list($monthQueryBefore, $dateBefore, $dayAgoBefore) = $this->reportTimeUtil->getDayAgo($dayReport, true);
        $dataBefore      = $this->__getDataBySiteId($siteId, $monthQueryBefore, $filter);
        $dateBefore      = array_reverse($dateBefore);
        $dataChartBefore = $this->reportKeywordTransformData->chartKeywordPublicHome($dataBefore, $dateBefore, $columns);

        $dataCompare = $this->reportHelperUtil->compareDataChart($dataChart, $dataChartBefore);

        return [
            "dates"           => $dates,
            "dataChart"       => $dataChart,
            "dateBefore"      => $dateBefore,
            "dataChartBefore" => $dataChartBefore,
            "dataCompare"     => $dataCompare
        ];
    }

    public function dataTableKeywordPublicHome($request, $sites): array
    {
        $dayReport = $request->get("day", 7);
        list($monthQuery, $dates, $dayAgo) = $this->reportTimeUtil->getDayAgo($dayReport);
        $arraySites = $sites->pluck("site_name", "id")->toArray();
        $siteIds    = array_keys($arraySites);

        $items = DB::table("keyword_statistics")
            ->whereIn("site_id", $siteIds)
            ->where("type", ReportKeywordEnum::TYPE_KEYWORD_PUBLIC)
            ->get();

        return $this->reportKeywordTransformData->tableKeywordPublicHome($items, $dates, $arraySites);
    }

    public function dataTableKeywordPublicDetail($siteId): array
    {
        $yearCurrent = Carbon::now()->year;
        $filter      = [
            ["year", "=", $yearCurrent],
            ["type", "=", ReportKeywordEnum::TYPE_KEYWORD_PUBLIC]
        ];
        $items       = $this->__getDataBySiteId($siteId, [], $filter);
        $dataRtn     = [];

        foreach ($items as $item)
        {
            $month         = $item->month;
            $dayMaxInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $yearCurrent);

            for ($day = 1; $day <= $dayMaxInMonth; $day++)
            {
                $column = "d_" . $day;

                $dataRtn[$month]["data_days"][$day] = $item->$column;
            }

            $dataRtn[$month]["total"] = array_sum($dataRtn[$month]["data_days"]);
        }

        ksort($dataRtn);

        return $dataRtn;
    }

    public function dataTableLocationPublicHome($arraySites): array
    {
        $siteIds = array_keys($arraySites);
        $items   = DB::table("keyword_statistics")
            ->whereIn("site_id", $siteIds)
            ->where("type", ReportKeywordEnum::TYPE_LOCATION_PUBLIC)
            ->get();

        return $this->reportKeywordTransformData->tableLocationPublicHome($items, $arraySites);
    }

    public function dataTableKwExtractHome($arraySites, $dayReport = 30, $sort = null): array
    {
        //        $siteIds    = array_keys($arraySites);
        list($monthQuery, $dates, $dayAgo) = $this->reportTimeUtil->getDayAgo($dayReport);
        $filter = [
            ["type", "=", ReportKeywordEnum::TYPE_EXTRACT_KEYWORD]
        ];

        $dataRtn = [];

        foreach ($arraySites as $site_id => $site_name)
        {
            $items = $this->__getDataBySiteId($site_id, $monthQuery, $filter);
            $data  = [];

            foreach ($items as $item)
            {
                foreach ($dates as $date)
                {
                    $dateItem = Carbon::parse($date); // Y/m/d
                    $year     = $dateItem->year;
                    $month    = $dateItem->month;
                    $day      = $dateItem->day;
                    $column   = "d_" . $day;

                    if(!isset($data[$item->key_type][$date]))
                    {
                        $data[$item->key_type][$date] = 0;
                    }

                    if ($item->year == $year && $item->month == $month)
                    {
                        $data[$item->key_type][$date] = $item->$column;
                    }
                }
            }

            $total_jd        = array_sum($data["count_jd"] ?? []);
            $total_extract   = array_sum($data["count_jd_done"] ?? []);
            $percent_extract = ($total_jd != 0) ? (round(($total_extract / $total_jd) * 100, 2)) : 0;

            $dataRtn[$site_id] = [
                "site_id"         => $site_id,
                "site_name"       => $site_name,
                "total_jd"        => $total_jd,
                "total_extract"   => $total_extract,
                "percent_extract" => $percent_extract,
                "data"            => $data
            ];
        }

        $arrPercentExtract = array_column($dataRtn, 'percent_extract');

        if ($sort == "asc")
        {
            array_multisort($arrPercentExtract, SORT_ASC, $dataRtn);
        }
        if ($sort == "desc")
        {
            array_multisort($arrPercentExtract, SORT_DESC, $dataRtn);
        }

        return [
            "dates"  => $dates,
            "result" => $dataRtn
        ];
    }

    public function chartOverviewExtract($request): array
    {
        $site_id   = $request->get("site_id");
        $dayReport = $request->get("day") ?? 30;
        list($monthQuery, $dates, $dayAgo) = $this->reportTimeUtil->getDayAgo($dayReport);

        $dates = array_reverse($dates);
        // Lấy dữ liệu
        $data  = $this->__getDataBySiteId($site_id, $monthQuery, []);
        $items = $data->filter(function ($item, $key)
        {
            return in_array($item->key_type, ["count_jd", "count_jd_done"]);
        });

        return $this->reportKeywordTransformData->chartOverviewExtract($items, $dates);
    }

    public function chartLocationExtract(Request $request): array
    {
        $siteId    = $request->get("site_id");
        $dayReport = $request->get("day", 30);
        list($monthQuery, $dates, $dayAgo) = $this->reportTimeUtil->getDayAgo($dayReport);
        $dates   = array_reverse($dates);
        $columns = ReportKeywordEnum::EXTRACT_LOCATION_KEY_TYPE;
        $filter  = [
            ["type", '=', ReportKeywordEnum::TYPE_EXTRACT_LOCATION]
        ];

        $dataOriginal = $this->__getDataBySiteId($siteId, $monthQuery, $filter);
        $dataChart    = $this->reportKeywordTransformData->chartLocationExtract($dataOriginal, $dates, $columns);
        $dataCompare  = $this->reportHelperUtil->calculatePercentExtract($dataChart['count_jd'], $dataChart['count_jd_done']);

        return [
            "dates"       => $dates,
            "dataChart"   => $dataChart,
            "dataCompare" => $dataCompare
        ];
    }

    public function dataTableLocationExtract($request, $sites): array
    {
        $dayReport = $request->get("day", 30);
        $tabSub    = $request->get("tabSub", "table-month");
        list($monthQuery, $filter, $dates) = $this->__paramQueryLocationExtract($tabSub, $dayReport);

        $dataRtn = [];

        foreach ($sites as $site)
        {
            $items = $this->__getDataBySiteId($site->id, $monthQuery, $filter);

            foreach ($items as $item)
            {
                if ($tabSub == "table-month")
                {
                    foreach ($dates as $date)
                    {
                        $dateItem = Carbon::parse($date); // Y/m/d
                        $year     = $dateItem->year;
                        $month    = $dateItem->month;
                        $day      = $dateItem->day;
                        $column   = "d_" . $day;

                        if ($item->year == $year && $item->month == $month)
                        {
                            $dataRtn[$site->id][$item->key_type][$date] = $item->$column;
                        }
                    }
                }
                else
                {
                    $key_type = $item->key_type;
                    $total    = 0;

                    for ($day = 1; $day <= 31; $day++)
                    {
                        $column = "d_" . $day;
                        $total  += $item->$column;
                    }

                    $dataRtn[$site->id][$key_type][$item->month] = $total;
                    ksort($dataRtn[$site->id][$key_type]);
                }

            }
        }

        return [
            "data"  => $dataRtn,
            "dates" => $dates
        ];
    }

    private function __paramQueryLocationExtract($tab, $dayReport): array
    {
        list($monthQuery, $dates, $dayAgo) = $this->reportTimeUtil->getDayAgo($dayReport);
        $filter = [
            ["type", "=", ReportKeywordEnum::TYPE_EXTRACT_LOCATION],
        ];

        if ($tab == "table-year")
        {
            $currentYear = Carbon::now()->year;
            $filter[]    = ["year", "=", $currentYear];
            $monthQuery  = [];
            $dates       = range(1, 12); // Month in year
        }

        return [
            $monthQuery,
            $filter,
            $dates
        ];
    }

    private function __getDataBySiteId($site_id, $monthQuery, $filter)
    {
        $query = DB::table("keyword_statistics")
            ->where('site_id', $site_id);

        if ($filter)
        {
            $query = $this->scopeFilter($query, $filter);
        }

        $query->where(function ($q) use ($monthQuery)
        {
            foreach ($monthQuery as $year => $monthArr)
            {
                $q->orWhere(function ($q) use ($year, $monthArr)
                {
                    $q->Where("year", $year)
                        ->whereIn("month", $monthArr);
                });
            }
        });

        return $query->get();
    }

}
