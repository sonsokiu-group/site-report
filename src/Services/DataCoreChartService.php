<?php

namespace Workable\SiteReport\Services;

use Illuminate\Support\Facades\DB;
use Workable\SiteReport\Core\AbstractChartData\DataChartAbstract;
use Workable\Support\Traits\ScopeCondition;
use Workable\Support\Traits\ScopeRepositoryTrait;

class DataCoreChartService extends DataChartAbstract
{
    use ScopeRepositoryTrait;
    use ScopeCondition;

    function getData($time)
    {
        // get param query
        $table   = $this->paramQueryChart->table();
        $filter  = $this->paramQueryChart->filter();
        $whereIn = $this->paramQueryChart->whereIn() ?? [];

        // build query
        $query = DB::table($table);
        $query = $this->scopeWhereIn($query, $whereIn);
        $query = $this->scopeFilter($query, $filter);

        $dataRaw = $query->get();

        // transform data raw
        return $this->transformData($dataRaw, $time);
    }
}
