<?php

namespace Workable\SiteReport\Utils;

use Carbon\Carbon;
use Workable\SiteReport\Enum\ReportTrafficTypeEnum;

class ReportTrafficTransformData
{

    public function transformDataChartDetailReferer($data, $dates, $columns): array
    {
        $dataTransform = [];

        $dataStruct = $this->__handleChartDataStructure($dates, $columns);

        foreach ($dataStruct as $keyReport => $arrayDataReport)
        {
            foreach ($arrayDataReport as $date => $value)
            {
                $dateItem = Carbon::parse($date); // Y/m/d
                $year = $dateItem->year;
                $month = $dateItem->month;
                $day = $dateItem->day;
                $column = "d_" . $day;

                foreach ($data as $dataItem)
                {
                    if ($dataItem->year == $year && $dataItem->month == $month)
                    {
                        $referer = $dataItem->referer;
                        $redirect = $dataItem->redirect;
                        if ($keyReport == $redirect)
                        {
                            if(isset($dataTransform[$keyReport][$date][$referer]))
                            {
                                $dataTransform[$keyReport][$date][$referer] += $dataItem->$column;
                            }
                            else
                            {
                                $dataTransform[$keyReport][$date][$referer] = $dataItem->$column;
                            }
                        }
                    }
                }
            }
        }

        $result = [];

        foreach ($dataTransform as $keyRedirect => $arrData)
        {
            foreach ($arrData as $date => $item)
            {
                foreach ($item as $keyReferer => $valueReferer)
                {
                    if(isset($result[$keyReferer][$date]))
                    {
                        $result[$keyReferer][$date] += $valueReferer;
                    }
                    else
                    {
                        $result[$keyReferer][$date] = $valueReferer;
                    }
                }
            }
        }

        $dataRtn = [];
        foreach ($result as $keyReferer => $arrValue)
        {
            $dataRtn[$keyReferer] = array_values($arrValue);
        }

      return $dataRtn;
    }

    public function transformDataChartOverview($data, $dates, $columns, $typeChart): array
    {
        $dataTransform = $this->transformDataLineChart($data, $dates, $columns, $typeChart);

        $dataRtn = [];

        foreach ($dataTransform as $keyReport => $arrayDataReport)
        {
            foreach ($arrayDataReport as $index => $value)
            {
               if(isset($dataRtn[$index]))
               {
                   $dataRtn[$index] += $value;
               }
               else
               {
                   $dataRtn[$index] = $value;
               }
            }
        }

        return $dataRtn;
    }

    public function transformDataLineChart($data, $dates, $columns, $typeChart): array
    {
        $dataRtn = [];

        $dataStruct = $this->__handleChartDataStructure($dates, $columns);

        foreach ($dataStruct as $keyReport => $arrayDataReport)
        {
            foreach ($arrayDataReport as $date => $value)
            {
                $dateItem = Carbon::parse($date); // Y/m/d
                $year = $dateItem->year;
                $month = $dateItem->month;
                $day = $dateItem->day;
                $column = "d_" . $day;

                foreach ($data as $dataItem)
                {
                    if ($dataItem->year == $year && $dataItem->month == $month)
                    {
                        switch ($typeChart) {
                            case ReportTrafficTypeEnum::CHART_TRAFFIC_HOME:
                                $dataStruct[$keyReport][$date] += $dataItem->$column;
                                break;

                            case ReportTrafficTypeEnum::CHART_GROUP_REDIRECT:
                                $redirectGroup = $this->checkGroupRedirect($dataItem->redirect);
                                if ($keyReport == $redirectGroup)
                                {
                                    $dataStruct[$keyReport][$date] += $dataItem->$column;
                                }
                                break;

                            case ReportTrafficTypeEnum::CHART_GROUP_REFERER:
                                if ($keyReport == $dataItem->referer)
                                {
                                    $dataStruct[$keyReport][$date] += $dataItem->$column;
                                }
                                break;

                            case ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_KEYWORD:
                            case ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_CATEGORY:
                            case ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_REMOTE:
                            case ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_HOME:
                            case ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_DETAIL:
                            case ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_COMPANY:
                            case ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_MAIN:
                                if ($keyReport == $dataItem->redirect)
                                {
                                    $dataStruct[$keyReport][$date] += $dataItem->$column;
                                }
                                break;

                            default:
                                if($typeChart == $dataItem->redirect){
                                    $dataStruct[$keyReport][$date] += $dataItem->$column;
                                }
                        }
                    }
                }
            }

            $dataRtn[$keyReport] = array_values($dataStruct[$keyReport]);
        }

        return $dataRtn;
    }

    public function transformDataPieChart($data, $dates, $columns, $typeChart): array
    {
        $dataTransform = $this->transformDataLineChart($data, $dates, $columns, $typeChart);

        $dataRtn = [];

        foreach ($dataTransform as $keyReport => $arrayDataReport)
        {
            $dataRtn[$keyReport] = array_sum($arrayDataReport);
        }

        return $dataRtn;
    }

    public function transformDataBarChart($data, $dates, $columns, $typeChart): array
    {
        $dataTransform = $this->transformDataLineChart($data, $dates, $columns, $typeChart);

        $dataRtn = [];

        foreach ($dataTransform as $keyReport => $arrayDataReport)
        {
            $dataRtn[$keyReport] = array_sum($arrayDataReport);
        }

        return $dataRtn;
    }

    public function __handleChartDataStructure($dates, $columns): array
    {
        // xây dựng cấu trúc data của biểu đồ.
        $dataStruct = [];

        foreach ($columns as $columnItem) {
            $keyReport = $columnItem["name"];

            foreach ($dates as $date) {
                $dataStruct[$keyReport][$date] = 0;
            }
        }

        return $dataStruct;
    }

    public function transformDataChartAllSite($dataOriginal, $dates, $siteIds): array
    {
        $dataRtn = [];

        foreach ($dates as $date)
        {
            $dateItem = Carbon::parse($date); //$date có dạng Y/m/d
            $year = $dateItem->year;
            $month = $dateItem->month;
            $day = $dateItem->day;
            $column = "d_" . $day;

            foreach ($dataOriginal as $dataItem)
            {
                if( $dataItem->year == $year && $dataItem->month == $month )
                {
                    $siteId = $dataItem->site_id;
                    if(isset($dataRtn[$siteId])){
                        $dataRtn[$siteId] += $dataItem->$column;
                    }
                    else
                    {
                        $dataRtn[$siteId] = $dataItem->$column;
                    }
                }
            }
        }

        // Bổ sung dữ liệu cho các siteId không tồn tại bản ghi trong DB
        foreach ($siteIds as $id)
        {
            if ( !isset($dataRtn[$id]) )
            {
                $dataRtn[$id] = 0;
            }
        }
        ksort($dataRtn);

        return $dataRtn;
    }

    public function transformDataTableOverview($dataOriginal, $dates): array
    {
        $dataRtn = [];

        foreach ($dates as $date)
        {
            $dateItem = Carbon::parse($date); // $date có dạng Y/m/d
            $year = $dateItem->year;
            $month = $dateItem->month;
            $day = $dateItem->day;
            $column = "d_" . $day;
            $date = $dateItem->format('d/m/Y');
            $dataRtn[$date]["total"] = 0;

            foreach ($dataOriginal as $dataItem)
            {
                if( $dataItem->year == $year && $dataItem->month == $month )
                {
                    $redirect = $dataItem->redirect;

                    if( isset($dataRtn[$date][$redirect]) ){
                        $dataRtn[$date][$redirect] += $dataItem->$column;
                    }
                    else
                    {
                        $dataRtn[$date][$redirect] = $dataItem->$column;
                    }

                    $dataRtn[$date]["total"] += $dataItem->$column;
                }
            }
        }

        return $dataRtn;
    }

    public function checkGroupRedirect($redirect = ""): string
    {
        $CONFIG_KEY_REDIRECT = ReportTrafficTypeEnum::CONFIG_KEY_REDIRECT;

        switch ($redirect) {
            case $CONFIG_KEY_REDIRECT['VIEW_DETAIL']:
                return "detail";

            case $CONFIG_KEY_REDIRECT["VIEW_HOME"]:
                return "home";

            case $CONFIG_KEY_REDIRECT["VIEW_COMPANY"]:
            case $CONFIG_KEY_REDIRECT["VIEW_COMPANY_STATE"]:
            case $CONFIG_KEY_REDIRECT["VIEW_COMPANY_CITY"]:
                return "company";

            case $CONFIG_KEY_REDIRECT["VIEW_KEYWORD"]:
            case $CONFIG_KEY_REDIRECT["VIEW_KEYWORD_STATE"]:
            case $CONFIG_KEY_REDIRECT["VIEW_KEYWORD_COUNTY"]:
            case $CONFIG_KEY_REDIRECT["VIEW_KEYWORD_CITY"]:
            case $CONFIG_KEY_REDIRECT["VIEW_KEYWORD_REGION"]:
            case $CONFIG_KEY_REDIRECT["VIEW_KEYWORD_AREA"]:
                return "keyword";

            case $CONFIG_KEY_REDIRECT["VIEW_CATEGORY"]:
            case $CONFIG_KEY_REDIRECT["VIEW_CATEGORY_STATE"]:
            case $CONFIG_KEY_REDIRECT["VIEW_CATEGORY_COUNTY"]:
            case $CONFIG_KEY_REDIRECT["VIEW_CATEGORY_CITY"]:
            case $CONFIG_KEY_REDIRECT["VIEW_CATEGORY_REGION"]:
            case $CONFIG_KEY_REDIRECT["VIEW_CATEGORY_AREA"]:
                return "category";

            case $CONFIG_KEY_REDIRECT["VIEW_LOCATION_STATE"]:
            case $CONFIG_KEY_REDIRECT["VIEW_LOCATION_COUNTY"]:
            case $CONFIG_KEY_REDIRECT["VIEW_LOCATION_CITY"]:
            case $CONFIG_KEY_REDIRECT["VIEW_LOCATION_REGION"]:
            case $CONFIG_KEY_REDIRECT["VIEW_LOCATION_AREA"]:
                return "location";

            case $CONFIG_KEY_REDIRECT["VIEW_REMOTE"]:
                return "remote";

            case $CONFIG_KEY_REDIRECT["VIEW_WORK_TYPE"]:
                return "work_type";

            default:
                return $redirect;
        }
    }

    public function transformDataTableRedirect($dataOriginal, $type): array
    {
        $dataRtn = [];

        foreach ($dataOriginal as $dataItem)
        {
            switch ($type) {
                case ReportTrafficTypeEnum::CHART_GROUP_REDIRECT:
                    $redirectGroup = $this->checkGroupRedirect($dataItem->redirect);

                    if (!isset($dataRtn[$redirectGroup]))
                    {
                        $dataRtn[$redirectGroup] = 0;
                    }

                    for ($i = 1; $i <= 31; $i++)
                    {
                        $column = "d_".$i;
                        $dataRtn[$redirectGroup] += $dataItem->$column;
                    }
                    break;

                default:
                    if (!isset($dataRtn[$dataItem->redirect]))
                    {
                        $dataRtn[$dataItem->redirect] = 0;
                    }
                    for ($i = 1; $i <= 31; $i++)
                    {
                        $column = "d_".$i;
                        $dataRtn[$dataItem->redirect] += $dataItem->$column;
                    }

            }
        }

        return $dataRtn;
    }

    public function transformDataTableReferer($dataOriginal, $dates, $type): array
    {
        $dataRtn = [];
        $arrayReferer = [];

        foreach ($dates as $date)
        {
            $dateItem = Carbon::parse($date); // $date có dạng Y/m/d
            $year = $dateItem->year;
            $month = $dateItem->month;
            $day = $dateItem->day;
            $column = "d_" . $day;

            foreach ($dataOriginal as $dataItem)
            {
                if ($dataItem->year == $year && $dataItem->month == $month)
                {
                    $redirect = $dataItem->redirect;
                    $referer = $dataItem->referer;

                    if (!in_array($referer, $arrayReferer))
                    {
                        $arrayReferer[] = $referer;
                    }

                    switch ($type)
                    {
                        case ReportTrafficTypeEnum::CHART_GROUP_REDIRECT:
                            $redirectGroup = $this->checkGroupRedirect($redirect);

                            if (!isset($dataRtn[$redirectGroup][$referer]))
                            {
                                $dataRtn[$redirectGroup][$referer] = $dataItem->$column;
                            }
                            else
                            {
                                $dataRtn[$redirectGroup][$referer] += $dataItem->$column;
                            }
                            break;

                        default:
                            if (!isset($dataRtn[$redirect][$referer]))
                            {
                                $dataRtn[$redirect][$referer] = $dataItem->$column;
                            }
                            else
                            {
                                $dataRtn[$redirect][$referer] += $dataItem->$column;
                            }
                    }
                }
            }
        }

        // Start: Đoạn code bổ sung traffic $referer khồng tồn tại của $redirect, mục đích render dữ liệu bảng đẹp mắt hơn
        foreach ($dataRtn as $redirect => $arrayValue)
        {
            foreach ($arrayReferer as $referer)
            {
                if(!isset($arrayValue[$referer]))
                {
                    $dataRtn[$redirect][$referer] = 0;
                }
            }
        }
        // End

        return $dataRtn;
    }

}
