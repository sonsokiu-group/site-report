<?php

namespace Workable\SiteReport\Utils;

class ReportHelperUtil
{
    public function compareDataChart($dataCurrent, $dataBefore): array
    {
        $totalCurrent = 0;
        $totalBefore  = 0;

        foreach ($dataCurrent as $keyReport => $valueReport)
        {
            if (is_array($valueReport))
            {
                $totalCurrent += array_sum($valueReport);
            }
            else
            {
                $totalCurrent += $valueReport;
            }

        }

        foreach ($dataBefore as $keyReport => $valueReport)
        {
            if (is_array($valueReport))
            {
                $totalBefore += array_sum($valueReport);
            }
            else
            {
                $totalBefore += $valueReport;
            }
        }

        if ($totalBefore === 0)
        {
            $growthRate = 100;
        }
        else
        {
            $growthRate = $this->compareGrowth($totalCurrent, $totalBefore, 2);
        }

        return [
            "totalCurrent" => $totalCurrent,
            "totalBefore"  => $totalBefore,
            "growthRate"   => $growthRate
        ];
    }

    public function compareGrowth($valueCurrent, $valueBefore, $precision = 2)
    {
        $result = 0;

        if ($valueBefore !== 0)
        {
            $result = (($valueCurrent - $valueBefore) / $valueBefore) * 100;
            $result = round($result, $precision);
        }

        return $result;
    }

    public function sortByTrafficAll($trafficCurrent, $sites): array
    {
        $dataRtn = [];

        arsort($trafficCurrent);

        foreach ($trafficCurrent as $siteId => $totalTraffic)
        {
            $dataRtn[$sites[$siteId]] = $totalTraffic;
        }

        return $dataRtn;
    }

    public function calculatePercentExtract($dataJd, $dataExtract): array
    {
        $totalJd        = 0;
        $totalJdExtract = 0;

        if (is_array($dataJd) && is_array($dataExtract))
        {
            $totalJd        = array_sum($dataJd);
            $totalJdExtract = array_sum($dataExtract);
        }

        $percentExtract = ($totalJd !== 0) ? round(($totalJdExtract / $totalJd) * 100, 2) : 0;

        return [
            "totalJd"        => $totalJd,
            "totalJdExtract" => $totalJdExtract,
            "percentExtract" => $percentExtract
        ];
    }
}
