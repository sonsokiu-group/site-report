<?php

namespace Workable\SiteReport\Utils;

use Carbon\Carbon;
use Carbon\CarbonPeriod;

class ReportTimeUtils
{
    public function getDayAgo($day = 30,$before = false): array
    {
        if($before){
            $startDay = Carbon::now()->subDays($day)->subDays($day)->subDays(1);
            $endDay   = Carbon::now()->subDays($day)->subDays(1);
        }else{
            $startDay = Carbon::now()->subDays($day);
            $endDay   = Carbon::now();
        }

        $period           = CarbonPeriod::create($startDay, $endDay);
        $monthQuery       = [];
        $rangeDayLastWeek = [];
        $dateRtn          = [];

        foreach ($period as $date)
        {
            $year                              = $date->format('Y');
            $month                             = $date->format('m');
            $day                               = $date->format('d');
            $month                             = (int)$month;
            $day                               = (int)$day;
            $rangeDayLastWeek[$year][$month][] = $day;
            $monthQuery[$year][$month]         = $month;
            $dateRtn[]                         = $date->format("Y/m/d");
        }

        return [
            $monthQuery,
            array_reverse($dateRtn),
            $rangeDayLastWeek
        ];
    }

    public function getArrayPeriod($monthBefore = 3): array
    {
        // Lấy ra mảng thời gian có dạng [ $month => $year ] của khoảng thời gian hiện tại trở về $monthBefore.
        // và các khoảng thời gian năm trước đó.

        $currentPeriod = [];
        $previousPeriod = [];
        $timePeriod = [];

        for ($i = 0; $i < $monthBefore; $i++)
        {
            $monthCurrent = Carbon::now()->subMonths($i);
            $month = $monthCurrent->format('m');
            $year = $monthCurrent->year;

            $currentPeriod[$month] = $year;
            $previousPeriod[$month] = $year - 1;
        }

        foreach ($currentPeriod as $month => $year)
        {
            $timePeriod[] = $month . "/" . $year;
        }
        foreach ($previousPeriod as $month => $year)
        {
            $timePeriod[] = $month . "/" . $year;
        }

        return [
            $currentPeriod,
            $previousPeriod,
            $timePeriod
        ];
    }
}
