<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 3/13/24
 * Time: 10:59
 */

namespace Workable\SiteReport\Utils;

use Carbon\Carbon;
use Workable\SiteReport\Enum\ReportKeywordEnum;

class ReportKeywordTransformData
{
    public function chartKeywordLevel($dataOriginal): array
    {
        $columns   = ReportKeywordEnum::KEYWORD_LEVEL_KEY_TYPE;
        $dataChart = [];

        foreach ($columns as $column)
        {
            foreach ($dataOriginal as $item)
            {
                if ($item->key_type == $column)
                {
                    $dataMeta           = json_decode($item->meta, true);
                    $totalKeyword       = $dataMeta["total"];
                    $dataChart[$column] = $totalKeyword;
                }
            }
        }

        return $dataChart;
    }

    public function tableKeywordLevel($dataOriginal, $arraySites): array
    {
        $columns = ReportKeywordEnum::KEYWORD_LEVEL_KEY_TYPE;
        $result  = [];

        foreach ($arraySites as $siteId => $siteName)
        {
            $result[$siteId] = [
                "site_name"     => $siteName,
                "total_keyword" => 0,
                "data_keyword"  => []
            ];
            foreach ($dataOriginal as $item)
            {
                foreach ($columns as $column)
                {
                    if (!isset($result[$siteId]["data_keyword"][$column]))
                    {
                        $result[$siteId]["data_keyword"][$column] = 0;
                    }

                    if ($siteId == $item->site_id && $item->key_type == $column)
                    {
                        $dataMeta                                 = json_decode($item->meta, true);
                        $keywordLevel                             = $dataMeta["total"] ?? 0;
                        $result[$siteId]["data_keyword"][$column] += $keywordLevel;
                        $result[$siteId]["total_keyword"]         += $keywordLevel;
                    }
                }
            }
        }

        return $result;
    }

    public function chartKeywordPublicHome($dataOriginal, $dates, $columns): array
    {
        $dataRtn    = [];
        $dataStruct = $this->__handleChartDataStructure($dates, $columns);

        foreach ($dataStruct as $keyReport => $arrayDataReport)
        {
            foreach ($arrayDataReport as $date => $value)
            {
                $dateItem = Carbon::parse($date); // Y/m/d
                $year     = $dateItem->year;
                $month    = $dateItem->month;
                $day      = $dateItem->day;
                $column   = "d_" . $day;

                foreach ($dataOriginal as $dataItem)
                {
                    if ($dataItem->year == $year && $dataItem->month == $month)
                    {
                        $dataStruct[$keyReport][$date] += $dataItem->$column;
                    }
                }
            }

            $dataRtn[$keyReport] = array_values($dataStruct[$keyReport]);
        }

        return $dataRtn;
    }

    public function tableKeywordPublicHome($dataOriginal, $dates, $arraySites): array
    {
        $dataRtn = [];

        foreach ($arraySites as $siteId => $siteName)
        {
            $dataRtn[$siteId] = [
                "site_id"       => $siteId,
                "site_name"     => $siteName,
                "total_keyword" => 0,
                "data_keyword"  => []
            ];
            foreach ($dataOriginal as $item )
            {
                foreach ($dates as $date)
                {
                    $dateItem = Carbon::parse($date); // Y/m/d
                    $year     = $dateItem->year;
                    $month    = $dateItem->month;
                    $day      = $dateItem->day;
                    $date     = $dateItem->format('d/m/y');
                    $column   = "d_" . $day;

                    if (!isset($dataRtn[$siteId]["data_keyword"][$date]))
                    {
                        $dataRtn[$siteId]["data_keyword"][$date] = 0;
                    }

                    if ($siteId == $item->site_id && $item->year == $year && $item->month == $month)
                    {
                        $dataRtn[$siteId]["data_keyword"][$date] = $item->$column;
                        $dataRtn[$siteId]["total_keyword"]       += $item->$column;
                    }
                }
            }
        }

        return $dataRtn;
    }

    public function tableLocationPublicHome($dataOriginal, $arraySites): array
    {
        $dataRtn = [];

        foreach ($arraySites as $siteId => $siteName)
        {
            $dataRtn[$siteId] = [
                'site_name'       => $siteName,
                'total_location'  => 0,
                'location_public' => 0,
                'percent_public'  => 0
            ];
        }

        foreach ($dataOriginal as $item)
        {
            $dataMeta                                   = json_decode($item->meta, true);
            $totalLocation                              = $dataMeta["total_location"] ?? 0;
            $locationPublic                             = $dataMeta["location_public"] ?? 0;
            $percentPubic                               = ($totalLocation == 0) ? 0 : (round(($locationPublic / $totalLocation) * 100, 2));
            $dataRtn[$item->site_id]['total_location']  = $totalLocation;
            $dataRtn[$item->site_id]['location_public'] = $locationPublic;
            $dataRtn[$item->site_id]['percent_public']  = $percentPubic;
        }

        return $dataRtn;
    }

    public function chartOverviewExtract($dataOriginal, $dates): array
    {
        $dataRtn = [];
        foreach ($dataOriginal as $item)
        {
            foreach ($dates as $date)
            {
                $dateItem = Carbon::parse($date); // Y/m/d
                $year     = $dateItem->year;
                $month    = $dateItem->month;
                $day      = $dateItem->day;
                $column   = "d_" . $day;

                if (!isset($dataRtn[$item->type][$item->key_type][$date]))
                {
                    $dataRtn[$item->type][$item->key_type][$date] = 0;
                }

                if ($item->year == $year && $item->month == $month)
                {
                    $dataRtn[$item->type][$item->key_type][$date] = $item->$column;
                }
            }
        }

        return [
            "dates"     => $dates,
            "dataChart" => [
                "count_jd"         => array_values($dataRtn[ReportKeywordEnum::TYPE_EXTRACT_KEYWORD]["count_jd"] ?? []),
                "extract_kw"       => array_values($dataRtn[ReportKeywordEnum::TYPE_EXTRACT_KEYWORD]["count_jd_done"] ?? []),
                "extract_location" => array_values($dataRtn[ReportKeywordEnum::TYPE_EXTRACT_LOCATION]["count_jd_done"] ?? [])
            ]
        ];
    }

    public function chartLocationExtract($dataOriginal, $dates, $columns): array
    {
        $dataRtn    = [];
        $dataStruct = $this->__handleChartDataStructure($dates, $columns);

        foreach ($dataStruct as $keyReport => $arrayDataReport)
        {
            foreach ($arrayDataReport as $date => $value)
            {
                $dateItem = Carbon::parse($date); // Y/m/d
                $year     = $dateItem->year;
                $month    = $dateItem->month;
                $day      = $dateItem->day;
                $column   = "d_" . $day;

                foreach ($dataOriginal as $dataItem)
                {
                    if ($dataItem->year == $year && $dataItem->month == $month && $dataItem->key_type == $keyReport)
                    {
                        $dataStruct[$keyReport][$date] += $dataItem->$column;
                    }
                }
            }

            $dataRtn[$keyReport] = array_values($dataStruct[$keyReport]);
        }

        return $dataRtn;
    }

    public function __handleChartDataStructure($dates, $columns): array
    {
        // xây dựng cấu trúc data của biểu đồ.
        $dataStruct = [];

        foreach ($columns as $column)
        {
            $keyReport = $column;

            foreach ($dates as $date)
            {
                $dataStruct[$keyReport][$date] = 0;
            }
        }

        return $dataStruct;
    }

}
