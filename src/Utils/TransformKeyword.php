<?php


namespace Workable\SiteReport\Utils;

use Carbon\Carbon;
use Workable\SiteReport\Core\AbstractChartData\TransformChartAbstract;
use Workable\SiteReport\Enum\ReportTrafficTypeEnum;

class TransformKeyword extends TransformChartAbstract
{

    private $type;
    private $reportTimeUtil;
    private $reportHelperUtil;

    public function __construct($type, ReportTimeUtils $reportTimeUtil)
    {
        $this->type = $type;
        $this->reportTimeUtil = $reportTimeUtil;
        $this->reportHelperUtil = new ReportHelperUtil();
    }

    public function process($data,$time)
    {
        switch ($this->type)
        {
            case 'list_kw_level':
                return $this->__keyword($data,$time);
            case 'list_kw_public':
                return $this->__listKeywordPublic($data,$time);

            case 'kw_lv':

            default:
                return $data;
        }
    }



    private function __listKeywordPublic($dataRaw,$time)
    {
        $dataRtn = [];
        $dataRtnPeriod = [];


        list($monthQuery, $dates, $dayAgo) = $this->reportTimeUtil->getDayAgo($time);
        list($monthQueryPeriod, $datesPeriod, $dayAgoPeriod) = $this->reportTimeUtil->getDayAgo($time, true);

        foreach ($dates as $date)
        {
            $dateItem = Carbon::parse($date);
            $year     = $dateItem->year;
            $month    = $dateItem->month;
            $day      = $dateItem->day;
            $column   = "d_" . $day;

            foreach ($dataRaw as  $dataItem)
            {
                if ($dataItem->year == $year && $dataItem->month == $month)
                {
                    if(!isset($dataRtn[$dataItem->site_id][$dataItem->type][$date]))
                    {
                        $dataRtn[$dataItem->site_id][$dataItem->type][$date] = $dataItem->$column;
                    }
                }
            }
        }

        foreach ($datesPeriod as $date)
        {
            $dateItem = Carbon::parse($date);
            $year     = $dateItem->year;
            $month    = $dateItem->month;
            $day      = $dateItem->day;
            $column   = "d_" . $day;

            foreach ($dataRaw as  $dataItem)
            {
                if ($dataItem->year == $year && $dataItem->month == $month)
                {
                    if(!isset($dataRtnPeriod[$dataItem->site_id][$dataItem->type][$date]))
                    {
                        $dataRtnPeriod[$dataItem->site_id][$dataItem->type][$date] = $dataItem->$column;
                    }
                }
            }
        }

        $arrCompare = [];

        foreach ($dataRtn as $siteId => $dataItem)
        {
            foreach ($dataItem as $type => $data)
            {
                $arrCompare[$siteId] = $this->reportHelperUtil->compareDataChart(array_values($data), array_values($dataRtnPeriod[$siteId][$type]));
                $dataRtn[$siteId][$type] = array_values($data);
                $dataRtnPeriod[$siteId][$type] = array_values($dataRtnPeriod[$siteId][$type]);
            }
        }

        return [
            "dates" => $dates,
            "dataChart" => $dataRtn,
            "dateBefore" => $datesPeriod,
            "dataChartBefore" => $dataRtnPeriod,
            "dataCompare" => $arrCompare
        ];


    }

    private function __genderPercentage($totalCurrent,$totalBefore){

        $growthRate = 0;

        if ($totalBefore === 0)
        {
            $growthRate = 100;
        }
        else
        {
            if ($totalBefore !== 0)
            {
                $growthRate = (($totalCurrent - $totalBefore) / $totalBefore) * 100;
                $growthRate = round($growthRate, 2);
            }
        }


        return [
            "totalCurrent" => $totalCurrent,
            "totalBefore"  => $totalBefore,
            "growthRate"     => $growthRate
        ];
    }



}
