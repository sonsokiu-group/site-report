<?php

namespace Workable\SiteReport\Utils;


use Carbon\Carbon;

class ReportTrafficDataTable
{
    protected $reportHelperUtil;

    public function __construct()
    {
        $this->reportHelperUtil = new ReportHelperUtil();
    }

    public function makeDataTableRedirect($dataNow, $dataPeriod): array
    {
        $dataRtn = [];

        foreach ($dataNow as $timeReport => $arrValue)
        {
            $lastMonth = Carbon::createFromFormat('m/Y', $timeReport)->subMonth()->format('m/Y');
            $lastYear = Carbon::createFromFormat('m/Y', $timeReport)->subYear()->format('m/Y');

            foreach ($arrValue as $keyReport => $valueReport)
            {
                $valueLastMonth = $dataNow[$lastMonth][$keyReport] ?? 0;
                $valueLastPeriod = $dataPeriod[$lastYear][$keyReport] ?? 0;
                $dataRtn[$keyReport][$timeReport]["total"] = $valueReport;

                $dataRtn[$keyReport][$timeReport]["compare_last_month"] =
                    $this->reportHelperUtil->compareGrowth($valueReport, $valueLastMonth);

                $dataRtn[$keyReport][$timeReport]["compare_last_period"] =
                    $this->reportHelperUtil->compareGrowth($valueReport, $valueLastPeriod);
            }
        }

        foreach ($dataPeriod as $timeReport => $arrValue)
        {
            foreach ($arrValue as $keyReport => $value)
            {
                $dataRtn[$keyReport][$timeReport]["total"] = $value;
                $dataRtn[$keyReport][$timeReport]["compare_last_month"] = 0;
                $dataRtn[$keyReport][$timeReport]["compare_last_period"] = 0;
            }
        }

        return $dataRtn;
    }

    public function makeDataTableReferer($dataCurrent, $dataBefore): array
    {
        $dataRtn = [];
        $totalTraffic = 0;

        if (empty($dataCurrent))
        {
            return  $dataRtn;
        }

        foreach ($dataCurrent as $redirect => $arrayValue)
        {
            foreach ($arrayValue as $referer => $value)
            {
                if(!isset($dataBefore[$redirect][$referer]))
                {
                    $dataBefore[$redirect][$referer] = 0;
                }
                $valueLastPeriod = $dataBefore[$redirect][$referer];

                $totalTraffic += $value;
                $dataRtn[$redirect][$referer]["total"] = $value;
                $dataRtn[$redirect][$referer]["total_last_period"] = $valueLastPeriod;
                $dataRtn[$redirect][$referer]["compare_last_period"] =
                    $this->reportHelperUtil->compareGrowth($value, $valueLastPeriod);
            }
        }
        $dataRtn["total"] = $totalTraffic;

        return $dataRtn;
    }
}
