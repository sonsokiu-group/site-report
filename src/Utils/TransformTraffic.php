<?php


namespace Workable\SiteReport\Utils;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Workable\SiteReport\Core\AbstractChartData\TransformChartAbstract;
use Workable\SiteReport\Enum\ReportTrafficTypeEnum;

class TransformTraffic extends TransformChartAbstract
{

    private $type;
    private $reportTimeUtil;
    private $reportHelperUtil;

    public function __construct($type, ReportTimeUtils $reportTimeUtil)
    {
        $this->type = $type;
        $this->reportTimeUtil = $reportTimeUtil;
        $this->reportHelperUtil = new ReportHelperUtil();
    }

    public function process($data,$time)
    {
        switch ($this->type)
        {
            case 'traffic_all_site_bar':
                return $this->__trafficAllSiteBar($data,$time);
            case 'traffic_all_site_line':
                return $this->__trafficAllSiteLine($data,$time);
            case 'list_traffic_home':
                return $this->__listTrafficHome($data,$time);

            case 'kw_lv':

            default:
                return $data;
        }
    }

    private function __trafficAllSiteBar($dataRaw,$time){
        $dataRtn = [];
        $dataRtnPeriod = [];

        list($monthQuery, $dates, $dayAgo) = $this->reportTimeUtil->getDayAgo($time);
        list($monthQueryPeriod, $datesPeriod, $dayAgoPeriod) = $this->reportTimeUtil->getDayAgo($time, true);

        foreach ($dates as $date)
        {
            $dateItem = Carbon::parse($date);
            $year     = $dateItem->year;
            $month    = $dateItem->month;
            $day      = $dateItem->day;
            $column   = "d_" . $day;
            foreach ($dataRaw as  $dataItem)
            {
                if ($dataItem->year == $year && $dataItem->month == $month)
                {
                    if(!isset($dataRtn[$dataItem->site_id]))
                    {
                        $dataRtn[$dataItem->site_id] = $dataItem->$column;
                    }else{
                        $dataRtn[$dataItem->site_id] += $dataItem->$column;
                    }
                }else{
                    if(!isset($dataRtn[$dataItem->site_id]))
                    {
                        $dataRtn[$dataItem->site_id] = 0;
                    }
                }
            }
        }

        foreach ($datesPeriod as $date)
        {
            $dateItem = Carbon::parse($date);
            $year     = $dateItem->year;
            $month    = $dateItem->month;
            $day      = $dateItem->day;
            $column   = "d_" . $day;

            foreach ($dataRaw as  $dataItem)
            {
                if ($dataItem->year == $year && $dataItem->month == $month)
                {
                    if(!isset($dataRtnPeriod[$dataItem->site_id]))
                    {
                        $dataRtnPeriod[$dataItem->site_id] = $dataItem->$column;
                    }else{
                        $dataRtnPeriod[$dataItem->site_id] += $dataItem->$column;
                    }
                }else{
                    if(!isset($dataRtnPeriod[$dataItem->site_id]))
                    {
                        $dataRtnPeriod[$dataItem->site_id] = 0;
                    }
                }
            }
        }
        $ids = array_keys($dataRtn);
        $dataRtn = array_values($dataRtn);
        $dataRtnPeriod = array_values($dataRtnPeriod);
        $arrCompare = $this->reportHelperUtil->compareDataChart($dataRtn, $dataRtnPeriod);
        $sites = DB::table('sites')->whereIn('id',$ids)->get();
        $arrLabel = [];
        foreach ($ids as $site){
            $arrLabel[] = $sites->where('id',$site)->first()->site_name;
        }
        return [
            "label" => ($arrLabel),
            "dataChart" =>  ($dataRtn),
            "dateBefore" => ($datesPeriod),
            "dataChartBefore" => ($dataRtnPeriod),
            "dataCompare" => $arrCompare
        ];

    }
    private function __trafficAllSiteLine($dataRaw,$time){
        $dataRtn = [];
        $dataRtnPeriod = [];

        list($monthQuery, $dates, $dayAgo) = $this->reportTimeUtil->getDayAgo($time);
        list($monthQueryPeriod, $datesPeriod, $dayAgoPeriod) = $this->reportTimeUtil->getDayAgo($time, true);

        foreach ($dates as $date)
        {
            $dateItem = Carbon::parse($date);
            $year     = $dateItem->year;
            $month    = $dateItem->month;
            $day      = $dateItem->day;
            $column   = "d_" . $day;
            foreach ($dataRaw as  $dataItem)
            {
                if ($dataItem->year == $year && $dataItem->month == $month)
                {
                    if(!isset($dataRtn[$date]))
                    {
                        $dataRtn[$date] = $dataItem->$column;
                    }else{
                        $dataRtn[$date] += $dataItem->$column;
                    }
                }else{
                    if(!isset($dataRtn[$date]))
                    {
                        $dataRtn[$date] = 0;
                    }
                }
            }
        }

        foreach ($datesPeriod as $date)
        {
            $dateItem = Carbon::parse($date);
            $year     = $dateItem->year;
            $month    = $dateItem->month;
            $day      = $dateItem->day;
            $column   = "d_" . $day;

            foreach ($dataRaw as  $dataItem)
            {
                if ($dataItem->year == $year && $dataItem->month == $month)
                {
                    if(!isset($dataRtnPeriod[$date]))
                    {
                        $dataRtnPeriod[$date] = $dataItem->$column;
                    }else{
                        $dataRtnPeriod[$date] += $dataItem->$column;
                    }
                }else{
                    if(!isset($dataRtnPeriod[$date]))
                    {
                        $dataRtnPeriod[$date] = 0;
                    }
                }
            }
        }
        $dataRtn = array_values($dataRtn);
        $dataRtnPeriod = array_values($dataRtnPeriod);
        $arrCompare = $this->reportHelperUtil->compareDataChart($dataRtn, $dataRtnPeriod);

        return [
            "dates" => array_reverse($dates),
            "dataChart" => [
                'Kì trước' => array_reverse($dataRtnPeriod),
                'Hiện tại' => array_reverse($dataRtn)
            ],
            "dateBefore" => $datesPeriod,
            "dataChartBefore" => $dataRtnPeriod,
            "dataCompare" => $arrCompare
        ];

    }
    private function __listTrafficHome($dataRaw,$time)
    {
        $dataRtn = [];
        $dataRtnPeriod = [];

        list($monthQuery, $dates, $dayAgo) = $this->reportTimeUtil->getDayAgo($time);
        list($monthQueryPeriod, $datesPeriod, $dayAgoPeriod) = $this->reportTimeUtil->getDayAgo($time, true);

        foreach ($dates as $date)
        {

            $dateItem = Carbon::parse($date);
            $year     = $dateItem->year;
            $month    = $dateItem->month;
            $day      = $dateItem->day;
            $column   = "d_" . $day;
            foreach ($dataRaw as  $dataItem)
            {

                if ($dataItem->year == $year && $dataItem->month == $month)
                {
                    if(!isset($dataRtn[$dataItem->site_id][$date]))
                    {
                        $dataRtn[$dataItem->site_id][$date] = $dataItem->$column;
                    }
                }
            }
        }


        foreach ($datesPeriod as $date)
        {
            $dateItem = Carbon::parse($date);
            $year     = $dateItem->year;
            $month    = $dateItem->month;
            $day      = $dateItem->day;
            $column   = "d_" . $day;

            foreach ($dataRaw as  $dataItem)
            {
                if ($dataItem->year == $year && $dataItem->month == $month)
                {
                    if(!isset($dataRtnPeriod[$dataItem->site_id][$date]))
                    {
                        $dataRtnPeriod[$dataItem->site_id][$date] = $dataItem->$column;
                    }
                }
            }
        }
        $arrCompare = [];
        foreach ($dataRtn as $siteId => $dataItem)
        {
                if(!isset($dataRtnPeriod[$siteId]))
                {
                    $dataRtnPeriod[$siteId] = [];
                }
                $arrCompare[$siteId] = $this->reportHelperUtil->compareDataChart(array_values($dataItem), array_values($dataRtnPeriod[$siteId]));
                $dataRtn[$siteId] = array_values($dataItem);
                $dataRtnPeriod[$siteId] = array_values($dataRtnPeriod[$siteId]);
        }

        return [
            "dates" => array_reverse($dates),
            "dataChart" => array_reverse($dataRtn),
            "dateBefore" => array_reverse($datesPeriod),
            "dataChartBefore" => array_reverse($dataRtnPeriod),
            "dataCompare" => array_reverse($arrCompare)
        ];


    }

    private function __genderPercentage($totalCurrent,$totalBefore){

        $growthRate = 0;

        if ($totalBefore === 0)
        {
            $growthRate = 100;
        }
        else
        {
            if ($totalBefore !== 0)
            {
                $growthRate = (($totalCurrent - $totalBefore) / $totalBefore) * 100;
                $growthRate = round($growthRate, 2);
            }
        }


        return [
            "totalCurrent" => $totalCurrent,
            "totalBefore"  => $totalBefore,
            "growthRate"     => $growthRate
        ];
    }



}
