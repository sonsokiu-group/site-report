<?php

namespace Workable\SiteReport\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SiteTrafficOverviewTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('site_traffic_overview')->truncate();

        $sites      = $this->getSites();
        $timeArray  = $this->getTimeArray();
        $dataInsert = [];

        foreach ($sites as $siteId => $site)
        {
            \CliEcho::infonl('-- Run fake data sites_traffic_overview by site' . $site . ' - id:' . $siteId);
            foreach ($timeArray as $month => $year)
            {
                $data = [
                    'site_id'    => $siteId,
                    'year'       => $year,
                    'month'      => $month,
                    'meta'       => null,
                    'created_at' => now(),
                    'updated_at' => now()
                ];
                for ($day = 1; $day <= 31; $day++)
                {
                    $data['d_' . $day] = rand(10000, 50000);
                }

                $dataInsert[] = $data;
            }
        }

        DB::table('site_traffic_overview')->insert($dataInsert);
    }

    public function getTimeArray($monthBefore = 3): array
    {
        $dataRtn = [];

        for ($i = 0; $i < $monthBefore; $i++)
        {
            $monthCurrent = Carbon::now()->subMonths($i);
            $month        = $monthCurrent->format('m');
            $year         = $monthCurrent->year;

            $dataRtn[$month] = $year;
        }

        return $dataRtn;
    }

    public function getSites()
    {
        return DB::table("sites")
            ->pluck('site_name', 'id')
            ->toArray();
    }
}

// php artisan db:seed --class=Workable\\SiteReport\\Database\\Seeders\\SiteTrafficOverviewTableSeeder
