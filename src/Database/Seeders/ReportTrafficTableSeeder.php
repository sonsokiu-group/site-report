<?php

namespace Workable\SiteReport\Database\Seeders;


use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Workable\Base\Supports\CliEcho;

class ReportTrafficTableSeeder extends Seeder
{
    protected $referer = ['google', 'facebook'];
    protected $redirectGroup = [
        "detail"    => ["detail"],
        "home"      => ["home"],
        "company"   => [
            'company',
            'company_state',
            'company_city'
        ],
        "keyword"   => [
            'keyword',
            'keyword_state',
            'keyword_county',
            'keyword_city',
            'keyword_region',
            'keyword_area'
        ],
        "category"  => [
            'category',
            'category_state',
            'category_county',
            'category_city',
            'category_region',
            'category_area'
        ],
        "remote"    => [
            'remote'
        ],
        "work_type" => ['work_type'],
        "location"  => [
            'location_state',
            'location_county',
            'location_city',
            'location_region',
            'location_area',
        ]
    ];

    public function run()
    {
        DB::table('sites_traffic')->truncate();
        $sites = $this->getSites();

        foreach ($sites as $siteId => $site)
        {
            $dataInsert = $this->renderDataBySiteId($siteId);
            \CliEcho::infonl('-- Run fake data sites_traffic by site' . $site . ' - id:' . $siteId);
            DB::table('sites_traffic')->insert($dataInsert);
        }
    }

    public function renderDataBySiteId($siteId): array
    {
        $dataRtn     = [];
        $currentDate = Carbon::now();
        $yearCurrent = $currentDate->year;
        $twoYearsAgo = $currentDate->subYears(2)->year;

        for ($i = $yearCurrent; $i >= $twoYearsAgo; $i--)
        {
            for ($month = 1; $month <= 12; $month++)
            {
                $dataDefault = [
                    'site_id' => $siteId,
                    'year'    => $i,
                    'month'   => $month,
                    'meta'    => null
                ];

                foreach ($this->referer as $refererItem)
                {
                    foreach ($this->redirectGroup as $keyGroup => $arrayRedirect)
                    {
                        foreach ($arrayRedirect as $redirectItem)
                        {
                            $dataDefault['referer']  = $refererItem;
                            $dataDefault['redirect'] = $redirectItem;

                            for ($day = 1; $day <= 31; $day++)
                            {
                                if ($refererItem == 'facebook')
                                {
                                    $dataDefault['d_' . $day] = rand(100, 1000);
                                }
                                else
                                {
                                    $dataDefault['d_' . $day] = rand(500, 5000);
                                }
                            }

                            $dataRtn[] = $dataDefault;
                        }
                    }
                }
            }
        }

        return $dataRtn;
    }

    public function getSites()
    {
        return DB::table("sites")
            ->pluck('site_name', 'id')
            ->toArray();
    }
}

// php artisan db:seed --class=Workable\\SiteReport\\Database\\Seeders\\ReportTrafficTableSeeder
