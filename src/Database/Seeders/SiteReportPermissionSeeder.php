<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 3/5/24
 * Time: 10:05
 */

namespace Workable\SiteReport\Database\Seeders;

use Modules\Company\Database\Seeders\PermissionTableSeeder;

class SiteReportPermissionSeeder extends PermissionTableSeeder
{
    protected $path_config = 'packages/site-report/config/permission.php';

    public function run()
    {
        parent::run();
    }
}

// php artisan db:seed --class=Workable\\SiteReport\\Database\\Seeders\\SiteReportPermissionSeeder