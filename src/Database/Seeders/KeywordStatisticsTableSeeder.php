<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 3/8/24
 * Time: 16:15
 */

namespace Workable\SiteReport\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Workable\SiteReport\Enum\ReportKeywordEnum;

class KeywordStatisticsTableSeeder extends Seeder
{

    protected $dataType = [
        ReportKeywordEnum::TYPE_KEYWORD_LEVEL    => ReportKeywordEnum::KEYWORD_LEVEL_KEY_TYPE,
        ReportKeywordEnum::TYPE_KEYWORD_PUBLIC   => ReportKeywordEnum::KEYWORD_PUBLIC_KEY_TYPE,
        ReportKeywordEnum::TYPE_LOCATION_PUBLIC  => ReportKeywordEnum::LOCATION_PUBLIC_KEY_TYPE,
        ReportKeywordEnum::TYPE_EXTRACT_KEYWORD  => ReportKeywordEnum::EXTRACT_KEYWORD_KEY_TYPE,
        ReportKeywordEnum::TYPE_EXTRACT_LOCATION => ReportKeywordEnum::EXTRACT_LOCATION_KEY_TYPE
    ];

    public function run()
    {
        DB::table('keyword_statistics')->truncate();

        $sites      = $this->getSites();
        $timeArray  = $this->getTimeArray();
        $dataInsert = [];

        foreach ($sites as $siteId => $site)
        {
            \CliEcho::infonl('-- Run fake data keyword_statistics by site' . $site . ' - id:' . $siteId);
            foreach ($this->dataType as $key => $value)
            {

                if ($key == ReportKeywordEnum::TYPE_KEYWORD_LEVEL)
                {
                    foreach ($value as $keyType)
                    {
                        DB::table('keyword_statistics')->insert([
                            'site_id'  => $siteId,
                            'type'     => $key,
                            'key_type' => $keyType,
                            'year'     => now()->year,
                            'month'    => now()->month,
                            'meta'     => json_encode([
                                'total' => rand(100, 1000),
                            ])
                        ]);
                    }
                }
                if ($key == ReportKeywordEnum::TYPE_KEYWORD_PUBLIC)
                {
                    foreach ($timeArray as $month => $year)
                    {
                        $arr = [
                            'site_id'  => $siteId,
                            'type'     => $key,
                            'key_type' => Null,
                            'year'     => $year,
                            'month'    => $month,
                        ];
                        for ($i = 1; $i < 32; $i++)
                        {
                            $arr['d_' . $i] = rand(50, 500);
                        }
                        DB::table('keyword_statistics')->insert($arr);
                    }
                }
                if ($key == ReportKeywordEnum::TYPE_LOCATION_PUBLIC)
                {
                    $arr = [
                        'site_id'  => $siteId,
                        'type'     => $key,
                        'key_type' => Null,
                        'year'     => now()->year,
                        'month'    => now()->month,
                        'meta'     => json_encode([
                            'total_location'  => rand(50000, 60000),
                            'location_public' => rand(40000, 45000)
                        ])
                    ];
                    DB::table('keyword_statistics')->insert($arr);
                }
                if ($key == ReportKeywordEnum::TYPE_EXTRACT_KEYWORD)
                {
                    foreach ($timeArray as $month => $year)
                    {
                        foreach ($value as $keyType)
                        {
                            $arr = [
                                'site_id'  => $siteId,
                                'type'     => $key,
                                'key_type' => $keyType,
                                'year'     => $year,
                                'month'    => $month,
                            ];
                            for ($i = 1; $i < 32; $i++)
                            {
                                if ($keyType == 'count_jd')
                                {
                                    $count = rand(100, 150);
                                }
                                else
                                {
                                    $count = rand(50, 100);
                                }
                                $arr['d_' . $i] = $count;
                            }
                            DB::table('keyword_statistics')->insert($arr);
                        }
                    }
                }
                if ($key == ReportKeywordEnum::TYPE_EXTRACT_LOCATION)
                {
                    foreach ($timeArray as $month => $year)
                    {
                        foreach ($value as $keyType)
                        {
                            $arr = [
                                'site_id'  => $siteId,
                                'type'     => $key,
                                'key_type' => $keyType,
                                'year'     => $year,
                                'month'    => $month,
                            ];
                            for ($i = 1; $i < 32; $i++)
                            {
                                if ($keyType == 'count_jd')
                                {
                                    $count = rand(100, 150);
                                }
                                elseif ($keyType == 'count_jd_done')
                                {
                                    $count = rand(50, 100);
                                }
                                elseif ($keyType == 'state')
                                {
                                    $count = rand(30, 50);
                                }
                                elseif ($keyType == 'city')
                                {
                                    $count = rand(30, 50);
                                }
                                else
                                {
                                    $count = rand(30, 40);
                                }
                                $arr['d_' . $i] = $count;
                            }
                            DB::table('keyword_statistics')->insert($arr);
                        }
                    }
                }
            }
        }

        DB::table('keyword_statistics')->insert($dataInsert);
    }

    public function getSites()
    {
        return DB::table("sites")
            ->pluck('site_name', 'id')
            ->toArray();
    }

    public function getTimeArray($monthBefore = 3): array
    {
        $dataRtn = [];

        for ($i = 0; $i < $monthBefore; $i++)
        {
            $monthCurrent = Carbon::now()->subMonths($i);
            $month        = $monthCurrent->format('m');
            $year         = $monthCurrent->year;

            $dataRtn[$month] = $year;
        }

        return $dataRtn;
    }

}

// php artisan db:seed --class=Workable\\SiteReport\\Database\\Seeders\\KeywordStatisticsTableSeeder
