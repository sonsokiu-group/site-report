<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 5/14/24
 * Time: 16:34
 */

namespace Workable\SiteReport\Core\Contracts;

use Workable\Support\Http\HttpBuilder;

abstract class CrawlDataKeywordAbstract
{
    protected $betweenRequest = 1;
    protected $country = "";
    protected $url = "";
    protected $accessToken = "";
    protected $sourceCrawl = [];
    protected $httpBuilder;


    public function __construct()
    {
        $this->httpBuilder = new HttpBuilder();
    }

    public function setCountry($country): CrawlDataKeywordAbstract
    {
        $this->country = $country;
        return $this;
    }

    public function setSourceCrawl($sourceCrawl = []): CrawlDataKeywordAbstract
    {
        $this->sourceCrawl = $sourceCrawl;
        return $this;
    }

    public function setUrl($url, $token): CrawlDataKeywordAbstract
    {
        $this->url         = $url;
        $this->accessToken = $token;
        return $this;
    }

    private function __getHost()
    {
        return $this->sourceCrawl['host'];
    }

    protected function _buildParam($paramQuery = [], $accessToken = "")
    {
        $paramQuery['access_token'] = $accessToken;

        return $paramQuery;
    }

    protected function _buildHeader($header = [])
    {
        return $header;
    }

    private function __parseResult($result)
    {
        return $this->httpBuilder->getDataResponse($result);
    }

    protected function _requestGet($url, $paramQuery = [], $header = [])
    {
        $host            = $this->__getHost();
        $paramQueryBuild = $this->_buildParam($paramQuery, $this->accessToken);
        $headerBuild     = $this->_buildHeader($header);

        $response = $this->httpBuilder
            ->setTimeout(60)
            ->host($host)
            ->header($headerBuild)
            ->queryString($paramQueryBuild)
            ->get($url)
            ->call(false, true);

        return $this->__parseResult($response);
    }

    protected function _requestPost($url = "", $paramQuery = [], $header = [])
    {
        $host            = $this->__getHost();
        $paramQueryBuild = $this->_buildParam($paramQuery, $this->accessToken);
        $headerBuild     = $this->_buildHeader($header);

        $response = $this->httpBuilder
            ->setTimeout(60)
            ->host($host)
            ->header($headerBuild)
            ->formParams($paramQueryBuild)
            ->post($url)
            ->call(false, true);

        return $this->__parseResult($response);
    }

    public abstract function process($paramQuery = []);
}