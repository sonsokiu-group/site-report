<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 5/15/24
 * Time: 09:13
 */

namespace Workable\SiteReport\Core\Contracts;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

abstract class ProcessDataKeywordAbstract
{
    protected $paramQuery = '';

    public function setParamQuery($paramQuery): ProcessDataKeywordAbstract
    {
        $this->paramQuery = $paramQuery;
        return $this;
    }

    public function getProcess()
    {
        return Arr::get($this->paramQuery, 'process');
    }

    public function getCountry()
    {
        return Arr::get($this->paramQuery, 'site');
    }

    public function getSiteId()
    {
        return Arr::get($this->paramQuery, 'site_id');
    }

    public function getYear()
    {
        return Arr::get($this->paramQuery, 'year');
    }

    public function getMonth()
    {
        return Arr::get($this->paramQuery, 'month');
    }

    public function getDay()
    {
        return Arr::get($this->paramQuery, 'day');
    }

    public function updateOrInsert($dataSearch, $dataStore)
    {
        DB::table("keyword_statistics")->updateOrInsert($dataSearch, $dataStore);
    }

    public abstract function process($data = []);
}