<?php

namespace Workable\SiteReport\Core\QueryChartData;


use Workable\SiteReport\Utils\ReportTimeUtils;
use Workable\SiteReport\Utils\TransformKeyword;
use Workable\SiteReport\Utils\TransformTraffic;

class ChartQueryData
{
    private $query;

    public function __construct($query = [])
    {
        $this->query = $query;
    }

    public function table()
    {
        return $this->query['table'];
    }

    public function filter()
    {
        return $this->query['filter'] ?? [];
    }

    public function whereIn()
    {
        return $this->query['whereIn'] ?? [];
    }

    public function transformClass()
    {
        switch ($this->query['transform'])
        {
            case 'list_kw_public':
                return new TransformKeyword($this->query['transform'], new ReportTimeUtils());

            case 'list_traffic_home':
            case 'traffic_all_site_line' :
            case 'traffic_all_site_bar':
                return new TransformTraffic($this->query['transform'], new ReportTimeUtils());
        }
    }
}