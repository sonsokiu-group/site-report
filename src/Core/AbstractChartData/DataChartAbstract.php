<?php

namespace Workable\SiteReport\Core\AbstractChartData;
abstract class DataChartAbstract
{
    protected $paramQueryChart;

    public function setParamQueryChart($paramQueryChart): DataChartAbstract
    {
        $this->paramQueryChart = $paramQueryChart;
        return $this;
    }

    public abstract function getData($time);

    public function transformData($dataRtn, $time)
    {
        $classTransform = $this->paramQueryChart->transformClass();
        if ($classTransform instanceof TransformChartAbstract)
        {
            $dataRtn = $classTransform->transform($dataRtn, $time);
        }

        return $dataRtn;
    }
}
