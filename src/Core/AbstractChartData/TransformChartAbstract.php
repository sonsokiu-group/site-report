<?php

namespace Workable\SiteReport\Core\AbstractChartData;
abstract class TransformChartAbstract{

    public abstract function process($data,$time);

    public function transform($data,$time){
        return $this->process($data,$time);
    }
}
