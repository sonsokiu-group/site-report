# Google Analytics API

Core Package supports connection to GA4

## How to using

### Step 1: Init package
```
$googleAnalyticsEvent = new GoogleAnalyticsEvent();
```

### Step 2: Set configuration

1. Set date
```
$googleAnalyticsEvent->setDate($startDate, $endDate); 
```

- $startDate: Time to start collecting
- $endDate: Time to end collecting

2. Set property ID
```
$googleAnalyticsEvent->setPropertyID($yourPropertyID); 
```

3. Set stream ID
```
$googleAnalyticsEvent->setStreamID($yourStreamID); 
```

4. Set metric 

In Google Analytics 4 (GA4), setMetrics is a function that allows you to specify the metrics you want to include in your data query. Metrics are quantitative measurements that provide insights into user behavior and website performance. Examples of metrics include activeUsers, sessions, pageviews, bounceRate, etc.
```
$googleAnalyticsEvent->setMetrics("activeUsers"); 
```

5. Set dimensions 

In Google Analytics 4 (GA4), setDimensions is a method that allows you to specify the dimensions you want to include in your data query. Dimensions are qualitative attributes of your data, such as page URLs, page titles, traffic sources, and user locations. They help categorize and organize metrics, providing deeper insights into user behavior.

```
$googleAnalyticsEvent->setDimensions(['eventName', 'date', 'deviceCategory', 'streamId']); 
```

6. Set auth config
```
$googleAnalyticsEvent->setAuthConfig($path); 
```

- credential file is a json object from google console api when you create key on Service Accounts
- $path is path to credential file


### Step 3: Get Data

When you want get results from API
```
$googleAnalyticsEvent->get(); 
```
