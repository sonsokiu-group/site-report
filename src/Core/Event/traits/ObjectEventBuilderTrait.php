<?php

namespace Workable\SiteReport\Core\Event\traits;

trait ObjectEventBuilderTrait
{
    public function __getAnalystFilterEventGroup()
    {
        $groups     = [];
        $dataFilter = [
            "eventName" => $this->filterEvent,
            "streamId"  => $this->streamId,
        ];

        foreach ($dataFilter as $eventName => $value) {
            $group = $this->__getObjectFilterEvent($eventName, $value);

            if (empty($group)) {
                continue;
            }

            $groups[] = $group;
        }

        if (!$groups) {
            return null;
        }

//        Tạo nhóm điều kiện
        $filterExpressionList = new \Google_Service_AnalyticsData_FilterExpressionList();
        $filterExpressionList->setExpressions($groups);

//        Tạo filter theo nhóm điều kiện
        $combinedFilterExpression = new \Google_Service_AnalyticsData_FilterExpression();
        $combinedFilterExpression->setAndGroup($filterExpressionList);

        return $combinedFilterExpression;
    }

    private function __getObjectFilterEvent($eventName, $value)
    {
        $filters = !is_array($value) ? (string)$value : array_map('strval', array_filter($value));

        if (!$filters) {
            return null;
        }

        $filter = new \Google_Service_AnalyticsData_Filter();
        $filter->setFieldName($eventName);
        $filter->setInListFilter(new \Google_Service_AnalyticsData_InListFilter(["values" => $filters]));

        // Tạo FilterExpression
        $filterExpression = new \Google_Service_AnalyticsData_FilterExpression();
        $filterExpression->setFilter($filter);

        return $filterExpression;
    }

    private function __getObjectMetrics()
    {
        $metricObjects = [];
        $metrics       = array_filter($this->metrics);

        foreach ($metrics as $metric) {
            $metricObjects[] = new \Google_Service_AnalyticsData_Metric(['name' => $metric]);
        }

        return $metricObjects;
    }

    private function __getObjectDimensions()
    {
        $dimensionObjects = [];
        $dimensions       = array_filter($this->dimensions);

        foreach ($dimensions as $dimension) {
            $dimensionObjects[] = new \Google_Service_AnalyticsData_Dimension(['name' => $dimension]);
        }

        return $dimensionObjects;
    }
}
