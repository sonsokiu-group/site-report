<?php

namespace Workable\SiteReport\Core\Event;

use Workable\SiteReport\Core\Event\traits\ObjectEventBuilderTrait;

class GoogleAnalyticsEvent
{
    protected $client;
    private $propertyId = 0;
    private $streamId = 0;
    private $date;
    private $dimensions = [];
    private $metrics = [];
    private $filterEvent = [];

    use ObjectEventBuilderTrait;

    public function __construct()
    {
        $this->client = new \Google_Client();
        $this->client->addScope(\Google_Service_AnalyticsData::ANALYTICS_READONLY);
        $this->_setConfigAll();
    }

    private function _setConfigAll()
    {
        $this->setDate();
        $this->setAuthConfig(public_path("credential.json"));
    }

    public function setDate($startDate = "", $endDate = "")
    {
        $startDate = $startDate ?: now()->format("Y-m-d");
        $endDate   = $endDate ?: $startDate;

        $date = [
            'start_date' => $startDate,
            'end_date'   => $endDate,
        ];

        $this->date = $date;

        return $this;
    }

    public function setAuthConfig($path = "")
    {
        $this->client->setAuthConfig($path);

        return $this;
    }

    public function setPropertyID($propertyId)
    {
        $this->propertyId = $propertyId;

        return $this;
    }

    public function setStreamID($streamId)
    {
        $this->streamId = $streamId;

        return $this;
    }

    public function setFilterEvent($data = [])
    {
        if (!is_array($data)) {
            $data = [$data];
        }

        $this->filterEvent = $data;

        return $this;
    }


    public function setMetrics($data = "")
    {
        if (!is_array($data)) {
            $data = [$data];
        }

        $this->metrics = $data;

        return $this;
    }

    public function setDimensions($data = "")
    {
        if (!is_array($data)) {
            $data = [$data];
        }

        $this->dimensions = $data;

        return $this;
    }

    public function get()
    {
        try {
            $analyticsData = new \Google_Service_AnalyticsData($this->client);
            $requestBody   = new \Google_Service_AnalyticsData_RunReportRequest();

            $metricObjects    = $this->__getObjectMetrics();
            $dimensionObjects = $this->__getObjectDimensions();

            $filterExpression = $this->__getAnalystFilterEventGroup();

            if (!empty($filterExpression)) {
                $requestBody->setDimensionFilter($filterExpression);
            }

            $requestBody->setMetrics($metricObjects);
            $requestBody->setDimensions($dimensionObjects);
            $requestBody->setDateRanges([new \Google_Service_AnalyticsData_DateRange($this->date)]);

            $response = $analyticsData->properties
                ->runReport("properties/$this->propertyId", $requestBody)
                ->getRows();

        } catch (\Exception $e) {
            $response = $e->getMessage();
        }

        return $response;
    }
}
