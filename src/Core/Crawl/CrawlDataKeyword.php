<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 5/14/24
 * Time: 16:53
 */

namespace Workable\SiteReport\Core\Crawl;

use Illuminate\Support\Arr;
use Workable\Base\Supports\CliEcho;
use Workable\SiteReport\Core\Contracts\CrawlDataKeywordAbstract;
use Workable\SiteReport\Core\Entity\ExtractLocationProcess;
use Workable\SiteReport\Core\Entity\ExtractKeywordProcess;
use Workable\SiteReport\Core\Entity\KeywordLevelProcess;
use Workable\SiteReport\Core\Entity\KeywordPublicProcess;
use Workable\SiteReport\Core\Entity\LocationPublicProcess;

class CrawlDataKeyword extends CrawlDataKeywordAbstract
{
    public function process($paramQuery = [], $header = [])
    {
//                $this->__logging(Arr::get($paramQuery, 'process'), $this->country);
        $results     = $this->_requestGet($this->url, $paramQuery);
        if (isset($results["code"]) && $results["code"] === -1)
        {
            \CliEcho::warning($results["message"]);
            return;
        }

        $dataItems   = $results['data'] ?? [];
        $totalRecord = count($dataItems);
        if (!$totalRecord) return;

        $this->__processData($dataItems, Arr::get($paramQuery, 'process'), $paramQuery);
    }

    private function __processData($dataItems, $process, $paramQuery)
    {
        $processData = null;

        switch ($process)
        {
            case "keyword_level":
                $processData = new KeywordLevelProcess();
                break;

            case "keyword_public":
                $processData = new KeywordPublicProcess();
                break;

            case "location_public":
                $processData = new LocationPublicProcess();
                break;

            case "extract_keyword":
                $processData = new ExtractKeywordProcess();
                break;

            case "extract_location":
                $processData = new ExtractLocationProcess();
                break;

            default:
                break;
        }

        if ($processData)
        {
            $processData->setParamQuery($paramQuery)->process($dataItems);
        }
        else
        {
            \CliEcho::errornl('Cannot get process');
        }

    }

    private function __log($message)
    {
        \CliEcho::info($message);
    }

    private function __logging($process, $country)
    {
        $message = "-- Get data keyword: " . $process . " for site " . $country;
        $this->__log($message);
    }
}