<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 5/14/24
 * Time: 15:40
 */

namespace Workable\SiteReport\Core;

use Workable\SiteReport\Core\Crawl\CrawlDataKeyword;

class CrawlDataKeywordManager
{
    protected $country;

    public function setCountry($country): CrawlDataKeywordManager
    {
        $this->country = $country;
        return $this;
    }

    public function run($sourceCrawl = [], $paramQuery = [], $header = [])
    {
        $urlApi      = $sourceCrawl['api'];
        $accessToken = $sourceCrawl['access_token'];

        $crawlDataKeyword = new CrawlDataKeyword();
        $crawlDataKeyword->setCountry($this->country)
            ->setSourceCrawl($sourceCrawl)
            ->setUrl($urlApi, $accessToken)
            ->process($paramQuery, $header);
    }

}