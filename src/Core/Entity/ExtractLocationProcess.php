<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 5/15/24
 * Time: 09:01
 */

namespace Workable\SiteReport\Core\Entity;

use Illuminate\Support\Arr;
use Workable\SiteReport\Core\Contracts\ProcessDataKeywordAbstract;
use Workable\SiteReport\Enum\ReportKeywordEnum;

class ExtractLocationProcess extends ProcessDataKeywordAbstract
{
    public function process($data = [])
    {
        $column  = 'd_' . $this->getDay();

        $dataProcess = [
            'count_jd'      => $data['jd'] ?? 0,
            'count_jd_done' => $data['jd_success'] ?? 0,
            'state'         => $data['state'] ?? 0,
            'city'          => $data['city'] ?? 0,
            'county'        => $data['county'] ?? 0,
        ];

        $dataSearch = [
            "site_id" => $this->getSiteId(),
            "year"    => $this->getYear(),
            "month"   => $this->getMonth(),
            "type"    => ReportKeywordEnum::TYPE_EXTRACT_LOCATION,
        ];

        foreach ($dataProcess as $key => $value)
        {
            $dataSearch['key_type'] = $key;

            $dataStore = [
                $column      => $value,
                "updated_at" => now(),
            ];

            $this->updateOrInsert($dataSearch, $dataStore);
        }
    }
}