<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 5/15/24
 * Time: 09:00
 */

namespace Workable\SiteReport\Core\Entity;

use Workable\SiteReport\Core\Contracts\ProcessDataKeywordAbstract;
use Workable\SiteReport\Enum\ReportKeywordEnum;

class ExtractKeywordProcess extends ProcessDataKeywordAbstract
{
    public function process($data = [])
    {
        if (count($data) === 0)
        {
            return;
        }

        $dataProcess = [
            'count_jd'      => $data['jd'] ?? 0,
            'count_jd_done' => $data['jd_success'] ?? 0,
        ];

        $column = 'd_' . $this->getDay();

        $dataSearch = [
            "site_id" => $this->getSiteId(),
            "year"    => $this->getYear(),
            "month"   => $this->getMonth(),
            "type"    => ReportKeywordEnum::TYPE_EXTRACT_KEYWORD,
        ];

        foreach ($dataProcess as $key => $value)
        {
            $dataSearch['key_type'] = $key;

            $dataStore = [
                $column      => $value,
                "updated_at" => now(),
            ];

            $this->updateOrInsert($dataSearch, $dataStore);
        }
    }

}