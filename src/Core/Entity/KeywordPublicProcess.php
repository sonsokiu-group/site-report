<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 5/15/24
 * Time: 08:59
 */

namespace Workable\SiteReport\Core\Entity;


use Workable\SiteReport\Core\Contracts\ProcessDataKeywordAbstract;
use Workable\SiteReport\Enum\ReportKeywordEnum;

class KeywordPublicProcess extends ProcessDataKeywordAbstract
{
    public function process($data = [])
    {
        $totalKeywordPublic = $data["total"];
        $column             = 'd_' . $this->getDay();

        $dataSearch = [
            "site_id" => $this->getSiteId(),
            "type"    => ReportKeywordEnum::TYPE_KEYWORD_PUBLIC,
            "year"    => $this->getYear(),
            "month"   => $this->getMonth()
        ];

        $dataStore = [
            $column      => $totalKeywordPublic,
            "updated_at" => now(),
        ];

        $this->updateOrInsert($dataSearch, $dataStore);
    }
}