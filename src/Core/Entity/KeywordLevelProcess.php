<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 5/15/24
 * Time: 08:59
 */

namespace Workable\SiteReport\Core\Entity;


use Workable\SiteReport\Core\Contracts\ProcessDataKeywordAbstract;
use Workable\SiteReport\Enum\ReportKeywordEnum;

class KeywordLevelProcess extends ProcessDataKeywordAbstract
{
    public function process($data = [])
    {
        $dataSearch = [
            "site_id" => $this->getSiteId(),
            "type"    => ReportKeywordEnum::TYPE_KEYWORD_LEVEL
        ];

        foreach ($data as $key => $value)
        {
            if (!in_array($key, ReportKeywordEnum::KEYWORD_LEVEL_KEY_TYPE))
            {
                continue;
            }

            $dataSearch["key_type"] = $key;

            $dataStore = [
                'meta'       => json_encode(["total" => $value]),
                'updated_at' => now(),
                'year'       => $this->getYear(),
                'month'      => $this->getMonth()
            ];

            $this->updateOrInsert($dataSearch, $dataStore);
        }
    }
}