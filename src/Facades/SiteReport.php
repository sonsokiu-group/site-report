<?php

namespace Workable\SiteReport\Facades;

use Illuminate\Support\Facades\Facade;

class SiteReport extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "site-report";
    }
}
