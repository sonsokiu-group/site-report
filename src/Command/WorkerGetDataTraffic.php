<?php

namespace Workable\SiteReport\Command;

use Workable\RequestReport\Console\Abstracts\ReportCommandBaseAbstract;
use Workable\SiteReport\Command\Traits\FuncGetDataTrafficTrait;
use Workable\SiteReport\Core\Event\GoogleAnalyticsEvent;
use Workable\SiteReport\Enum\ReportTrafficTypeEnum;

class WorkerGetDataTraffic extends ReportCommandBaseAbstract
{
    protected $signature = 'worker:get-data-traffic
                            {--time= : The time like 1-1-2023}
                            {--type= : include m, y, m-y}';

    protected $description = 'Command thu thập dữ liệu event từ Google Analytics';
    private $googleAnalyticsEvent;
    private $configSites;

    public function __construct()
    {
        parent::__construct();
        $this->googleAnalyticsEvent = new GoogleAnalyticsEvent();
        $this->configSites          = config('packages.site-report.api');
    }

    use FuncGetDataTrafficTrait;

    public function handle()
    {
        $this->info('-- Collect data start --');

        $time = $this->option("time") ?? now()->subDays()->format("d-m-Y");
        $type = $this->option("type") ?? 'd-m-y';

        $this->_processTypeDate($type, $time);

        $this->warn('Handle data now...');
        $this->info('-- Collect data finish --');
    }

    protected function _handleDataByTime($arrTime = [])
    {
        list($day, $month, $year) = $arrTime;
        $day = (int)$day;
        $this->setDay($day);
        $this->setMonth($month);
        $this->setYear($year);

        $events       = $this->__getDataEventFromGA4();
        $dataDetail   = $events["detail"] ?? [];
        $dataOverview = $events["overview"] ?? [];

//      Thêm dữ liệu thống kê bảng chi tiết sites_traffic
        $this->__syncDataForDetail($dataDetail);

//      Thêm dữ liệu thống kê bảng site_traffic_overview
        $this->__syncDataForOverview($dataOverview);
    }

    private function __getDataEventFromGA4()
    {
        $this->info("-- Start sync data from GA4 in date: " . $this->getDate());

        $dataRtn       = [];
        $startTimeSync = microtime(true);
        $filterEvents  = array_keys(ReportTrafficTypeEnum::CONFIG_KEY_REDIRECT);

        $analyticsData = $this->googleAnalyticsEvent
            ->setDate($this->getDate())
            ->setFilterEvent($filterEvents)
            ->setPropertyID($this->configSites['property_id'])
            ->setDimensions(['eventName', 'date', 'sessionMedium', 'firstUserSource', 'deviceCategory', 'hostName', 'city', 'country', 'streamId'])
            ->setMetrics("activeUsers")
            ->get();

        $endTimeSync = microtime(true);

        if (!is_array($analyticsData)) {
            return $dataRtn;
        }

        $startTimeBuild = microtime(true);

        foreach ($analyticsData as $data) {
            $event     = $data->dimensionValues[0]->value;
            $streamId  = $data->dimensionValues[8]->value;
            $siteId    = $this->__findSiteIdByStreamId($this->configSites['list_stream_id'], $streamId);
            $eventName = ReportTrafficTypeEnum::CONFIG_KEY_REDIRECT[$event];

            if (!$siteId) {
                continue;
            }

            $referer = $data->dimensionValues[3]->value;
            $referer = $this->__classifyReferer($referer);
            $number  = $data->metricValues[0]->value;

            $dataRtn[$siteId][] = [
                "event"   => $eventName,
                "referer" => $referer,
                "number"  => (int)$number,
            ];
        }

//        Build gộp các event riêng lẻ thành mảng chứa thông tin event + số lượng
        $dataRtn = $this->__buildEvent($dataRtn);

        $endTimeBuild = microtime(true);

        $this->warn(print_r($dataRtn, true));
        $this->info("Execution time to sync GA4: " . (($endTimeSync - $startTimeSync) * 1000) . " ms");
        $this->info("Execution time to building event: " . (($endTimeBuild - $startTimeBuild) * 1000) . " ms");
        $this->info("-- Stop sync GA4");

        return $dataRtn;
    }

    private function __syncDataForDetail($dataDetail = [])
    {
        if (!$dataDetail) {
            return;
        }

        foreach ($dataDetail as $siteID => $event) {
            foreach ($event as $eventDetail) {
                $this->__incrementData(
                    $eventDetail["event"],
                    $eventDetail["referer"],
                    $eventDetail["number"],
                    $siteID
                );
            }
        }
    }

    private function __syncDataForOverview($dataOverview = [])
    {
        if (!$dataOverview) {
            return;
        }

        foreach ($dataOverview as $siteID => $number) {
            $this->__incrementDataOverview($number, $siteID);
        }
    }
}
