<?php

namespace Workable\SiteReport\Command\Traits;

use Illuminate\Support\Facades\DB;

trait FuncGetDataTrafficTrait
{
    private function __findSiteIdByStreamId($listStreamId, $streamId)
    {
        foreach ($listStreamId as $site) {
            if ($site['stream_id'] == $streamId) {
                $country = $site['country'];

                return DB::table('sites')
                    ->where('country', $country)
                    ->first()->id;
            }
        }

        return null;
    }

    private function __incrementData($eventName, $referer, $number, $siteId)
    {
        if (!$siteId) {
            return;
        }

        $dayCol = 'd_' . $this->getDay();

        $dataSearch = [
            'site_id'  => $siteId,
            'referer'  => $referer,
            'redirect' => $eventName,
            'year'     => $this->getYear(),
            'month'    => $this->getMonth(),
        ];

        $dataSave = [
            $dayCol      => $number,
            "updated_at" => now()->toDateTimeString()
        ];

        DB::table('sites_traffic')->updateOrInsert($dataSearch, $dataSave);
    }

    private function __classifyReferer($refererName)
    {
        $sources = [
            'google'    => 'google',
            'facebook'  => 'facebook',
            'fb'        => 'facebook',
            'linkedin'  => 'linkedin',
            'twitter'   => 'twitter',
            'instagram' => 'instagram',
            'pinterest' => 'pinterest',
            'tiktok'    => 'tiktok',
            'youtube'   => 'youtube'
        ];

        foreach ($sources as $source => $value) {
            if (strpos($refererName, $source) !== false) {
                return $value;
            }
        }

        return 'other';
    }

    private function __incrementDataOverview($dataOverview, $siteId)
    {
        if (!$siteId) {
            return;
        }

        $dayCol = 'd_' . $this->getDay();

        $dataSearch = [
            'site_id' => $siteId,
            'year'    => $this->getYear(),
            'month'   => $this->getMonth(),
        ];

        $dataSave = [
            $dayCol      => $dataOverview,
            "updated_at" => now()->toDateTimeString()
        ];

        DB::table('site_traffic_overview')->updateOrInsert($dataSearch, $dataSave);
    }

    private function __buildEvent($events)
    {
        $dataRtn = [];

        foreach ($events as $siteId => $value) {
            foreach ($value as $eventData) {
                $event   = $eventData['event'];
                $referer = $eventData['referer'];
                $number  = $eventData['number'];

                if (!isset($dataRtn["detail"][$siteId])) {
                    $dataRtn["detail"][$siteId]   = [];
                    $dataRtn["overview"][$siteId] = 0;
                }

                $dataRtn["overview"][$siteId] += $number;

                $found = false;
                foreach ($dataRtn["detail"][$siteId] as &$existingEventData) {
                    if ($existingEventData['event'] === $event && $existingEventData['referer'] === $referer) {
                        $existingEventData['number'] += $number;
                        $found                       = true;
                        break;
                    }
                }

                if (!$found) {
                    $dataRtn["detail"][$siteId][] = $eventData;
                }
            }
        }

        return $dataRtn;
    }
}
