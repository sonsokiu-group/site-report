<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 5/14/24
 * Time: 14:49
 */

namespace Workable\SiteReport\Command;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Collect\Core\Traits\ConfigApiCrawlTrait;
use Workable\SiteReport\Core\CrawlDataKeywordManager;

class WorkerCrawlDataKeywordCommand extends Command
{
    use ConfigApiCrawlTrait;

    protected $signature = 'worker-get-data:keyword {source} {process}
                            {--country=all : --country like us,uk,ca,..}
                            {--time=now : --time format Y/m/d like 2024/01/15 or now}';

    protected $description = 'Worker get data keyword for table keyword_statistics';

    protected $paramQuery = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info("-- Start run WorkerCrawlDataKeywordCommand");

        $this->__initQuery()
            ->__runCommand();

        $this->info("-- End run WorkerCrawlDataKeywordCommand");
    }

    private function __runCommand()
    {
        $country = $this->option('country');

        if ($country === 'all')
        {
            $this->__runMultiCountry();
        }
        else
        {
            $this->__runByCountry($country);
        }
    }

    private function __runMultiCountry()
    {
        $sites                   = DB::table('sites')->where('status', 1)->get(); // 1: status active
        $crawlDataKeywordManager = new CrawlDataKeywordManager();

        foreach ($sites as $site)
        {
            $this->warn("Process site: " . $site->site_name);
            $startTime = microtime(true);

            $country                     = $site->country;
            $this->paramQuery['site']    = $country;
            $this->paramQuery['site_id'] = $site->id;
            $sourceData                  = $this->getConfigSpider($country, $this->paramQuery['source'], $this->paramQuery['process']);

            $crawlDataKeywordManager->setCountry($country)
                ->run($sourceData, $this->paramQuery);

            $this->__dumpTimeExecute($startTime);
        }
    }

    private function __runByCountry($country): void
    {
        $site = DB::table("sites")
            ->where('status', 1) // 1: status active
            ->where('country', $country)->first();

        if (!$site)
        {
            $this->warn("Does not exist country: " . $country);
            return;
        }
        $this->warn("Process site: " . $site->site_name);
        $startTime = microtime(true);

        $this->paramQuery['site_id'] = $site->id;
        $sourceData                  = $this->getConfigSpider($country, $this->paramQuery['source'], $this->paramQuery['process']);

        $crawlDataKeywordManager = new CrawlDataKeywordManager();
        $crawlDataKeywordManager->setCountry($country)
            ->run($sourceData, $this->paramQuery);

        $this->__dumpTimeExecute($startTime);
    }

    private function __initQuery(): WorkerCrawlDataKeywordCommand
    {
        $source      = $this->argument('source');
        $process     = $this->argument('process');
        $country     = $this->option('country');
        $time        = $this->option('time');
        $timeCurrent = ($time === 'now') ? Carbon::now() : Carbon::parse($time);

        $this->paramQuery['source']  = $source;
        $this->paramQuery['process'] = $process;
        $this->paramQuery['site']    = $country;
        $this->paramQuery['year']    = $timeCurrent->year;
        $this->paramQuery['month']   = $timeCurrent->month;
        $this->paramQuery['day']     = $timeCurrent->day;

        return $this;
    }

    private function __dumpTimeExecute($startTime)
    {
        $timeExecute = (microtime(true) - $startTime) * 1000 . " ms";
        $this->info("-- Time process: " . $timeExecute);
    }
}

# php artisan worker-get-data:keyword analyst keyword_level --country=all --time=now
# php artisan worker-get-data:keyword analyst keyword_public --country=all --time=now
# php artisan worker-get-data:keyword analyst location_public --country=all --time=now
# php artisan worker-get-data:keyword analyst extract_keyword --country=all --time=now
# php artisan worker-get-data:keyword analyst extract_location --country=all --time=now