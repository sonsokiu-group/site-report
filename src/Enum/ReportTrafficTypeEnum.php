<?php

namespace Workable\SiteReport\Enum;

class ReportTrafficTypeEnum
{
    const CHART_TRAFFIC_HOME   = 1;
    const CHART_GROUP_REDIRECT = 2;
    const CHART_GROUP_REFERER  = 3;

    const CHART_GROUP_REDIRECT_KEYWORD   = 4;
    const CHART_GROUP_REDIRECT_CATEGORY  = 5;
    const CHART_GROUP_REDIRECT_REMOTE    = 6;
    const CHART_GROUP_REDIRECT_COMPANY   = 7;
    const CHART_GROUP_REDIRECT_HOME      = 8;
    const CHART_GROUP_REDIRECT_DETAIL    = 9;
    const CHART_GROUP_REDIRECT_LOCATION  = 10;
    const CHART_GROUP_REDIRECT_MAIN      = 11;
    const CHART_GROUP_REDIRECT_WORK_TYPE = 12;

    const MAPPING_KEY_CHART_GROUP = [
        self::CHART_GROUP_REDIRECT_KEYWORD   => "keyword",
        self::CHART_GROUP_REDIRECT_CATEGORY  => "category",
        self::CHART_GROUP_REDIRECT_REMOTE    => "remote",
        self::CHART_GROUP_REDIRECT_COMPANY   => "company",
        self::CHART_GROUP_REDIRECT_HOME      => "home",
        self::CHART_GROUP_REDIRECT_DETAIL    => "detail",
        self::CHART_GROUP_REDIRECT_LOCATION  => "location",
        self::CHART_GROUP_REDIRECT_WORK_TYPE => "work_type"
    ];

    const CONFIG_TYPE_CHART = [
        self::CHART_TRAFFIC_HOME            => [
            "columns" => [
                [
                    "name" => "traffic",
                ]
            ],
            "table"   => "site_traffic_overview"
        ],
        self::CHART_GROUP_REDIRECT          => [
            "columns" => [
                [
                    "name" => "detail",
                ],
                [
                    "name" => "home",
                ],
                [
                    "name" => "keyword",
                ],
                [
                    "name" => "category",
                ],
                [
                    "name" => "remote",
                ],
                [
                    "name" => "work_type",
                ],
                [
                    "name" => "company",
                ]
            ],
            "table"   => "sites_traffic"
        ],
        self::CHART_GROUP_REFERER           => [
            "columns" => [
                [
                    "name" => "google",
                ],
                [
                    "name" => "facebook",
                ],
                [
                    "name" => "other",
                ],
            ],
            "table"   => "sites_traffic"
        ],
        self::CHART_GROUP_REDIRECT_KEYWORD  => [
            "columns" => [
                [
                    "name" => "keyword",
                ],
                [
                    "name" => "keyword_state",
                ],
                [
                    "name" => "keyword_area",
                ],
                [
                    "name" => "keyword_city",
                ],
                [
                    "name" => "keyword_region",
                ],
                [
                    "name" => "keyword_county",
                ],

            ],
            "table"   => "sites_traffic"
        ],
        self::CHART_GROUP_REDIRECT_CATEGORY => [
            "columns" => [
                [
                    "name" => "category",
                ],
                [
                    "name" => "category_state",
                ],
                [
                    "name" => "category_area",
                ],
                [
                    "name" => "category_city",
                ],
                [
                    "name" => "category_region",
                ],
                [
                    "name" => "category_county",
                ]
            ],
            "table"   => "sites_traffic"
        ],
        self::CHART_GROUP_REDIRECT_REMOTE   => [
            "columns" => [
                [
                    "name" => "remote",
                ]
            ],
            "table"   => "sites_traffic"
        ],
        self::CHART_GROUP_REDIRECT_COMPANY  => [
            "columns" => [
                [
                    "name" => "company",
                ],
                [
                    "name" => "company_state",
                ],
                [
                    "name" => "company_city",
                ]
            ],
            "table"   => "sites_traffic"
        ],
        self::CHART_GROUP_REDIRECT_HOME     => [
            "columns" => [
                [
                    "name" => "home",
                ]
            ],
            "table"   => "sites_traffic"
        ],
        self::CHART_GROUP_REDIRECT_DETAIL   => [
            "columns" => [
                [
                    "name" => "detail",
                ]
            ],
            "table"   => "sites_traffic"
        ],
        self::CHART_GROUP_REDIRECT_LOCATION => [
            "columns" => [
                [
                    "name" => "location_state",
                ],
                [
                    "name" => "location_city",
                ],
                [
                    "name" => "location_area",
                ],
                [
                    "name" => "location_county",
                ],
                [
                    "name" => "location_region",
                ]
            ],
            "table"   => "sites_traffic"
        ],
        self::CHART_GROUP_REDIRECT_MAIN     => [
            "columns" => [
                [
                    "name" => "home",
                ],
                [
                    "name" => "company",
                ],
                [
                    "name" => "company_state",
                ],
                [
                    "name" => "company_city",
                ],
                [
                    "name" => "detail",
                ]
            ],
            "table"   => "sites_traffic"
        ]
    ];

    const CONFIG_KEY_CHART_OVERVIEW = [
        self::CHART_TRAFFIC_HOME            => "Tổng traffic",
        self::CHART_GROUP_REDIRECT_KEYWORD  => "Nhóm keyword",
        self::CHART_GROUP_REDIRECT_CATEGORY => "Nhóm category",
        self::CHART_GROUP_REDIRECT_REMOTE   => "Nhóm remote",
        self::CHART_GROUP_REDIRECT_MAIN     => "Nhóm Chính",
        self::CHART_GROUP_REDIRECT_LOCATION => "Nhóm location"
    ];

    const CONFIG_KEY_REDIRECT = [
        'VIEW_DETAIL' => 'detail',
        'VIEW_HOME'   => 'home',

        'VIEW_COMPANY'       => 'company',
        'VIEW_COMPANY_STATE' => 'company_state',
        'VIEW_COMPANY_CITY'  => 'company_city',

        'VIEW_KEYWORD'        => 'keyword',
        'VIEW_KEYWORD_STATE'  => 'keyword_state',
        'VIEW_KEYWORD_COUNTY' => 'keyword_county',
        'VIEW_KEYWORD_CITY'   => 'keyword_city',
        'VIEW_KEYWORD_REGION' => 'keyword_region',
        'VIEW_KEYWORD_AREA'   => 'keyword_area',

        "VIEW_CATEGORY"        => 'category',
        'VIEW_CATEGORY_STATE'  => 'category_state',
        'VIEW_CATEGORY_COUNTY' => 'category_county',
        'VIEW_CATEGORY_CITY'   => 'category_city',
        'VIEW_CATEGORY_REGION' => 'category_region',
        'VIEW_CATEGORY_AREA'   => 'category_area',

        'VIEW_REMOTE'    => 'remote',
        'VIEW_WORK_TYPE' => 'work_type',

        'VIEW_LOCATION_STATE'  => 'location_state',
        'VIEW_LOCATION_COUNTY' => 'location_county',
        'VIEW_LOCATION_CITY'   => 'location_city',
        'VIEW_LOCATION_REGION' => 'location_region',
        'VIEW_LOCATION_AREA'   => 'location_area',
    ];
}
