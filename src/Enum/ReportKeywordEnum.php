<?php
/**
 * Created by PhpStorm.
 * User: Sonsokiu
 * Date: 3/13/24
 * Time: 10:24
 */

namespace Workable\SiteReport\Enum;

class ReportKeywordEnum
{
    const TYPE_KEYWORD_LEVEL    = "keyword_level";
    const TYPE_KEYWORD_PUBLIC   = "keyword_public";
    const TYPE_LOCATION_PUBLIC  = "location_public";
    const TYPE_EXTRACT_KEYWORD  = "extract_keyword";
    const TYPE_EXTRACT_LOCATION = "extract_location";
    const ARRAY_TYPE = [
        self::TYPE_KEYWORD_LEVEL,
        self::TYPE_KEYWORD_PUBLIC,
        self::TYPE_LOCATION_PUBLIC,
        self::TYPE_EXTRACT_KEYWORD,
        self::TYPE_EXTRACT_LOCATION,
    ];

    const KEYWORD_LEVEL_KEY_TYPE = [
        'kw0',
        'kw1',
        'kw2',
        'kw3',
        'kw4',
        'kw5',
    ];

    const KEYWORD_PUBLIC_KEY_TYPE = [
        'keyword_public'
    ];

    const LOCATION_PUBLIC_KEY_TYPE = [
        'location_public'
    ];

    const EXTRACT_KEYWORD_KEY_TYPE = [
        'count_jd',
        'count_jd_done'
    ];

    const EXTRACT_LOCATION_KEY_TYPE = [
        'count_jd',
        'count_jd_done',
        'state',
        'city',
        'county'
    ];

}
