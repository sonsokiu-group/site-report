<?php

return [
    'keyword_level'    => [
        'active'       => 1,
        'host'         => env('REPORTER_HOST_KEYWORD_LEVEL', 'http://210.245.74.97:5301'),
        'access_token' => env('REPORTER_TOKEN_KEYWORD_LEVEL', 'YRMHetweG7VTEryR4OonSN7XYVC5uJjNeheX3jTiUs'),
        'api'          => env('REPORTER_URI_KEYWORD_LEVEL', '/api-report/get-data-keyword-reporter')
    ],
    'keyword_public'   => [
        'active'       => 1,
        'host'         => env('REPORTER_HOST_KEYWORD_PUBLIC', 'http://210.245.74.97:5301'),
        'access_token' => env('REPORTER_TOKEN_KEYWORD_PUBLIC', 'YRMHetweG7VTEryR4OonSN7XYVC5uJjNeheX3jTiUs'),
        'api'          => env('REPORTER_URI_KEYWORD_PUBLIC', '/api-report/get-data-keyword-reporter')
    ],
    'location_public'  => [
        'active'       => 1,
        'host'         => env('REPORTER_HOST_LOCATION_PUBLIC', 'http://210.245.74.97:5301'),
        'access_token' => env('REPORTER_TOKEN_LOCATION_PUBLIC', 'YRMHetweG7VTEryR4OonSN7XYVC5uJjNeheX3jTiUs'),
        'api'          => env('REPORTER_URI_LOCATION_PUBLIC', '/api-report/get-data-keyword-reporter')
    ],
    'extract_keyword'    => [
        'active'       => 1,
        'host'         => env('REPORTER_HOST_EXTRACT_KEYWORD', 'http://210.245.74.97:5301'),
        'access_token' => env('REPORTER_TOKEN_EXTRACT_KEYWORD', 'YRMHetweG7VTEryR4OonSN7XYVC5uJjNeheX3jTiUs'),
        'api'          => env('REPORTER_URI_EXTRACT_KEYWORD', '/api-report/get-data-keyword-reporter')
    ],
    'extract_location' => [
        'active'       => 1,
        'host'         => env('REPORTER_HOST_EXTRACT_LOCATION', 'http://210.245.74.97:5301'),
        'access_token' => env('REPORTER_TOKEN_EXTRACT_LOCATION', 'YRMHetweG7VTEryR4OonSN7XYVC5uJjNeheX3jTiUs'),
        'api'          => env('REPORTER_URI_EXTRACT_LOCATION', '/api-report/get-data-keyword-reporter')
    ]
];