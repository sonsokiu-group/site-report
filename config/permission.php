<?php

return [
    "site-report" => [
        [
            'title'        => 'Báo cáo traffic',
            'name'         => 'get.traffic_home.index',
            'uri'          => '/site-report/traffic/index',
            'show_sidebar' => 1,
            'menu'         => [
//                [
//                    'title'        => 'Tổng quan',
//                    'name'         => 'get.traffic_home.index',
//                    'uri'          => '/site-report/traffic/index',
//                    'show_sidebar' => 1,
//                ],
            ]
        ],
        [
            'title'        => 'Báo cáo keyword',
            'name'         => 'get.keyword_level.index',
            'uri'          => '/site-report/keyword-level/',
            'show_sidebar' => 1,
            'menu'         => [
                [
                    'title'        => 'Keyword level',
                    'name'         => 'get.keyword_level.index',
                    'uri'          => '/site-report/keyword-level/index',
                    'show_sidebar' => 1,
                ],
                [
                    'title'        => 'Keyword public',
                    'name'         => 'get.keyword_public.index',
                    'uri'          => '/site-report/keyword-public/index',
                    'show_sidebar' => 1,
                ],
                [
                    'title'        => 'Location public',
                    'name'         => 'get.location_public.index',
                    'uri'          => '/site-report/location-public/index',
                    'show_sidebar' => 1,
                ]
            ]
        ],
        [
            'title'        => 'Dữ liệu bóc tách',
            'name'         => 'get.keyword_extract.index',
            'uri'          => '/site-report/keyword-extract/index',
            'show_sidebar' => 1,
            'menu'         => []
        ],
    ]

];
