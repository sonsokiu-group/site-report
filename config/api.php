<?php

return [
    "property_id" => 367408029,
    "list_stream_id" => [
        [
            "stream_id" => 4990518449,
            "country" => "us"
        ],
        [
            "stream_id" => 7595922117,
            "country" => "au"
        ],
        [
            "stream_id" => 7595987131,
            "country" => "uk"
        ],
        [
            "stream_id" => 7596218785,
            "country" => "se"
        ],
        [
            "stream_id" => 7596247054,
            "country" => "fr"
        ],
        [
            "stream_id" => 7596312154,
            "country" => "fi"
        ],
        [
            "stream_id" => 7596345685,
            "country" => "de"
        ],
        [
            'stream_id' => 7596377296,
            'country' => 'tr'
        ],
        [
            'stream_id' => 7596379745,
            'country' => 'nl'
        ],
        [
            'stream_id' => 7596384994,
            'country' => 'ca'
        ],
        [
            'stream_id' => 7596402064,
            'country' => 'br'
        ],
        [
            'stream_id' => 7596403064,
            'country' => 'mx'
        ],
        [
            'stream_id' => 7596418996,
            'country' => 'es'
        ],
        [
            'stream_id' => 7596432027,
            'country' => 'it'
        ],
        [
            'stream_id' => 7596449347,
            'country' => 'ar'
        ],
        [
            'stream_id' => 7596451152,
            'country' => 'jp'
        ],
        [
            'stream_id' => 7596451898,
            'country' => 'in'
        ],
        [
            'stream_id' => 7596459720,
            'country' => 'id'
        ]
    ]
];
