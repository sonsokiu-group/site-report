const mix = require('laravel-mix'),
    pathBundle = 'public/vendor/site-report/';

mix.setPublicPath(pathBundle);

let build_scss = [
    {
        from: '/resources/assets/scss/report.scss',
        to: 'css/report.css'
    }
];

let build_js = [
    {
        from: '/resources/assets/js/libraries/chartJs.js',
        to: 'js/chartJs.js'
    },
    {
        from: '/resources/assets/js/pages/traffic_home.js',
        to: 'js/traffic_home.js'
    },
    {
        from: '/resources/assets/js/pages/traffic_detail_overview.js',
        to: 'js/traffic_detail_overview.js'
    },
    {
        from: '/resources/assets/js/pages/traffic_detail_redirect.js',
        to: 'js/traffic_detail_redirect.js'
    },
    {
        from: '/resources/assets/js/pages/traffic_detail_referer.js',
        to: 'js/traffic_detail_referer.js'
    },
    {
        from: '/resources/assets/js/pages/keyword/kw_level_home.js',
        to: 'js/kw_level_home.js'
    },
    {
        from: '/resources/assets/js/pages/keyword/kw_public_home.js',
        to: 'js/kw_public_home.js'
    },
    {
        from: '/resources/assets/js/pages/keyword/location_public_home.js',
        to: 'js/location_public_home.js'
    },
    {
        from: '/resources/assets/js/pages/keyword/kw_extract_home.js',
        to: 'js/kw_extract_home.js'
    },
    {
        from: '/resources/assets/js/pages/keyword/kw_extract_detail.js',
        to: 'js/kw_extract_detail.js'
    },
    {
        from: '/resources/assets/js/pages/keyword/extract_detail_overview.js',
        to: 'js/extract_detail_overview.js'
    },
    {
        from: '/resources/assets/js/pages/keyword/location_extract_home.js',
        to: 'js/location_extract_home.js'
    },
    {
        from: '/resources/assets/js/core/render_chart.js',
        to: 'js/render_chart.js'
    },
];

// Run
build_scss.map((val) => {
    let from = __dirname + val.from,
        to = val.to;
    mix.sass(from, to).minify(pathBundle + to)
});

build_js.map((val) => {
    let from = __dirname + val.from,
        to = val.to;
    mix.js(from, to).minify(pathBundle + to)
});

mix.webpackConfig({
    resolve: {
        alias: {
            resources_assets: path.resolve(__dirname + '/../../../resources/assets'),
        }
    }
});

mix.options({
    processCssUrls: false,
    terser: {
        extractComments: false,
    }
}).autoload({
    jquery: ['$', 'window.jQuery', 'jQuery']
});

if (mix.inProduction()) {
    mix.version();
}
