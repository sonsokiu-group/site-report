<?php
Route::group([
  'prefix' => '/api/site-report',
  'namespace' => 'Workable\SiteReport\Http\Controllers\Api',
  'middleware' => ["web"]
], function () {
    Route::get('/', 'SiteReportApiBaseController@index')->name('api_get.site-report.index');
});
