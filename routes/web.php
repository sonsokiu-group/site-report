<?php

use Illuminate\Support\Facades\Route;

// Route for report traffic
Route::group([
    'prefix'     => 'site-report/traffic/',
    'namespace'  => 'Workable\SiteReport\Http\Controllers\Traffic',
    'middleware' => ['web'],
], function ()
{
    Route::get('index', 'TrafficHomeController@index')->name('get.traffic_home.index');
    Route::get('{id}/overview', 'TrafficOverviewController@index')->name('get.traffic_overview.index');
    Route::get('{id}/redirect', 'TrafficRedirectController@index')->name('get.traffic_redirect.index');
    Route::get('{id}/referer', 'TrafficRefererController@index')->name('get.traffic_referer.index');
});

Route::group([
    'prefix'     => 'site-report/request/',
    'namespace'  => 'Workable\SiteReport\Http\Controllers\Request',
    'middleware' => ['web'],
], function ()
{
    Route::get('{id}', 'RobotRequestController@request')->name('get.overview.request');
});

// Route for report keyword
Route::group([
    'prefix'     => 'site-report',
    'namespace'  => 'Workable\SiteReport\Http\Controllers\Keyword',
    'middleware' => ['web'],
], function ()
{
    Route::get('keyword-level/index', 'KeywordLevelController@index')->name('get.keyword_level.index');
    Route::get('keyword-level/detail/{id}', 'KeywordLevelController@detail')->name('get.keyword_level.detail');

    Route::get('keyword-public/index', 'KeywordPublicController@index')->name('get.keyword_public.index');
    Route::get('keyword-public/detail/{id}', 'KeywordPublicController@detail')->name('get.keyword_public.detail');

    Route::get('location-public/index', 'LocationPublicController@index')->name('get.location_public.index');
    Route::get('location-public/detail/{id}', 'LocationPublicController@detail')->name('get.location_public.detail');

    Route::get('keyword-extract/index', 'KeywordExtractController@index')->name('get.keyword_extract.index');
    Route::get('keyword-extract/keyword/detail/{id}', 'KeywordExtractController@keywordDetail')->name('get.keyword_extract.keyword_detail');
    Route::get('keyword-extract/location/detail/{id}', 'KeywordExtractController@locationDetail')->name('get.keyword_extract.location_detail');
    Route::get('keyword-extract/index/detail/{id}', 'KeywordExtractController@overviewDetail')->name('get.keyword_extract.overview_detail');

    Route::get('keyword-extract/keyword', 'KeywordExtractController@keyword')->name('get.keyword_extract.keyword');
    Route::get('keyword-extract/location', 'KeywordExtractController@location')->name('get.keyword_extract.location');
});


Route::group([
    'prefix'     => 'site-report/render/',
    'namespace'  => 'Workable\SiteReport\Http\Controllers\RenderBlade',
    'middleware' => ['web'],
], function ()
{
    Route::get('chart','RenderController@apiDataChart')
        ->name('get.render.chart');

    Route::get('api-chart',"RenderController@apiDataChart")
        ->name('get.render.api-chart');
});
// Route Api, Ajax
Route::group([
    'prefix'     => 'site-report/',
    'namespace'  => 'Workable\SiteReport\Http\Controllers\Ajax',
    'middleware' => ['web'],
], function ()
{

    Route::group([
        'prefix' => '/traffic'
    ], function ()
    {
        // Get data chart line: traffic_home, traffic_overview,...
        Route::get('getDataLineChart', 'TrafficDataController@getDataLineChart')
            ->name('get.traffic_data.getDataLineChart');

        Route::get('getDataPieChart', 'TrafficDataController@getDataPieChart')
            ->name('get.traffic_data.getDataPieChart');

        Route::get('getDataBarChart', 'TrafficDataController@getDataBarChart')
            ->name('get.traffic_data.getDataBarChart');

        // Get Data chart Multi Site in page traffic home
        Route::get('getDataChartAllSite', 'TrafficDataController@getDataChartAllSite')
            ->name('get.traffic_data.getDataChartAllSite');

        Route::get('getDataChartOverview', 'TrafficDataController@getDataChartOverview')
            ->name('get.traffic_data.getDataChartOverview');

        Route::get('getDataChartDetailRedirect', 'TrafficDataController@getDataChartDetailRedirect')
            ->name('get.traffic_data.getDataChartDetailRedirect');

        Route::get('getDataChartDetailReferer', 'TrafficDataController@getDataChartDetailReferer')
            ->name('get.traffic_data.getDataChartDetailReferer');
    });

    Route::group([
        'prefix' => '/keyword'
    ], function ()
    {
        // Get data chart for keyword
        Route::get('chart-keyword-level', 'KeywordDataController@getDataChartKeywordLevel')
            ->name('get.keyword_data.getDataChartKeywordLevel');

        Route::get('chart-keyword-public-home', 'KeywordDataController@chartKeywordPublicHome')
            ->name('get.keyword_data.chartKeywordPublicHome');

        Route::get('chart-overview-extract', 'KeywordDataController@chartOverviewExtract')
            ->name('get.keyword_data.chartOverviewExtract');

        Route::get('chart-location-extract', 'KeywordDataController@chartLocationExtract')
            ->name('get.chart_data.chartLocationExtract');
    });
});
