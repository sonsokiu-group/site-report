<?php
Route::group([
  'prefix' => 'site-report',
  'namespace' => 'Workable\SiteReport\Http\Controllers',
  'middleware' => ["web"]
], function () {
    Route::get('/', 'SiteReportBaseController@index')->name('get.site-report.index');
});
