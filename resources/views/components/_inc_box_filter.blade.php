<div class="box-filter">
    <div class="form-filter pull-left">
        <form action="{{$action}}" class="form-inline js-form-filter-report" method="GET">
            @if(!isset($page))
                Lọc theo thời gian:
            @endif
            <select class="form-control js-change-day-report" name="day">
                <option value="7" {{ form_query('day', $query) == 7 ? 'selected' : '' }}>7 ngày</option>
                <option value="14" {{ form_query('day', $query) == 14 ? 'selected' : '' }}>14 ngày</option>
                <option value="30" {{ form_query('day', $query, 30) == 30 ? 'selected' : '' }}>30 ngày</option>
            </select>

{{--            Date range theo thoi gian lua chon                     --}}
{{--            <input type="hidden" id="start_date_filter" name="start_date" value="{{form_query('start_date', $query)}}">--}}
{{--            <input type="hidden" id="end_date_filter" name="end_date" value="{{form_query('end_date', $query)}}">--}}
{{--            <div class=" d-flex align-center">--}}
{{--                <div id="js-date-sort"--}}
{{--                     style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: auto">--}}
{{--                    Thời gian:--}}
{{--                    <span class="fw-medium"></span>--}}
{{--                </div>--}}
{{--            </div>--}}

            @if( isset($continents))
                <select class="form-control" name="continent">
                    <option value="">-- Châu lục --</option>
                    @foreach($continents as $key=> $continent)
                        <option
                            value="{{$continent['slug']}}" {{ query_selected('continent', $key) }}>{{$continent['name']}}</option>
                    @endforeach
                </select>
            @endif

            @if(isset($page) && $page == "traffic_home")
                    <input type="text" class="form-control" name="site_name" placeholder="Tìm tên quốc gia"
                           value="{{form_query('site_name', $query)}}">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
                    <a href="{{$action}}" class="">
                        <button type="button" class="form-control btn btn-default">Refresh</button>
                    </a>
            @endif

        </form>
    </div>
    <div class="pull-right">
        {{--        <a href="" class="btn btn-success"><i class="fa fa-plus"></i>--}}
        {{--            Thêm mới</a>--}}
    </div>
    <div class="clearfix"></div>
    <br>
</div>
