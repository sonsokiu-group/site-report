<div>
    <div class="nav-tab-main">
        <ul class="nav nav-tabs">
            <li role="presentation" class="{{ (isset($tabMain) && $tabMain == "overview") ? "active" : "" }}">
                <a class="btn btn-md" href="{{ route('get.traffic_overview.index', ['id' => $site->id]) }}">Tổng quan</a>
            </li>
            <li role="presentation" class="{{ (isset($tabMain) && $tabMain == "redirect") ? "active" : "" }}">
                <a class="btn btn-md" href="{{ route('get.traffic_redirect.index', ['id' => $site->id]) }}">Traffic Loại Trang</a>
            </li>
            <li role="presentation" class="{{ (isset($tabMain) && $tabMain == "referer") ? "active" : "" }}">
                <a class="btn btn-md" href="{{ route('get.traffic_referer.index', ['id' => $site->id]) }}">Traffic Nguồn referer</a>
            </li>
            <li role="presentation" class="{{ (isset($tabMain) && $tabMain == "data-public") ? "active" : "" }}">
                <a class="btn btn-md" href="{{ route('get.keyword_level.detail', ['id' => $site->id]) }}">Dữ liệu xuất bản</a>
            </li>
            <li role="presentation" class="{{ (isset($tabMain) && $tabMain == "data-extract") ? "active" : "" }}">
                <a class="btn btn-md" href="{{ route('get.keyword_extract.keyword_detail', ['id' => $site->id]) }}">Dữ liệu bóc tách</a>
            </li>
            <li role="presentation" class="{{ (isset($tabMain) && $tabMain == "request") ? "active" : "" }}">
                <a class="btn btn-md" href="{{ route('get.overview.request', ['id' => $site->id]) }}">Dữ liệu Request</a>
            </li>
        </ul>
    </div>

    @if(isset($tabSub))
        <div class="nav-tab-sub">
            <ul class="nav nav-tabs">
                @if( isset($tabMain) && $tabMain == "data-public")
                    <li role="presentation" class="{{ ($tabSub == "keyword-level") ? "active" : "" }}">
                        <a class="btn btn-md" href="{{ route('get.keyword_level.detail', ['id' => $site->id]) }}">Tổng quan</a>
                    </li>
                    <li role="presentation" class="{{ ($tabSub == "keyword-public") ? "active" : "" }}">
                        <a class="btn btn-md" href="{{ route('get.keyword_public.detail', ['id' => $site->id]) }}">Keyword public</a>
                    </li>
                    <li role="presentation" class="{{ ($tabSub == "location-public") ? "active" : "" }}">
                        <a class="btn btn-md" href="{{ route('get.location_public.detail', ['id' => $site->id]) }}">Location public</a>
                    </li>
                @else
                    <li role="presentation" class="{{ ($tabSub == "overview-extract") ? "active" : "" }}">
                        <a class="btn btn-md" href="{{ route('get.keyword_extract.overview_detail', ['id' => $site->id]) }}">Tổng quan</a>
                    </li>
                    <li role="presentation" class="{{ ($tabSub == "keyword-extract") ? "active" : "" }}">
                        <a class="btn btn-md" href="{{ route('get.keyword_extract.keyword_detail', ['id' => $site->id]) }}">Keyword</a>
                    </li>
                    <li role="presentation" class="{{ ($tabSub == "location-extract") ? "active" : "" }}">
                        <a class="btn btn-md" href="{{ route('get.keyword_extract.location_detail', ['id' => $site->id]) }}">Location</a>
                    </li>
                @endif
            </ul>
        </div>
    @endif
</div>