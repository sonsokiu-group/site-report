<input type="hidden" name="site_id" id="js-site-id" value="{{$site->id ?? 0}}">

<script>
    let URL_RENDER_CHART = '{{route('get.render.chart')}}';
</script>
<script src="{{ mix('js/chartJs.min.js', '/vendor/site-report') }}"></script>
<script src="{{ mix('js/render_chart.min.js', '/vendor/site-report') }}"></script>
