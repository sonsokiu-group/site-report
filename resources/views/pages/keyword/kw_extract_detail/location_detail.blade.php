@extends(__layout_master_core())
@section('title', "Báo cáo chi tiết location bóc tách")

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/site-report/css/report.css') }}">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12 js-header-fix">
            @include('packages.site-report::pages.traffic.components._inc_report_header',
                      [
                          'routeReportAll' => 'get.keyword_extract.index',
                          'routeReportDetail' => 'get.keyword_extract.location_detail'
                      ])

            <div style="display: flex;justify-content: space-between">
                @include('packages.site-report::components._inc_nav_tab')

                @include('packages.site-report::components._inc_box_filter',
                          ['action'=> route('get.keyword_extract.location_detail', ['id' => $site->id])])
            </div>
        </div>

        <div class="col-md-12 report-content">

{{--            Render biểu đồ--}}
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="box-chart">
                        <div class="chart-header">
                            <div class="box-title">
                                <b>Biểu đồ location bóc tách</b>
                            </div>
                        </div>

                        <div class="chart-main js-dom-chart" style="height: 400px" data-id="{{$site->id}}">
                            <canvas class="js-canvas"></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6">

                </div>
            </div>



         <div>
             {{--Bảng thống kê chi tiết location_analyst tất cả các site--}}
             <style>
                 .nav-lv2 .active > a {
                     background-color: #307be1 !important;
                     color: #ffffff;
                 }
                 .btn-purple{
                     background: #605ca8!important;
                 }
             </style>

             <div style="display:flex;justify-content:space-between">
                 <div> <span style="font-weight: bold">Chú thích:</span> <br>
                     <button type="button" class="btn btn-sm btn-warning"></button> Tổng số tin |
                     <button type="button" class="btn btn-sm btn-purple"></button> Số tin bóc được |
                     <button type="button" class="btn btn-sm btn-danger"></button>  State - City - County |
                     <button type="button" class="btn btn-sm btn-success"></button> Tỉ lệ
                 </div>
             </div>

             <div class="mt-5" style="background-color: #ffffff; overflow-x: scroll">
                 <table class="table table-striped table-bordered table-scroll">
                     <thead>
                     <tr style="background-color: #00a65a; color: #ffffff">
                         <th width="" style="min-width:120px">Ngày</th>
                         <th width="" class="text-center" style="min-width:120px">Tổng số tin</th>
                         <th width="" class="text-center" style="min-width:120px">Bóc được</th>
                         <th width="" class="text-center" style="min-width:120px">State</th>
                         <th width="" class="text-center" style="min-width:120px">City</th>
                         <th width="" class="text-center" style="min-width:120px">County</th>
                         <th width="" class="text-center" style="min-width:120px">Tỉ lệ</th>
                     </tr>
                     </thead>
                     <tbody>
                     @foreach($dataTableReport["dates"] as $key => $date)
                         @php
                             $countJd = $dataTableReport['data'][$site->id]['count_jd'][$date] ?? 0;
                             $countJdDone = $dataTableReport['data'][$site->id]['count_jd_done'][$date] ?? 0;
                             $countJdState = $dataTableReport['data'][$site->id]['state'][$date] ?? 0;
                             $countJdCity = $dataTableReport['data'][$site->id]['city'][$date] ?? 0;
                             $countJdCounty = $dataTableReport['data'][$site->id]['county'][$date] ?? 0;
                             $percent = 0;
                             if($countJd != 0 && $countJdDone != 0){
                                 $percent = round(( $countJdDone / $countJd * 100), 2);
                             }
                         @endphp
                         <tr>
                             <td class="text-center">{{ $date }}</td>
                             <td class="text-center text-orange">{{$countJd }}</td>
                             <td class="text-center text-purple">{{ $countJdDone}}</td>
                             <td class="text-center text-danger">{{ $countJdState}}</td>
                             <td class="text-center text-danger">{{$countJdCity }}</td>
                             <td class="text-center text-danger">{{$countJdCounty }}</td>
                             <td class="text-center text-success">{{$percent . "%" }}</td>
                         </tr>
                     @endforeach
                     </tbody>
                 </table>
             </div>
         </div>


        </div>
    </div>

    <input type="hidden" class="js-day" value="{{ form_query('day', $query, 30) }}">
    @include('packages.site-report::components._inc_scroll_top')
@stop

@section('script')
    <script src="{{ mix('js/chartJs.min.js', '/vendor/site-report') }}"></script>
    <script type="text/javascript">
        var CHART_LOCATION_EXTRACT = '{{route('get.chart_data.chartLocationExtract')}}';
    </script>
    <script src="{{ mix('js/location_extract_home.js', '/vendor/site-report') }}"></script>
@stop
