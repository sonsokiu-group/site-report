{{--Box chart top for keyword extract detail--}}
<div class="row">
    <div class="col-12 col-lg-6">
        <div class="box-chart">
            <div class="chart-header">
                <div class="box-title">
                    <b>Biểu đồ từ khoá bóc tách</b>
                </div>
            </div>

            <div class="chart-main js-dom-chart" style="height: 400px" data-id="{{$site->id}}"
                 data-name="{{$site->site_name}}"
                 data-type="">
                <canvas class="js-canvas"></canvas>
            </div>
        </div>
    </div>

    <div class="col-12 col-lg-6">

    </div>
</div>
