{{--Bảng thống kê chi tiết keyword extract theo ngày--}}

<div class="mt-5" style="background-color: #ffffff; overflow-x: scroll">
    <table class="table table-hover table-bordered">
        <thead>
        <tr style="background-color: #00a65a; color: #ffffff">
            <th width="" style="min-width: 120px">Ngày</th>
            <th width="" style="min-width: 120px" class="text-center">Số tin</th>
            <th width="" style="min-width: 120px" class="text-center">Keyword bóc tách</th>
            <th width="" style="min-width: 120px" class="text-center">Tỉ lệ bóc tách</th>
        </tr>
        </thead>
        <tbody>

        @foreach( array_reverse($dataTableReport["dates"], true) as $index => $date)
            @php
                $count_jd = $dataTableReport["dataChart"]["count_jd"][$index] ?? 0;
                $extract_kw = $dataTableReport["dataChart"]["extract_kw"][$index] ?? 0;
//                $extract_location = $dataTableReport["dataChart"]["extract_location"][$index];
                $percent_extract_kw = ($count_jd > 0) ? round(($extract_kw / $count_jd) * 100, 2) : 0;
//                $percent_extract_location = ($count_jd > 0) ? round(($extract_location / $count_jd) * 100, 2) : 0;
            @endphp
            <tr>
                <td>{{ $date }}</td>
                <td class="text-center">{{ $count_jd }}</td>
                <td class="text-center">
                    <span>{{ $extract_kw }}</span>
                </td>
                <td class="text-center">
                    <span class="text-success">( {{ $percent_extract_kw . " %" }} )</span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

