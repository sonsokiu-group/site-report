@extends(__layout_master_core())
@section('title', "Báo cáo chi tiết tổng quan dữ liệu bóc tách")

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/site-report/css/report.css') }}">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12 js-header-fix">
            @include('packages.site-report::pages.traffic.components._inc_report_header',
                      [
                          'routeReportAll' => 'get.keyword_extract.index',
                          'routeReportDetail' => 'get.keyword_extract.keyword_detail'
                      ])

            <div style="display: flex;justify-content: space-between">
                @include('packages.site-report::components._inc_nav_tab')

                @include('packages.site-report::components._inc_box_filter',
                          ['action'=> route('get.keyword_extract.overview_detail', ['id' => $site->id])])
            </div>
        </div>

        <div class="col-md-12 report-content">
            <div class="row">

                <div class="col-md-6">
                    <div class="box-chart">
                        <div class="chart-header">
                            <div class="box-title">
                                <a target="_blank" href="{{$site->site_url}}">
                                    <b>{{$site->site_name}}</b>
                                </a>
                            </div>

                        </div>

                        <div class="chart-main js-dom-chart" style="height: 280px" data-id="{{$site->id}}"
                             data-name="{{$site->site_name}}"
                             data-type="">
                            <canvas id="{{ "chart-overview-site-" . $site->id}}"></canvas>
                        </div>
                    </div>
                </div>

            </div>

            <div class="mt-5" style="background-color: #ffffff; overflow-x: scroll">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr style="background-color: #00a65a; color: #ffffff">
                        <th width="" style="min-width: 120px">Ngày</th>
                        <th width="" style="min-width: 120px" class="text-center">Tổng tin</th>
                        <th width="" style="min-width: 120px" class="text-center">Keyword bóc tách</th>
                        <th width="" style="min-width: 120px" class="text-center">Location bóc tách</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( array_reverse($dataTableReport["dates"], true) as $index => $date)
                        @php
                            $count_jd = $dataTableReport["dataChart"]["count_jd"][$index] ?? 0;
                            $extract_kw = $dataTableReport["dataChart"]["extract_kw"][$index] ?? 0;
                            $extract_location = $dataTableReport["dataChart"]["extract_location"][$index] ?? 0;
                            $percent_extract_kw = ($count_jd > 0) ? round(($extract_kw / $count_jd) * 100, 2) : 0;
                            $percent_extract_location = ($count_jd > 0) ? round(($extract_location / $count_jd) * 100, 2) : 0;
                        @endphp
                        <tr>
                            <td>{{ $date }}</td>
                            <td class="text-center">{{ $count_jd }}</td>
                            <td class="text-center">
                                <span>{{ $extract_kw }}</span>
                                <span class="text-success">( {{ $percent_extract_kw . " %" }} )</span>
                            </td>
                            <td class="text-center">
                                <span>{{ $extract_location }}</span>
                                <span class="text-success">( {{ $percent_extract_location . " %" }} )</span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <input type="hidden" class="js-day" value="{{ form_query('day', $query, 30) }}">
    @include('packages.site-report::components._inc_scroll_top')
@stop

@section('script')
    <script src="{{ mix('js/chartJs.min.js', '/vendor/site-report') }}"></script>
    <script>
        var CHART_OVERVIEW_EXTRACT = '{{route('get.keyword_data.chartOverviewExtract')}}'
    </script>
    <script src="{{ mix('js/extract_detail_overview.min.js', '/vendor/site-report') }}"></script>
@stop
