@extends(__layout_master_core())
@section('title', "Báo cáo chi tiết keyword bóc tách")

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/site-report/css/report.css') }}">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12 js-header-fix">
            @include('packages.site-report::pages.traffic.components._inc_report_header',
                      [
                          'routeReportAll' => 'get.keyword_extract.index',
                          'routeReportDetail' => 'get.keyword_extract.keyword_detail'
                      ])

            <div style="display: flex;justify-content: space-between">
                @include('packages.site-report::components._inc_nav_tab')

                @include('packages.site-report::components._inc_box_filter',
                          ['action'=> route('get.keyword_extract.keyword_detail', ['id' => $site->id])])
            </div>
        </div>

        <div class="col-md-12 report-content">
            @include('packages.site-report::pages.keyword.kw_extract_detail.components.inc_chart_line_overview')
            @include('packages.site-report::pages.keyword.kw_extract_detail.components.inc_table_kw_extract_detail')
        </div>
    </div>

    <input type="hidden" class="js-day" value="{{ form_query('day', $query, 30) }}">
    @include('packages.site-report::components._inc_scroll_top')
@stop

@section('script')
    <script src="{{ mix('js/chartJs.min.js', '/vendor/site-report') }}"></script>
    <script>
        var CHART_OVERVIEW_EXTRACT = '{{route('get.keyword_data.chartOverviewExtract')}}'
    </script>
    <script src="{{ mix('js/kw_extract_detail.min.js', '/vendor/site-report') }}"></script>
@stop
