@extends(__layout_master_core())
@section('title', "Báo cáo tổng quan dữ liệu bóc tách")

@section('css')
    <link rel="stylesheet" href="{{mix('css/report.css', 'vendor/site-report')}}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="report-header">
                <h3 class="mt-5">Tổng quan dữ liệu bóc tách</h3>
                <div class="" style="padding-right: 35px">
                    <ul class="breadcrumb-list">
                        <li>Site report</li>
                        <li style="white-space: nowrap;">Data extract</li>
                    </ul>
                </div>
            </div>
            <br>
            <div style="display: flex;justify-content: space-between">
                @include('packages.site-report::pages.keyword.kw_extract_home.components.inc_tab_nav_keyword_extract')
                @include('packages.site-report::pages.keyword.components.inc_form_filter_keyword', ['action'=> route('get.keyword_extract.index')])
            </div>

            @include('packages.site-report::pages.keyword.kw_extract_home.components.inc_list_chart')
        </div>
    </div>

    <input type="hidden" class="js-day" value="{{ form_query('day', $query, 7) }}">
    @include('packages.site-report::components._inc_scroll_top')
@stop

@section('script')
    <script src="{{ mix('js/chartJs.min.js', '/vendor/site-report') }}"></script>
    <script>
        var CHART_OVERVIEW_EXTRACT = '{{route('get.keyword_data.chartOverviewExtract')}}'
    </script>
    <script src="{{ mix('js/kw_extract_home.min.js', '/vendor/site-report') }}"></script>
@stop