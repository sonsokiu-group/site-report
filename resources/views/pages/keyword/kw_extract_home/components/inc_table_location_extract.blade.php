{{--Bảng thống kê chi tiết location_analyst tất cả các site--}}
<style>
    .nav-lv2 .active > a {
        background-color: #307be1 !important;
        color: #ffffff;
    }

    .btn-purple {
        background: #605ca8 !important;
    }
</style>

<div style="display:flex;justify-content:space-between">
    <div><span style="font-weight: bold">Chú thích:</span> <br>
        <button type="button" class="btn btn-sm btn-warning"></button>
        Tổng số tin |
        <button type="button" class="btn btn-sm btn-purple"></button>
        Số tin bóc được |
        <button type="button" class="btn btn-sm btn-danger"></button>
        State - City - County |
        <button type="button" class="btn btn-sm btn-success"></button>
        Tỉ lệ
    </div>
</div>

{{--@dd($dataTableReport)--}}
<div class="mt-5" style="background-color: #ffffff; overflow-x: scroll">
    <table class="table table-striped table-bordered table-scroll">
        <thead>
        <tr style="background-color: #00a65a; color: #ffffff">
            <th width="" style="min-width: 120px;max-width: 120px">Site</th>
            <th width="" class="text-center" style="min-width:150px;max-width: 150px">Chú thích</th>
            @foreach($dataTableReport['dates'] as $date)
                @php
                    if (isset($tabSub) && $tabSub == "table-year")
                        {
                            $date = "Tháng " . $date;
                        }
                @endphp
                <th style="min-width: 120px" class="text-center">{{$date}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($sites as $site)
            <tr>
                <td>
                    <a href="{{ route('get.keyword_extract.location_detail', ['id' => $site->id]) }}">{{ $site->site_name }}</a>
                </td>
                <td class="text-center text-orange">
                    <span class="">  <span class="text-purple"> Bóc được </span>  / <span class="text-orange fw-bold">Tổng số</span></span>
                    <br>
                    <span class="text-danger"> State / City / County</span>
                    <br>
                    <strong class="text-success" style="font-size:12px"> (tỉ lệ)</strong>
                </td>
                @foreach($dataTableReport["dates"] as $date)
                    @php
                        $countJd = $dataTableReport['data'][$site->id]['count_jd'][$date] ?? 0;
                        $countJdDone = $dataTableReport['data'][$site->id]['count_jd_done'][$date] ?? 0;
                        $countJdState = $dataTableReport['data'][$site->id]['state'][$date] ?? 0;
                        $countJdCity = $dataTableReport['data'][$site->id]['city'][$date] ?? 0;
                        $countJdCounty = $dataTableReport['data'][$site->id]['county'][$date] ?? 0;
                        $percent = 0;
                        if($countJd != 0 && $countJdDone != 0){
                            $percent = round(( $countJdDone/$countJd*100), 2);
                        }
                    @endphp
                    <td class="text-center">
                        <span class="">  <span class="text-purple"> {{ $countJdDone }} </span>  / <span
                                    class="text-orange fw-bold">{{$countJd}}</span></span>
                        <br>
                        <span class="text-danger"> {{ $countJdState }} / {{ $countJdCity }} / {{ $countJdCounty }}</span>
                        <br>
                        <strong class="text-success" style="font-size:12px"> ( {{ $percent . "%" }} )</strong>
                    </td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>


