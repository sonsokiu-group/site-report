<div class="row">
    @foreach($sites as $key => $site)
        <div class="col-md-4">
            <div class="box-chart">
                <div class="chart-header">
                    <div class="box-title">
                        <a target="_blank" href="{{$site->site_url}}">
                            <b>{{$site->site_name}}</b>
                        </a>
                    </div>
                    <a class="btn-see-details"
                       href="{{ route('get.keyword_extract.location_detail', ['id' => $site->id]) }}">Xem chi tiết</a>
                </div>
                <div class="row" style="padding: 0 15px;margin-bottom:20px">
                    <div class="col-md-6">
                        Bóc được tổng: <b class="js-total-current"></b> / <b class="js-total-before"></b> JD
                    </div>
                    <div class="col-md-6">
                        Tỉ lệ bóc : <b class="js-growthRate"> </b>
                    </div>

                </div>

                <div class="chart-main js-dom-chart" style="height: 280px" data-id="{{$site->id}}"
                     data-name="{{$site->site_name}}"
                     data-type="">
                    <canvas class="js-canvas"></canvas>
                </div>
            </div>
        </div>
    @endforeach
</div>

{!! PaginateHelper::paginate($sites, $query) !!}
