
<style>
    .nav-lv2 .active > a {
        background-color: #307be1 !important;
        color: #ffffff;
    }
    .btn-purple{
        background: #605ca8!important;
    }
</style>
<div style="display:flex;justify-content:space-between">
{{--    <ul class="nav nav-tabs mb-2 nav-lv2">--}}
{{--        <li role="presentation" class="@if(!isset($_REQUEST['type']) || $_REQUEST['type'] == 'month') active @endif--}}
{{--    ">--}}
{{--            <a href="{{route('get.location_analyst.index',['tab' => 'table','type' => 'month'])}}">Theo ngày</a>--}}
{{--        </li>--}}
{{--        <li role="presentation" class="@if(isset($_REQUEST['type']) && $_REQUEST['type'] == 'year') active @endif--}}
{{--    ">--}}
{{--            <a href="{{route('get.location_analyst.index',['tab' => 'table','type' => 'year'])}}">Theo Tháng</a>--}}
{{--        </li>--}}
    </ul>
    <div> <span style="font-weight: bold">Chú thích:</span> <br>
        <button type="button" class="btn btn-sm btn-warning"></button> Tổng số tin |
        <button type="button" class="btn btn-sm btn-purple"></button> Số tin bóc được |
        <button type="button" class="btn btn-sm btn-success"></button> Tỉ lệ
    </div>
</div>

<div class="mt-5" style="background-color: #ffffff; overflow-x: scroll">
    <table class="table table-hover table-bordered table-scroll">
        <thead>
        <tr style="background-color: #00a65a; color: #ffffff">
            <th width="" style="min-width: 120px">Site</th>
            <th width="" class="text-center" style="min-width: 100px">Tổng</th>
            @foreach($dataReportTable['dates'] as $date)
                <th width="" class="text-center">{{ $date }}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($dataReportTable['result'] as $arrData)
            <tr>
                <td>
                    <a href="{{ route('get.keyword_extract.keyword_detail', ['id' => $arrData['site_id']]) }}">{{ $arrData['site_name'] }}</a>
                </td>
                <td class="text-center">
                    <p>
                        <span class="text-purple">{{ $arrData['total_extract'] }}</span>
                        <span>/</span>
                        <span class="text-orange">{{ $arrData['total_jd'] }}</span>
                    </p>
                    <p><span class="text-success">( {{  $arrData['percent_extract'] . "%" }} )</span></p>
                </td>
                @foreach($dataReportTable['dates'] as $date)
                    @php
                        $count_jd = $arrData['data']['count_jd'][$date] ?? 0;
                        $count_jd_done = $arrData['data']['count_jd_done'][$date] ?? 0;
                        $percent_extract = ($count_jd != 0) ? round(($count_jd_done / $count_jd) * 100, 2 ) : 0;
                    @endphp
                    <td class="text-center">
                        <p>
                            <span class="text-purple">{{ $count_jd_done }}</span>
                            <span>/</span>
                            <span class="text-orange">{{ $count_jd }}</span>
                        </p>
                        <p><span class="text-success">( {{  $percent_extract . "%" }} )</span></p>
                    </td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>