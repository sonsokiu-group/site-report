<ul class="nav nav-tabs">
    <li role="presentation" class="{{ ($tabMain == 'index') ? 'active' :'' }}">
        <a href="{{ route('get.keyword_extract.index') }}">Tổng quan</a>
    </li>
    <li role="presentation" class="{{ ($tabMain == 'keyword') ? 'active' :'' }}">
        <a href="{{ route('get.keyword_extract.keyword') }}">Keyword</a>
    </li>
    <li role="presentation" class="{{ ($tabMain == 'location') ? 'active' :'' }}">
        <a href="{{ route('get.keyword_extract.location', ['tabSub' => 'table-month']) }}">Location</a>
    </li>
</ul>