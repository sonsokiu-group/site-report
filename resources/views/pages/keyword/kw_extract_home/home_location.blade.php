@extends(__layout_master_core())
@section('title', "Báo cáo location bóc tách")

@section('css')
    <link rel="stylesheet" href="{{mix('css/report.css', 'vendor/site-report')}}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="report-header">
                <h3 class="mt-5">Báo cáo location bóc tách</h3>
                <div class="" style="padding-right: 35px">
                    <ul class="breadcrumb-list">
                        <li>Site report</li>
                        <li style="white-space: nowrap;">Location extract</li>
                    </ul>
                </div>
            </div>
            <br>
            <div style="display: flex;justify-content: space-between">
                @include('packages.site-report::pages.keyword.kw_extract_home.components.inc_tab_nav_keyword_extract')
                @include('packages.site-report::pages.keyword.components.inc_form_filter_keyword', ['action'=> route('get.keyword_extract.location', ['tabSub' => $tabSub])])
            </div>
            @if( isset($tabMain) && $tabMain == "location" )
                <div style="display: flex;justify-content: space-between">
                    <ul class="nav nav-tabs">
                        <li role="presentation" class="{{ ($tabSub == 'chart') ? 'active' : ''}}">
                            <a href="{{ route('get.keyword_extract.location', ['tabSub' => 'chart']) }}">Biểu đồ</a>
                        </li>
                        <li role="presentation" class="{{ ($tabSub == 'table-month') ? 'active' : ''}}">
                            <a href="{{ route('get.keyword_extract.location', ['tabSub' => 'table-month']) }}">Bảng số
                                liệu tháng</a>
                        </li>
                        <li role="presentation" class="{{ ($tabSub == 'table-year') ? 'active' : ''}}">
                            <a href="{{ route('get.keyword_extract.location', ['tabSub' => 'table-year']) }}">Bảng số
                                liệu năm</a>
                        </li>
                    </ul>
                </div>
            @endif

            @if($tabSub == "chart")
                @include('packages.site-report::pages.keyword.kw_extract_home.components.inc_list_chart_location_extract')
            @else
                @include('packages.site-report::pages.keyword.kw_extract_home.components.inc_table_location_extract')
            @endif

        </div>
    </div>

    <input type="hidden" class="js-day" value="{{ form_query('day', $query, 30) }}">
    @include('packages.site-report::components._inc_scroll_top')
@stop

@section('script')
    <script src="{{ mix('js/chartJs.min.js', '/vendor/site-report') }}"></script>
    <script type="text/javascript">
        var CHART_LOCATION_EXTRACT = '{{route('get.chart_data.chartLocationExtract')}}';
    </script>
    <script src="{{ mix('js/location_extract_home.js', '/vendor/site-report') }}"></script>
@stop