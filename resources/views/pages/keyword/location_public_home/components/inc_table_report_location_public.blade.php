{{--Bảng thống kê chi tiết location_public tất cả các site--}}

<div class="mt-5" style="background-color: #ffffff; overflow-x: scroll">
    <table class="table table-hover table-bordered table-scroll">
        <thead>
        <tr style="background-color: #00a65a; color: #ffffff">
            <th width="" style="width: 30px" >STT</th>
            <th width="" style="min-width: 120px">Site</th>
            <th width="" style="min-width: 120px" class="text-center">Tổng địa điểm</th>
            <th width="" style="min-width: 120px" class="text-center">Địa điểm public</th>
            <th width="" style="min-width: 120px" class="text-center">Tỉ lệ</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i = 1;
        @endphp
        @foreach($dataTableReport as $siteId => $arrayData)
            <tr>
                <td>{{$i++}}</td>
                <td>
                    <a href="{{ route('get.location_public.detail', ['id' => $siteId]) }}">{{ $arrayData["site_name"]}}</a>
                </td>
                <td class="text-center">{{ $arrayData["total_location"] }}</td>
                <td class="text-center">{{ $arrayData["location_public"] }}</td>
                <td class="text-center">{{ $arrayData["percent_public"] . "%" }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>