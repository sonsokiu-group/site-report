@extends(__layout_master_core())
@section('title', "Báo cáo location public")

@section('css')
    <link rel="stylesheet" href="{{mix('css/report.css', 'vendor/site-report')}}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="report-header">
                <h3 class="mt-5">Báo cáo location public</h3>
                <div class="" style="padding-right: 35px">
                    <ul class="breadcrumb-list">
                        <li>Site report</li>
                        <li style="white-space: nowrap;">Location public</li>
                    </ul>
                </div>
            </div>
            <br>
            <div style="display: flex;justify-content: space-between">
                <div>

                </div>
                @include('packages.site-report::pages.keyword.components.inc_form_filter_keyword', [
                    'action'=> route('get.location_public.index')
                ])
            </div>

            @include('packages.site-report::pages.keyword.location_public_home.components.inc_table_report_location_public')
        </div>
    </div>

    <input type="hidden" class="js-day" value="{{ form_query('day', $query, 30) }}">
    @include('packages.site-report::components._inc_scroll_top')
@stop

@section('script')
    <script src="{{ mix('js/chartJs.min.js', '/vendor/site-report') }}"></script>
    <script src="{{ mix('js/location_public_home.js', '/vendor/site-report') }}"></script>
@stop
