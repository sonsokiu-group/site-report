@extends(__layout_master_core())
@section('title', "Báo cáo chi tiết location public")

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/site-report/css/report.css') }}">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12 js-header-fix">
            @include('packages.site-report::pages.traffic.components._inc_report_header',
                      [
                          'routeReportAll' => 'get.keyword_public.index',
                          'routeReportDetail' => 'get.keyword_public.detail'
                      ])

            <div style="display: flex;justify-content: space-between">
                @include('packages.site-report::components._inc_nav_tab')

            </div>
        </div>

        <div class="col-md-12 report-content">
            {{--Bảng thống kê chi tiết location_public--}}

            <div class="mt-5" style="background-color: #ffffff">
                <table class="table table-hover table-bordered table-scroll">
                    <thead>
                    <tr style="background-color: #00a65a; color: #ffffff">
                        <th width="" style="min-width: 120px">Site</th>
                        <th width="" style="min-width: 120px" class="text-center">Tổng địa điểm</th>
                        <th width="" style="min-width: 120px" class="text-center">Địa điểm public</th>
                        <th width="" style="min-width: 120px" class="text-center">Tỉ lệ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dataTableReport as $siteId => $arrayData)
                        <tr>
                            <td>
                                <a href="{{ route('get.location_public.detail', ['id' => $siteId]) }}">{{ $arrayData["site_name"]}}</a>
                            </td>
                            <td class="text-center">{{ $arrayData["total_location"] }}</td>
                            <td class="text-center">{{ $arrayData["location_public"] }}</td>
                            <td class="text-center">{{ $arrayData["percent_public"] . "%" }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <input type="hidden" class="js-day" value="{{ form_query('day', $query, 30) }}">
    @include('packages.site-report::components._inc_scroll_top')
@stop

@section('script')
    <script src="{{ mix('js/chartJs.min.js', '/vendor/site-report') }}"></script>
@stop
