@extends(__layout_master_core())
@section('title', "Báo cáo chi tiết keyword level")

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/site-report/css/report.css') }}">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12 js-header-fix">
            @include('packages.site-report::pages.traffic.components._inc_report_header',
                      [
                          'routeReportAll' => 'get.keyword_level.index',
                          'routeReportDetail' => 'get.keyword_level.detail'
                      ])

            <div style="display: flex;justify-content: space-between">
                @include('packages.site-report::components._inc_nav_tab')
            </div>
        </div>

        <div class="col-md-12 report-content">
            @include('packages.site-report::pages.keyword.kw_level_detail.components.inc_box_chart_top')
        </div>
    </div>
    <input type="hidden" class="js-day" value="{{ form_query('day', $query, 14) }}">

    @include('packages.site-report::components._inc_scroll_top')
@stop

@section('script')
    <script src="{{ mix('js/chartJs.min.js', '/vendor/site-report') }}"></script>
    <script src="{{ mix('js/kw_public_home.min.js', '/vendor/site-report') }}"></script>
    <script type="text/javascript">
        let URL_CHART_KW_LEVEL = '{{ route('get.keyword_data.getDataChartKeywordLevel') }}';
        let URL_CHART_KW_PUBLIC = '{{route('get.keyword_data.chartKeywordPublicHome')}}';
        let URL_GET_DATA_CHART = '{{route('get.job_seo.getDataChart')}}'; // sử dụng cho biểt đồ xuất bản tin và từ khoá Seo
    </script>
    <script src="{{ mix('js/kw_level_home.min.js', '/vendor/site-report') }}"></script>
    <script src="{{ mix('js/job_seo_report.js', '/vendor/m_report') }}"></script>
@stop
