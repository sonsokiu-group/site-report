{{--Box chart top for keyword_level detail--}}

<div class="row">
    {{-- Biều đổ Xuất bản tin --}}
    <div class="col-md-4">
        <div class="box-chart">
            <div class="chart-header">
                <div class="box-title">
                    <b>Xuất bản tin</b>
                </div>

            </div>
            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-6">
                    Luỹ kế: <b class=""></b>
                </div>
                <div class="col-md-6">
                    Hiện tại: <b class="js-total-current"></b>
                </div>

            </div>

            <div class="chart-main js-dom-chart-job-seo" style="height: 400px" data-id="{{$site->id}}"
                 data-name="{{$site->site_name}}"
                 data-type="upload_public">
                <canvas class="js-canvas"></canvas>
            </div>
        </div>
    </div>

    {{-- Biều đổ Từ khoá Seo --}}
    <div class="col-md-4">
        <div class="box-chart">
            <div class="chart-header">
                <div class="box-title">
                    <b>Từ khoá Seo</b>
                </div>

            </div>
            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-6">
                    Luỹ kế: <b class=""></b>
                </div>
                <div class="col-md-6">
                    Hiện tại: <b class="js-total-current"></b>
                </div>

            </div>

            <div class="chart-main js-dom-chart-job-seo" style="height: 400px" data-id="{{$site->id}}"
                 data-name="{{$site->site_name}}"
                 data-type="seo_content">
                <canvas class="js-canvas"></canvas>
            </div>
        </div>
    </div>

    {{-- Biểu đồ Từ khoá xuất bản --}}
    <div class="col-md-4">
        <div class="box-chart">
            <div class="chart-header">
                <div class="box-title">
                    <b>Từ khoá xuất bản</b>
                </div>

            </div>
            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-6">
                    Tổng: <b class="js-total-current"></b> keyword
                </div>
                <div class="col-md-6">
                    Tỉ lệ phát triển: <b class="js-growth-rate"></b>
                </div>

            </div>

            <div class="chart-main js-dom-chart" style="height: 400px" data-id="{{$site->id}}"
                 data-name="{{$site->site_name}}"
                 data-type="">
                <canvas class="js-canvas"></canvas>
            </div>
        </div>
    </div>

    <div class="col-12 col-lg-6">
        <div class="box-chart">
            <div class="chart-header">
                <div class="box-title">
                    <b>Keyword level</b>
                </div>
            </div>

            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-6">
                    Tổng lũy kế: <b class="js-total"></b> keyword
                </div>


            </div>


            <div class="chart-main js-dom-chart-new" style="height: 400px" data-id="{{$site->id}}"
            data-type="bar">
                <canvas class="js-canvas"></canvas>
            </div>
        </div>
    </div>

    <div class="col-12 col-lg-6">
        <div class="box-chart">
            <div class="chart-header">
                <div class="box-title">
                    <b>Keyword level phân bổ</b>
                </div>
            </div>
            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-6">
                    Tổng lũy kế: <b class="js-total"></b> keyword
                </div>


            </div>


            <div class="chart-main js-dom-chart-new" style="height: 400px" data-id="{{$site->id}}"
                 data-type="pie">
                <canvas class="js-canvas"></canvas>
            </div>
        </div>
    </div>

</div>
