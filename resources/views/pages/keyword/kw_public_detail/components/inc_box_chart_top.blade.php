<div class="row">

        <div class="col-md-6">
            <div class="box-chart">
                <div class="chart-header">
                    <div class="box-title">
                        <b>Tổng quan keyword public</b>
                    </div>

                </div>
                <div class="row" style="padding: 0 15px;margin-bottom:20px">
                    <div class="col-md-6">
                        Tổng: <b class="js-total-current"></b> keyword
                    </div>
                    <div class="col-md-6">
                        Tỉ lệ phát triển: <b class="js-growth-rate"></b>
                    </div>

                </div>

                <div class="chart-main js-dom-chart" style="height: 400px" data-id="{{$site->id}}"
                     data-name="{{$site->site_name}}"
                     data-type="">
                    <canvas class="js-canvas"></canvas>
                </div>
            </div>
        </div>

</div>

