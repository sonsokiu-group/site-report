<div class="mt-5 box-table-statistic">
{{--    <h4>Bảng thống kê keyword public</h4>--}}
    <table class="table table-hover table-bordered">
        <thead>
        <tr style="background-color: #00a65a; color: #ffffff">
            <th width="">Ngày</th>
            @for($month = 1; $month < 13; $month++)
                <th width="" class="text-center">{{ "Tháng " . $month }}</th>
            @endfor
        </tr>
        </thead>
        <tbody>
        @for($day = 1; $day < 32; $day++)
            <tr>
                <td> {{ "Ngày " . $day }}</td>
                @for($month = 1; $month < 13; $month++)
                    @php
                        $valueKeyword = $dataTableDetail[$month]["data_days"][$day] ?? "---";
                    @endphp
                    <td class="text-center {{ ($valueKeyword === 0) ? 'bg-danger' : '' }}">
                        <span class=""> {{ $valueKeyword }} </span>
                    </td>
                @endfor
            </tr>
        @endfor
        <tr class="bg-warning">
            <td>Tổng</td>
            @for($month = 1; $month < 13; $month++)
                <td class="text-center">
                    <span class=""> {{ $dataTableDetail[$month]["total"] ?? "---" }} </span>
                </td>
            @endfor
        </tr>
        </tbody>
    </table>
</div>