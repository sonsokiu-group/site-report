@extends(__layout_master_core())
@section('title', "Báo cáo chi tiết keyword public")

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/site-report/css/report.css') }}">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12 js-header-fix">
            @include('packages.site-report::pages.traffic.components._inc_report_header',
                      [
                          'routeReportAll' => 'get.keyword_public.index',
                          'routeReportDetail' => 'get.keyword_public.detail'
                      ])

            <div style="display: flex;justify-content: space-between">
                @include('packages.site-report::components._inc_nav_tab')

            </div>
        </div>
        <div class="col-md-12 report-content">
{{--            @include('packages.site-report::pages.keyword.kw_public_detail.components.inc_box_chart_top')--}}
            @include('packages.site-report::pages.keyword.kw_public_detail.components.inc_table_kw_public_detail')
        </div>
    </div>

    <input type="hidden" class="js-day" value="{{ form_query('day', $query, 30) }}">
    @include('packages.site-report::components._inc_scroll_top')
@stop

@section('script')
    <script src="{{ mix('js/chartJs.min.js', '/vendor/site-report') }}"></script>
    <script type="text/javascript">
        let URL_CHART_KW_PUBLIC = '{{route('get.keyword_data.chartKeywordPublicHome')}}'
    </script>
    <script src="{{ mix('js/kw_public_home.min.js', '/vendor/site-report') }}"></script>
@stop
