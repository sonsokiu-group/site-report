{{--Bảng thống kê chi tiết keyword_level tất cả các site--}}

<div class="mt-5" style="background-color: #ffffff; overflow-x: scroll">
    <table class="table table-hover table-bordered table-scroll">
        <thead>
        <tr style="background-color: #00a65a; color: #ffffff">
            <th width="" style="width: 30px">STT</th>
            <th width="" style="min-width: 120px">Site</th>
            <th width="" style="min-width: 120px" class="text-center">Tổng</th>
            <th width="" style="min-width: 120px" class="text-center">Level 0</th>
            <th width="" style="min-width: 120px" class="text-center">Level 1</th>
            <th width="" style="min-width: 120px" class="text-center">Level 2</th>
            <th width="" style="min-width: 120px" class="text-center">Level 3</th>
            <th width="" style="min-width: 120px" class="text-center">Level 4</th>
            <th width="" style="min-width: 120px" class="text-center">Level 5</th>
        </tr>
        </thead>
        <tbody>
        @if(!empty($dataTableReport))
            @php
                $i = 1;
            @endphp
            @foreach($dataTableReport as $siteId => $dataItem)
                <tr>
                    <td>{{$i++}}</td>
                    <td>
                        <a href="{{ route('get.keyword_level.detail', ['id' => $siteId]) }}">{{ $dataItem['site_name'] }}</a>
                    </td>
                    <td class="text-center">{{ $dataItem['total_keyword'] }}</td>
                    @foreach($dataItem['data_keyword'] as $levelKeyword => $value)
                        @php
                            $percentKeywordLevel = round(($value / $dataItem['total_keyword']) * 100, 2)
                        @endphp
                        <td class="text-center">
                            <span class=""> {{ $value }} </span>
                            @if($dataItem['total_keyword'] != 0)
                                <span class="text-success"> ( {{$percentKeywordLevel . "%"}} )</span>
                            @endif
                        </td>
                    @endforeach
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>