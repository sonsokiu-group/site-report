@extends(__layout_master_core())
@section('title', "Báo cáo keyword level")

@section('css')
    <link rel="stylesheet" href="{{mix('css/report.css', 'vendor/site-report')}}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="report-header">
                <h3 class="mt-5">Báo Cáo Tổng Quan Keyword Level</h3>
                <div class="" style="padding-right: 35px">
                    <ul class="breadcrumb-list">
                        <li>Site report</li>
                        <li style="white-space: nowrap;">Keyword level</li>
                    </ul>
                </div>
            </div>
            <br>

            <div style="display: flex;justify-content: space-between">
                @include('packages.site-report::pages.keyword.components.inc_nav_tab_keyword', ['nameRouteTab' => 'get.keyword_level.index'])
                @include('packages.site-report::pages.keyword.components.inc_form_filter_keyword', ['action'=> route('get.keyword_level.index')])
            </div>

            @if( isset($tab) && $tab == 'chart' )
                @include('packages.site-report::pages.keyword.kw_level_home.components.inc_chart_bar_kw_level')
            @else
                @include('packages.site-report::pages.keyword.kw_level_home.components.inc_table_report_kw_level')
            @endif

        </div>
    </div>

    @include('packages.site-report::components._inc_scroll_top')
@stop

@section('script')
    <script src="{{ mix('js/chartJs.min.js', '/vendor/site-report') }}"></script>
    <script type="text/javascript">
        let URL_CHART_KW_LEVEL = '{{ route('get.keyword_data.getDataChartKeywordLevel') }}'
    </script>
    <script src="{{ mix('js/kw_level_home.min.js', '/vendor/site-report') }}"></script>
@stop
