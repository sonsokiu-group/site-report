{{--Bảng thống kê chi tiết keyword_public tất cả các site--}}

<div class="mt-5" style="background-color: #ffffff; overflow-x: scroll">
    <table class="table table-hover table-bordered table-scroll">
        <thead>
        <tr style="background-color: #00a65a; color: #ffffff">
            <th width="">STT</th>
            <th width="" style="min-width: 120px">Site</th>
            <th width="" class="text-center">Tổng</th>
            @foreach( reset($dataTableReport)["data_keyword"] as $date => $value)
                <th width="" class="text-center">{{$date}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>

        @php($i = 0)
        @foreach($dataTableReport as $siteId => $arrayData)
            <tr>
                <td>{{++$i}}</td>
                <td>
                    <a href="{{ route('get.keyword_public.detail', ['id' => $siteId]) }}">{{ $arrayData["site_name"] }}</a>
                </td>
                <td class="text-center">{{ $arrayData["total_keyword"] }}</td>
                @foreach($arrayData["data_keyword"] as $date => $value)
                    <td class="text-center {{ $value == 0 ? 'bg-danger' : '' }}">
                        <span class=""> {{ $value }} </span>
                        {{--                        <span class="text-success"> ( {{ round(( rand(100, 300) / 10), 2 ) . "%" }} )</span>--}}
                    </td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>