<div class="box-filter">
    <div class="form-filter pull-left">
        <form action="{{$action}}" method="GET"
              class="form-inline js-form-filter-report">

            <input type="hidden" class="form-control" name="tab"
                   value="{{form_query('tab', $query, 'chart')}}">
            @if(isset($tabSub))
                <input type="hidden" class="form-control" name="tabSub"
                       value="{{form_query('tabSub', $query, 'table-month')}}">
            @endif

            @if( isset($isFilterDay) && $isFilterDay === true )
                @if(isset($page) && $page == "detail")
                    Lọc theo thời gian:
                @endif

                <select class="form-control js-change-day-report" name="day">
                    <option value="7" {{ form_query('day', $query, 7) == 7 ? 'selected' : '' }}>7 ngày</option>
                    <option value="14" {{ form_query('day', $query) == 14 ? 'selected' : '' }}>14 ngày</option>
                    <option value="30" {{ form_query('day', $query) == 30 ? 'selected' : '' }}>30 ngày</option>
                </select>
            @endif

            @if( isset($isFilterSort) && $isFilterSort === true )
                <select class="form-control" name="sort">
                    <option value="">-- Hiệu quả bóc tách --</option>
                        <option value="asc" {{ query_selected('sort', 'asc') }}>Tăng dần</option>
                        <option value="desc" {{ query_selected('sort', 'desc') }}>Giảm dần</option>
                </select>
            @endif

            @if( isset($continents))
                <select class="form-control" name="continent">
                    <option value="">-- Châu lục --</option>
                    @foreach($continents as $key=> $continent)
                        <option
                                value="{{$continent['slug']}}" {{ query_selected('continent', $key) }}>{{$continent['name']}}</option>
                    @endforeach
                </select>
            @endif

            <input type="text" class="form-control" name="site_name" placeholder="Tìm tên quốc gia"
                   value="{{form_query('site_name', $query)}}">
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
            <a href="{{$action}}" class="">
                <button type="button" class="form-control btn btn-default">Refresh</button>
            </a>
        </form>
    </div>

    <div class="pull-right">
        {{--        <a href="" class="btn btn-success"><i class="fa fa-plus"></i>--}}
        {{--            Thêm mới</a>--}}
    </div>
    <div class="clearfix"></div>
    <br>
</div>

