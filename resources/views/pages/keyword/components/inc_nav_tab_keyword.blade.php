<ul class="nav nav-tabs">
    <li class="{{ ($tab == 'chart') ? 'active' : '' }}">
        <a href="{{ route($nameRouteTab, ['tab' => 'chart']) }}">Biểu đồ</a>
    </li>
    <li class="{{ ($tab == 'table') ? 'active' : '' }}">
        <a href="{{ route($nameRouteTab, ['tab' => 'table']) }}">Bảng số liệu</a>
    </li>
</ul>