<div class="row">
    @foreach($sites as $key => $site)
        <div class="col-sm-6 col-lg-4">
            <div class="box-chart">
                <div class="chart-header">
                    <div class="box-title">
                        <a target="_blank" href="{{$site->site_url}}">
                            <b>{{$site->site_name}}</b>
                        </a>
                    </div>
                    <a class="btn-see-details"
                       href="{{ route('get.traffic_overview.index', ['id' => $site->id]) }}">Xem chi tiết</a>
                </div>
                <div class="row" style="padding: 0 15px;margin-bottom:20px">
                    <div class="col-md-6">
                        Tổng traffic: <b class="js-total-current"></b> lượt
                    </div>
                    <div class="col-md-6">
                        Tỉ lệ phát triển: <b class="js-growth-rate">  </b>
                    </div>

                </div>
                <div class="chart-main js-dom-chart" style="height: 280px" data-id="{{$site->id}}" data-name="{{$site->site_name}}"
                     data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_TRAFFIC_HOME }}">
                    <canvas id="{{ "chart-overview-site-" . $site->id}}"></canvas>
                </div>
            </div>
        </div>
    @endforeach
</div>

{!! PaginateHelper::paginate($sites, $query) !!}
