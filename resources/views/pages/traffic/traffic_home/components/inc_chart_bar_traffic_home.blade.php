<div class="row">
    <div class="col-md-10">
        <div class="box-chart">
            <div class="chart-header">
                <div class="box-title">
                    Traffic tổng quan của các site
                </div>
            </div>
            <div class="chart-main" style="height: 450px">
                <canvas id="chart-bar-all-site"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="box-chart">
            <div class="chart-header">
                <div class="box-title">
                    Xếp hạng
                </div>
            </div>
            <div class="chart-main" style="height: 450px">
                <div class="group-items js-group-item-info">

                </div>
            </div>
        </div>
    </div>
</div>
