@extends(__layout_master_core())
@section('title', "Báo cáo traffic")

@section('css')
    <link rel="stylesheet" href="{{mix('css/report.css', 'vendor/site-report')}}">
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="report-header">
                <h3 class="mt-5">Biểu đồ tổng quan traffic</h3>
                <div class="" style="padding-right: 35px">
                    <ul class="breadcrumb-list">
                        <li>Overview</li>
                        <li style="white-space: nowrap;">Traffic</li>
                    </ul>
                </div>
            </div>
            <br>

            @include('packages.site-report::components._inc_box_filter', ['action'=> route('get.traffic_home.index')])

            {!! __render_chart_item("config_chart_traffic_home@traffic_all_site_line") !!}

            @include('packages.site-report::pages.traffic.traffic_home.components.inc_chart_bar_traffic_home')
            @include('packages.site-report::pages.traffic.traffic_home.components.inc_chart_traffic_overview')
        </div>
    </div>

    <input type="hidden" class="js-day" value="{{ form_query('day', $query, 30) }}">
    @include('packages.site-report::components._inc_scroll_top')
@stop

@section('script')
    <script src="{{ mix('js/chartJs.min.js', '/vendor/site-report') }}"></script>
    <script>
        var URL_GET_DATA_TRAFFIC_LINE_CHART = '{{route('get.traffic_data.getDataLineChart')}}';
        var URL_GET_DATA_CHART_TRAFFIC_ALL = '{{route('get.traffic_data.getDataChartAllSite')}}';
    </script>
    <script src="{{ mix('js/traffic_home.js', '/vendor/site-report') }}"></script>
    @include('packages.site-report::templates.dom_chart_js')
@stop
