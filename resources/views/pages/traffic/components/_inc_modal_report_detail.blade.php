<div id="modal-report-detail" class="modal"
     data-url="{{$urlGetDataDetail}}" data-modal-type="{{$modalType}}">

    <div class="modal-content">
        <span class="btn-close-modal">&times;</span>
        <div class="modal-header" style="display: flex;justify-content: space-between">
            <h4>
                Biểu đồ chi tiết theo: <b class="js-name-chart"></b>
            </h4>
            <select style="width:200px" class="form-control js-option-day-report" name="day">
                <option value="7">7 ngày</option>
                <option value="14">14 ngày</option>
                <option value="30">30 ngày</option>
            </select>
        </div>
        <div class="modal-body">
            <div class="box-chart" style="margin-bottom: 0">
                <div class="row" style="padding: 0 15px;margin-bottom:20px">
                    <div class="col-md-4">
                        Tổng hiện tại: <b class="js-traffic-total"></b>
                    </div>
                    <div class="col-md-4">
                        Tổng trước đó: <b class="js-traffic-before"> </b>
                    </div>
                    <div class="col-md-4">
                        Tỉ lệ phát triển: <b class="js-traffic-rate"></b>
                    </div>
                </div>
                <div class="chart-main" style="width: 100%; height: 300px"
                     data-id="{{$site->id}}">
                    <canvas id="chart-report-detail"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

