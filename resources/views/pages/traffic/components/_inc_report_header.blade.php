<div class="report-header">
    <div style="margin: 15px 0;font-size:16px" class="box-change-site d-flex">
        <a href="{{ route($routeReportAll) }}"
           style="cursor: pointer;color:#333;font-size:16px"
           class="back-to-overview">
            Tất cả site
        </a>
        <span style="padding:0 10px;font-size:20px">|</span>
        Nguồn site:
        <select class="js-select-change-site"
                id="select-change-site">
            <optgroup label="123 WORK">
                @foreach($sites as $item)
                    @if($item->site_url != 'https:/123job.vn')
                        <option @if($item->id == $site->id) selected @endif
                        value="{{ route($routeReportDetail, ['id' => $item->id]) }}">
                            {{$item->site_name}}</option>
                    @endif
                @endforeach
            </optgroup>
            <optgroup label="123 JOB">
                @foreach($sites as $item)
                    @if($item->site_url == 'https:/123job.vn')
                        <option @if($item->id == $site->id) selected @endif
                        value="{{ route($urlReportDetail, ['id' => $item->id]) }}">
                            {{$item->site_name}}</option>
                    @endif
                @endforeach
            </optgroup>
        </select>
    </div>

    {{-- Breadcrumb --}}
    <div class="box-breadcrumb" style="padding-right: 35px">
        <ul class="breadcrumb-list">
            <li class="breadcrumb-item">Report</li>
            <li class="breadcrumb-item" style="white-space: nowrap;">{{$site->site_name}}</li>
        </ul>
    </div>
</div>