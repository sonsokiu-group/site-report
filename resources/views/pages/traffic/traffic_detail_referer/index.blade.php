@extends(__layout_master_core())
@section('title', "Báo cáo chi tiết traffic")

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/site-report/css/report.css') }}">
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 js-header-fix">
            @include('packages.site-report::pages.traffic.components._inc_report_header',
                      [
                          'routeReportAll' => 'get.traffic_home.index',
                          'routeReportDetail' => 'get.traffic_referer.index'
                      ])

            <div style="display: flex;justify-content: space-between">
                @include('packages.site-report::components._inc_nav_tab')

                @include('packages.site-report::components._inc_box_filter',
                          ['action'=> route('get.traffic_referer.index', ['id' => $site->id])])
            </div>
        </div>

        <div class="col-md-12 report-content">
            @include('packages.site-report::pages.traffic.traffic_detail_referer.components.inc_box_chart_top')
{{--            @include('packages.site-report::pages.traffic.traffic_detail_referer.components.inc_chart_pie')--}}
{{--            @include('packages.site-report::pages.traffic.traffic_detail_referer.components.inc_list_chart_line_group')--}}
            @include('packages.site-report::pages.traffic.traffic_detail_referer.components.inc_table_group_referer')
            @include('packages.site-report::pages.traffic.traffic_detail_referer.components.inc_table_detail_referer')
        </div>
    </div>

    @include('packages.site-report::pages.traffic.components._inc_modal_report_detail', [
                'urlGetDataDetail' => route('get.traffic_data.getDataChartDetailReferer'),
                'modalType' => 'reportReferer'
            ])
    <input type="hidden" class="js-day" value="{{ form_query('day', $query, 30) }}">
    @include('packages.site-report::components._inc_scroll_top')
@stop

@section('script')
    <script src="{{ mix('js/chartJs.min.js', '/vendor/site-report') }}"></script>
    <script>
        var URL_GET_DATA_TRAFFIC_LINE_CHART = '{{route('get.traffic_data.getDataLineChart')}}';
        var URL_GET_DATA_PIE_CHART = '{{route('get.traffic_data.getDataPieChart')}}';
        var URL_GET_DATA_BAR_CHART = '{{route('get.traffic_data.getDataBarChart')}}';
        var SITE_ID = '{{$site->id}}';
    </script>
    <script src="{{ mix('js/traffic_detail_referer.js','/vendor/site-report') }}"></script>
@stop
