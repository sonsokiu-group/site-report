<div class="row">
    <div class="col-md-6">
        <div class="box-chart">
            <div class="chart-header">
                <div class="box-title">
                    <b>Nguồn traffic vào trang chính</b>
                </div>
                <div class="group-report">
                    <label for="" class="js-referer-report active"
                           data-referer="google">
                        Google</label>
                    <label for="" class="js-referer-report"
                           data-referer="facebook">
                        Facebook</label>
                </div>
            </div>
            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-4">
                    Tổng traffic: <b class="js-total-current"> </b> lượt
                </div>
                <div class="col-md-4">
                    Tỉ lệ phát triển: <b class="js-growth-rate"> </b>
                </div>
                <div class="col-md-4">
                    Số traffic mới: <b class="js-traffic-new"> 0</b>
                </div>
            </div>
            <div class="chart-main js-dom-chart" style="height: 450px" data-id="{{$site->id}}"
                 data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_MAIN }}">
                <canvas class="js-canvas"></canvas>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box-chart">
            <div class="chart-header">
                <div class="box-title">
                    <b>Nguồn traffic vào trang keyword</b>
                </div>
                <div class="group-report">
                    <label for="" class="js-referer-report active"
                           data-referer="google">
                        Google</label>
                    <label for="" class="js-referer-report"
                           data-referer="facebook">
                        Facebook</label>
                </div>
            </div>
            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-4">
                    Tổng traffic: <b class="js-total-current"> </b> lượt
                </div>
                <div class="col-md-4">
                    Tỉ lệ phát triển: <b class="js-growth-rate"> </b>
                </div>
                <div class="col-md-4">
                    Số traffic mới: <b class="js-traffic-new"> 0</b>
                </div>
            </div>
            <div class="chart-main js-dom-chart" style="height: 450px" data-id="{{$site->id}}"
                 data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_KEYWORD }}">
                <canvas class="js-canvas"></canvas>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box-chart">
            <div class="chart-header">
                <div class="box-title">
                    <b>Nguồn traffic vào trang category</b>
                </div>
                <div class="group-report">
                    <label for="" class="js-referer-report active"
                           data-referer="google">
                        Google</label>
                    <label for="" class="js-referer-report"
                           data-referer="facebook">
                        Facebook</label>
                </div>
            </div>
            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-4">
                    Tổng traffic: <b class="js-total-current"> </b> lượt
                </div>
                <div class="col-md-4">
                    Tỉ lệ phát triển: <b class="js-growth-rate"></b>
                </div>
                <div class="col-md-4">
                    Số traffic mới: <b class="js-traffic-new"> 0</b>
                </div>
            </div>
            <div class="chart-main js-dom-chart" style="height: 450px" data-id="{{$site->id}}"
                 data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_CATEGORY }}">
                <canvas class="js-canvas"></canvas>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box-chart">
            <div class="chart-header">
                <div class="box-title">
                    <b>Nguồn traffic vào trang remote</b>
                </div>
                <div class="group-report">
                    <label for="" class="js-referer-report active"
                           data-referer="google">
                        Google</label>
                    <label for="" class="js-referer-report"
                           data-referer="facebook">
                        Facebook</label>
                </div>
            </div>
            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-4">
                    Tổng traffic: <b class="js-total-current"> </b> lượt
                </div>
                <div class="col-md-4">
                    Tỉ lệ phát triển: <b class="js-growth-rate"> %</b>
                </div>
                <div class="col-md-4">
                    Số traffic mới: <b class="js-traffic-new"> 0</b>
                </div>
            </div>
            <div class="chart-main js-dom-chart" style="height: 450px" data-id="{{$site->id}}"
                 data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_REMOTE }}">
                <canvas class="js-canvas"></canvas>
            </div>
        </div>
    </div>
</div>
