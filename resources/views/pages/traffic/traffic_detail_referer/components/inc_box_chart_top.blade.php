<div class="row">
    <div class="col-12 col-lg-6">
        <div class="box-chart js-bar-chart-referer" data-id="{{$site->id}}"
             data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REFERER }}">
            <div class="chart-header">
                <div class="box-title">
                    <b>Traffic theo nguồn referer</b>
                </div>
            </div>
            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-4">
                    Tổng traffic: <b class="js-total-current"> </b> lượt
                </div>
                <div class="col-md-4">
                    Tỉ lệ phát triển: <b class="js-growth-rate"> %</b>
                </div>
            </div>
            <div class="chart-main" style="height: 400px;">
                <canvas id="chartBarTrafficReferer"></canvas>
            </div>
        </div>
    </div>

    <div class="col-12 col-lg-6">
        <div class="box-chart">
            <div class="chart-header">
                <div class="box-title">
                    <b>Traffic theo nguồn referer</b>
                </div>
            </div>
            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-4">
                    Tổng traffic: <b class="js-total-current"> </b> lượt
                </div>
                <div class="col-md-4">
                    Tỉ lệ phát triển: <b class="js-growth-rate"> %</b>
                </div>
            </div>
            <div class="chart-main js-dom-chart" style="height: 400px" data-id="{{$site->id}}"
                 data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REFERER }}">
                <canvas class="js-canvas"></canvas>
            </div>
        </div>
    </div>
</div>
