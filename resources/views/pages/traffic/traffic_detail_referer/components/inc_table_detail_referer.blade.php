@if(count($dataTableDetail))
<div class="box-table-statistic">
    <h4>Bảng thống kê lượt traffic chi tiết theo trang</h4>

    <table class="table table-hover table-bordered">
        <thead>
        <tr style="background-color: #00a65a; color: #ffffff">
            <th colspan="1" rowspan="2" width="15%">Trang redirect</th>
            @foreach(reset($dataTableDetail) as $keyReferer => $arrayValue)
                @if($keyReferer == "total")
                    @continue
                @endif
                <th width="" colspan="1" class="text-center">
                    <div class="flex-space-between">
                        <span>Tỉ lệ</span>
                        <span>{{$keyReferer}}</span>
                        <span>Kì trước</span>
                    </div>
                </th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($dataTableDetail as $keyReport => $arrayData)
            @if($keyReport == "total")
                @continue
            @endif
            <tr>
                <td class="flex-space-between">
                    <span>{{$keyReport}}</span>
{{--                    <span class="js-show-modal-detail btn-show-modal-detail" data-type="{{$keyReport}}">Chi tiết</span>--}}
                </td>

                @foreach($arrayData as $keyReferer => $arrayValue)
                    <td class="text-center" colspan="1">
                        <div class="text-center" style="position: relative">
                            <span class="absolute-top-left"> {{ round(($arrayValue["total"] / $dataTableGroup["total"]), 3) * 100 . "%" }} </span>
                            <span>
                                <b>{{$arrayValue["total"]}}</b>
                                @if($arrayValue["compare_last_period"] > 0)
                                    <span class="text-success">
                                <i class="fa fa-long-arrow-up"></i>
                                    {{ $arrayValue["compare_last_period"] . "%"}}
                            </span>
                                @else
                                    <span class="text-danger">
                                <i class="fa fa-long-arrow-down"></i>
                                    {{ $arrayValue["compare_last_period"] . "%"}}
                            </span>
                                @endif
                            </span>
                            <span class="absolute-top-right">{{ $arrayValue["total_last_period"] }}</span>
                        </div>
                    </th>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif
