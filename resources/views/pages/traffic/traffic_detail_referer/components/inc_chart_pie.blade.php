<div class="row">

    <div class="col-md-6">
        <div class="box-chart js-pie-chart-google" data-id="{{$site->id}}" data-referer="google">
            <div class="chart-header">
                <div class="box-title">
                    <b>Chi tiết traffic nhóm theo Google</b>
                </div>
                <div class="group-report">
                    <label for="" class="js-type-report active"
                           data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_KEYWORD }}">
                        Keyword</label>
                    <label for="" class="js-type-report"
                           data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_CATEGORY }}">
                        Category</label>
                    <label for="" class="js-type-report"
                           data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_REMOTE }}">
                        Remote</label>
                </div>
            </div>
            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-4">
                    Tổng traffic: <b class="js-total-current"> </b> lượt
                </div>
                <div class="col-md-4">
                    Tỉ lệ phát triển: <b class="js-growth-rate"> %</b>
                </div>
            </div>
            <div class="chart-main" style="height: 400px;">
                <canvas id="chartPieGroupGoogle"></canvas>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box-chart js-pie-chart-facebook" data-id="{{$site->id}}" data-referer="facebook">
            <div class="chart-header">
                <div class="box-title">
                    <b>Chi tiết traffic nhóm theo Facebook</b>
                </div>
                <div class="group-report">
                    <label for="" class="js-type-report active"
                           data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_KEYWORD }}">
                        Keyword</label>
                    <label for="" class="js-type-report"
                           data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_CATEGORY }}">
                        Category</label>
                    <label for="" class="js-type-report"
                           data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_REMOTE }}">
                        Remote</label>
                </div>
            </div>
            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-4">
                    Tổng traffic: <b class="js-total-current"> </b> lượt
                </div>
                <div class="col-md-4">
                    Tỉ lệ phát triển: <b class="js-growth-rate"> %</b>
                </div>
            </div>
            <div class="chart-main" style="height: 400px;">
                <canvas id="chartPieGroupFacebook"></canvas>
            </div>
        </div>
    </div>
</div>
