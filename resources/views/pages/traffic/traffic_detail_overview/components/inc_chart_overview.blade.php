<div class="row">
    <div class="col-md-9">
        <h3 style="margin-bottom: 1.5rem;"><b>Biểu đồ traffic</b></h3>
        <div class="box-chart" style="position: relative">
            <div class="chart-nav-tab js-row-tab">
                @php($firstElement = reset($overViewTab))
                @foreach($overViewTab as $key => $nameTab)
                    <div class="item-tab {{($firstElement === $nameTab) ? "active" : "" }}" data-type="{{$key}}">
                        <div class="item-tab-title ">{{$nameTab}}</div>
                    </div>
                @endforeach
            </div>

            <div class="btn-nav-move nav-pre js-nav-move"><i class="fa fa-chevron-left"></i></div>
            <div class="btn-nav-move nav-next js-nav-move"><i class="fa fa-chevron-right"></i></div>

            <div class="chart-main" style="height: 450px" data-id="{{$site->id}}">
                <canvas id="js-chart-overview"></canvas>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <h3 style="margin-bottom: 1.5rem;"><b>Thông số</b></h3>
        <div class="table-content-chart">
            <div class="table-item">
                <div class="item-title">Tổng số traffic hiện tại</div>
                <div class="item-value js-data-table-now"></div>
            </div>
            <div class="table-item">
                <div class="item-title">Tổng số traffic trước đó</div>
                <div class="item-value js-data-table-before"></div>
            </div>
            <div class="table-item">
                <div class="item-title">Tỉ lệ tăng trưởng</div>
                <div class="item-value js-data-table-rate"></div>
            </div>
        </div>
    </div>
</div>
