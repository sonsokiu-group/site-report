<div class="row">
    @foreach($listChart as $chartItem)
        <div class="col-md-6">
            <div class="box-chart">
                <div class="chart-header">
                    <div class="box-title">
                        <b>{{ $chartItem["chartName"] }}</b>
                    </div>
                    <a class="btn-see-details"
                       href="{{ route($chartItem["routeDetail"], ['id' => $site->id]) }}">Xem chi tiết</a>
                </div>
                <div class="chart-main js-dom-chart" style="height: 450px" data-id="{{$site->id}}"
                     data-type="{{$chartItem["chartType"]}}">
                    <canvas class="js-canvas"></canvas>
                </div>
            </div>
        </div>
    @endforeach
</div>