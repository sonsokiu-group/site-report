{{--Bảng thống kê chi tiết traffic theo loại trang--}}
@if($dataTable != [])
<div class="mt-5" style="background-color: #ffffff; overflow-x: scroll">
    <table class="table table-hover table-bordered">
        <thead>
        <tr style="background-color: #00a65a; color: #ffffff">
            <th width="10%" style="min-width: 120px">Ngày</th>
            @foreach( reset($dataTable) as $key => $value)
                <th width="10%" style="min-width: 120px" class="text-center">{{$key}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($dataTable as $date => $groupData)
            <tr>
                <td>{{$date}}</td>
                @foreach($groupData as $key => $value)
                    @if($groupData["total"] !== 0)
                    <td class="text-center">
                        <span class=""> {{ $value }} </span>
                        @if($key !== "total")
                            <span class="text-success"> ( {{ round(($value / $groupData["total"]) * 100, 2 ) . "%" }} )</span>
                        @endif
                    </td>
                    @else
                        <td class="text-center">
                            <span class=""> {{ $value }} </span>
                        </td>
                    @endif
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif
