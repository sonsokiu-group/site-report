<div class="row">
    @foreach($listChartGroupRedirect as $chartItem)
        <div class="col-md-6">
            <div class="box-chart">
                <div class="chart-header">
                    <div class="box-title">
                        <b>Tổng traffic nhóm trang {{$chartItem["chartName"]}}</b>
                    </div>
                </div>
                <div class="row" style="padding: 0 15px;margin-bottom:20px">
                    <div class="col-md-4">
                        Tổng traffic: <b class="js-total-current"> </b> lượt
                    </div>
                    <div class="col-md-4">
                        Tỉ lệ phát triển: <b class="js-growth-rate"> </b>
                    </div>
                </div>
                <div class="chart-main js-dom-chart" style="height: 450px" data-id="{{$site->id}}"
                     data-type="{{ $chartItem["chartType"] }}">
                    <canvas class="js-canvas"></canvas>
                </div>
            </div>
        </div>
    @endforeach
</div>
