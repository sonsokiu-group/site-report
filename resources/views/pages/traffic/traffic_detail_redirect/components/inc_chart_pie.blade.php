<div class="row">
    <div class="col-md-6">
        <div class="box-chart" style="margin-bottom: 30px">
            <div class="chart-header">
                <div class="box-title">
                    <b>Tổng traffic theo nhóm</b>
                </div>
            </div>
            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-4">
                    Tổng traffic: <b class="js-total-current"> </b> lượt
                </div>
                <div class="col-md-4">
                    Tỉ lệ phát triển: <b class="js-growth-rate"> </b>
                </div>
            </div>
            <div class="chart-main js-pie-chart" style="height: 400px;"
                 data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT }}"
                 data-name="group">
                <canvas id="chartPieGroupOverview" class="js-canvas-pie-chart"></canvas>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box-chart" style="margin-bottom: 30px">
            <div class="chart-header">
                <div class="box-title">
                    <b>Traffic chi tiết từng nhóm</b>
                </div>
                <div class="group-report">
                    <label for="" class="js-type-report active"
                           data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_KEYWORD }}">
                        Keyword</label>
                    <label for="" class="js-type-report"
                           data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_CATEGORY }}">
                        Category</label>
                    <label for="" class="js-type-report"
                           data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_REMOTE }}">
                        Remote</label>
                </div>
            </div>
            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-4">
                    Tổng traffic: <b class="js-total-current"> </b> lượt
                </div>
                <div class="col-md-4">
                    Tỉ lệ phát triển: <b class="js-growth-rate"> </b>
                </div>
            </div>
            <div class="chart-main js-pie-chart" style="height: 400px;"
                 data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT_KEYWORD }}"
                 data-name="detail">
                <canvas id="chartPieGroupDetail" class="js-canvas-pie-chart"></canvas>
            </div>
        </div>
    </div>
</div>
