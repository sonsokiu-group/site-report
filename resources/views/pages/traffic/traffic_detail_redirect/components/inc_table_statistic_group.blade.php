@if(count($dataTableGroup))
    <div class="box-table-statistic">
        <h4>Bảng thống kê lượt traffic theo nhóm trang</h4>
        <table class="table table-hover table-bordered">
            <thead>
            <tr style="background-color: #00a65a; color: #ffffff">
                <th colspan="1" rowspan="" width="100px"></th>
                <th width="" colspan="3" class="text-center">Hiện tại</th>
                <th width="" colspan="3" class="text-center">Cùng kì</th>
            </tr>
            </thead>
            <thead>
            <tr style="background-color: #00a65a; color: #ffffff">
                <th style="width: 13%">Trang redirect</th>
                @foreach($dataTableGroup["timePeriod"] as $period)
                    <th class="text-center" colspan="1">
                        <div class="text-center" style="position: relative">
                            <span class="absolute-top-left">(Tháng trước)</span>
                            <span>{{ $period }}</span>
                            <span class="absolute-top-right">(Kì trước)</span>
                        </div>
                    </th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach($dataTableGroup["data"] as $keyReport => $groupData)
                <tr>
                    <td class="flex-space-between">
                        <span>{{$keyReport}}</span>
                        <span class="js-show-modal-detail btn-show-modal-detail"
                              data-type="{{$keyReport}}">Chi tiết</span>
                    </td>
                    @foreach($dataTableGroup["timePeriod"] as $period)
                        @php
                            $arrayValue = $groupData[$period] ?? [];
                            $compare_last_month = $arrayValue['compare_last_month'] ?? 0;
                            $total = $arrayValue['total'] ?? 0;
                            $compare_last_period = $arrayValue['compare_last_period'] ?? 0;
                        @endphp
                        <td>
                            <div class="text-center" style="position: relative">
                                @if($compare_last_month > 0)
                                    <span class="text-success absolute-top-left">
                                         <i class="fa fa-long-arrow-up"></i>
                                        {{ abs($compare_last_month) . "%" }}
                                    </span>
                                @elseif($compare_last_month < 0)
                                    <span class="text-danger absolute-top-left">
                                        <i class="fa fa-long-arrow-down"></i>
                                        {{ abs($compare_last_month) . "%" }}
                                    </span>
                                @else
                                    <span></span>
                                @endif

                                <span>{{ $total }}</span>

                                @if($compare_last_period > 0)
                                    <span class="text-success absolute-top-right">
                                        <i class="fa fa-long-arrow-up"></i>
                                        {{ abs($compare_last_period) . "%" }}
                                    </span>
                                @elseif($compare_last_period < 0)
                                    <span class="text-danger absolute-top-right">
                                        <i class="fa fa-long-arrow-down"></i>
                                        {{ abs($compare_last_period) . "%" }}
                                    </span>
                                @else
                                    <span></span>
                                @endif
                            </div>
                        </td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif
