<div class="row">
    <div class="col-md-12">
        <div class="box-chart">
            <div class="chart-header">
                <div class="box-title">
                    <b>Traffic theo nhóm trang</b>
                </div>
            </div>
            <div class="row" style="padding: 0 15px;margin-bottom:20px">
                <div class="col-md-4">
                    Tổng traffic: <b class="js-total-current"> </b> lượt
                </div>
                <div class="col-md-4">
                    Tỉ lệ phát triển: <b class="js-growth-rate"> </b>
                </div>
            </div>
            <div class="chart-main js-dom-chart" style="height: 450px" data-id="{{$site->id}}"
                 data-type="{{ \Workable\SiteReport\Enum\ReportTrafficTypeEnum::CHART_GROUP_REDIRECT }}">
                <canvas class="js-canvas"></canvas>
            </div>
        </div>
    </div>
</div>
