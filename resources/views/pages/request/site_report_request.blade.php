@extends(__layout_master_core())
@section('title', "Báo cáo chi tiết request")

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/site-report/css/report.css') }}">
    <style>
        .dt-scroll-body {
            max-height: 530px !important;
            min-height: 530px !important;
        }

        .box-change-site {
            align-items: center;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
            background-color: #00a65a !important;
        }

        .w-60-sm {
            width: 66.668%;
        }

        @media screen and (max-width: 992px) {
            .w-60-sm {
                width: 100%
            }
        }
        .h-100 {
            height: 100%;
        }
    </style>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12 js-header-fix">
            @include('packages.site-report::pages.traffic.components._inc_report_header',
                      [
                          'routeReportAll' => 'get.traffic_home.index',
                          'routeReportDetail' => 'get.overview.request'
                      ])

            <div style="display: flex;justify-content: space-between">
                @include('packages.site-report::components._inc_nav_tab')
            </div>
        </div>
        <div class="col-md-12">
            <div style="margin-top: 20px">
                <ul id="myTab" class="nav nav-tabs">
                    <li class="active"><a href="#request" data-toggle="tab">Requests Detail</a></li>
                    <li class=""><a href="#page" data-toggle="tab">Requests Page</a></li>
                </ul>
                <div class="d-flex" style="margin-top: 20px; flex-wrap: wrap">
                    <div class="w-60-sm">
                        <div class="tab-content h-100" style="padding: 0">
                            <div class="tab-pane active" id="request">
                                @include("plugins.request-report::pages.robot_detail.robot_detail_chart", ["sites" => array($site), "page" => "detail"])
                            </div>
                            <div class="tab-pane h-100" id="page">
                                @include("plugins.request-report::pages.robot_detail.robot_page_detail_chart", ["sites" => array($site), "page" => "detail"])
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12" style="margin-bottom: 20px">
                        <div class="bg-white info-request" style="padding: 10px 20px; height: 100%; ">
                            <div class="d-flex justify-content-between align-items-center" style="margin-bottom: 10px">
                                <div style="font-size: 1.1rem;margin: 5px 0; font-weight: bold">Biểu đồ loại trang Request</div>
                                <div class="">
                                    <form class="form-inline" method="get">
                                        <div class="form-group">
                                            <select class="form-control" name="day_ago_chart_pie"
                                                    onchange="this.form.submit()">
                                                <option value="7" {{ query_selected("day_ago_chart_pie", 7, 7) }}>
                                                    7 ngày
                                                </option>
                                                <option value="14" {{ query_selected("day_ago_chart_pie", 14, 7) }}>
                                                    14 ngày
                                                </option>
                                                <option value="30" {{ query_selected("day_ago_chart_pie", 30, 7) }}>
                                                    30 ngày
                                                </option>
                                            </select>
                                        </div>
                                        <input class="hidden" name="day_ago_table" type="text"
                                               value="{{ form_query("day_ago_table", $query, 7) }}">
                                        <input class="hidden" name="day_ago_chart_line" type="text"
                                               value="{{ form_query("day_ago_chart_line", $query, 7) }}">
                                    </form>
                                    {!! get_error($errors,'year') !!}
                                </div>
                            </div>
                            <div class="container-chart" style="margin-top: 20px">
                                <canvas class="chart-request-pie"
                                        id="chart-request-pie-{{ $site->country }}"
                                        data-country="{{ $site->country }}"
                                        data-url="{{ $site->site_url }}">
                                </canvas>
                                <span class="js-not-data hidden"
                                      style="position: absolute; top: 50%; right: 50%; transform: translateX(50%)">Không có dữ liệu</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include("plugins.request-report::pages.robot_detail.robot_detail_table", ["siteActive" => $site])
        </div>
    </div>

    <input type="hidden" class="js-day" value="{{ form_query('day', $query, 30) }}">
    @include('packages.site-report::components._inc_scroll_top')
@stop

@section('script')
    <script src="{{ mix('js/traffic_detail_overview.js','/vendor/site-report') }}"></script>
@stop
