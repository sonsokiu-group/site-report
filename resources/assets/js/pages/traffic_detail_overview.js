import SITE_REPORT_JS_CORE from "../core/index";
import {JS_CONFIG_CHART, JS_TRANSFORM_DATA_CHART} from "../core/config_chart";

const JS_TRAFFIC_DETAIL_OVERVIEW = {
    init() {
        this.loadLineChartGroup();
        this.loadChartOverview();
        this.handleEvent();
    },

    loadLineChartGroup() {
        //render chart line "group_redirect" and "group_referer"
        $('.js-dom-chart').each(function () {
            let domChart = $(this);
            let site_id = domChart.data("id");
            let typeChart = domChart.data("type");
            let dayReport = $('.js-day').val();

            $.ajax({
                type: "GET",
                url: URL_GET_DATA_TRAFFIC_LINE_CHART,
                data: {
                    site_id: site_id,
                    day: dayReport,
                    type: typeChart
                }
            }).done(function (res) {
                if (res.code === 1) {
                    let chartCanvas = domChart.find(".js-canvas").get(0).getContext("2d");
                    let dataChart = JS_TRANSFORM_DATA_CHART.renderDataLineChart(res.data);

                    new Chart(chartCanvas, {
                        type: "line",
                        data: {
                            labels: dataChart.labels,
                            datasets: dataChart.datasets
                        },
                        options: JS_CONFIG_CHART.chartLineOption
                    });
                }
            });
        });
    },

    loadChartOverview() {
        let _this = this;
        let dayReport = $('.js-day').val();
        let site_id = SITE_ID;
        let typeChart = $('.js-row-tab .item-tab.active').data('type'); // type chart traffic overview

        // init chart
        if (!window.myChartOverview) {
            let chartCanvas = $('#js-chart-overview');

            window.myChartOverview = new Chart(chartCanvas, {
                type: "line",
                data: {},
                options: JS_CONFIG_CHART.chartLineOption,
                plugins: [
                    JS_CONFIG_CHART.pluginChartLine
                ]
            });
        }

        _this.__getDataChartOverview(dayReport, site_id, typeChart);
    },

    handleEvent() {
        let _this = this

        $('.js-row-tab .item-tab').on('click', function () {
            $('.js-row-tab .item-tab').removeClass('active');
            $(this).addClass('active');
            let selectedPosition = $(this).position().left;

            $('.js-row-tab').animate({
                scrollLeft: selectedPosition
            }, 300);

            let dayReport = $('.js-day').val();
            let typeReport = $(this).data('type');

            _this.__getDataChartOverview(dayReport, SITE_ID, typeReport);
        });

        $('.js-nav-move.nav-pre').on('click', function () {
            let $list = $('.js-row-tab');

            $list.animate({
                scrollLeft: '-=200px'
            }, 300);
        });

        $('.js-nav-move.nav-next').on('click', function () {
            let $list = $('.js-row-tab');

            $list.animate({
                scrollLeft: '+=200px'
            }, 300);
        });
    },

    __getDataChartOverview(dayChart, siteId, typeReport) {
        let _this = this;

        $.ajax({
            type: "GET",
            url: URL_GET_DATA_CHART_TRAFFIC_OVERVIEW,
            data: {
                site_id: siteId,
                day: dayChart,
                type: typeReport
            }
        }).done(function (res) {
            if (res.code === 1) {
                _this.__renderDataChartOverview(res.data);
                _this.__fillDataTableOverview(res.data);
            }
        }).fail(function (err) {

        });
    },

    __renderDataChartOverview(data) {
        let dataStatistics = {
            labels: data["dates"],
            dataUnit: "lượt",
            lineTension: .4,
            datasets: [
                {
                    yAxisID: 'yAxes1',
                    label: "Cũ",
                    backgroundColor: "rgba(151,187,205,0.2)",
                    borderColor: "rgba(151,187,205,1)",
                    pointBackgroundColor: "#ccc",
                    pointBorderColor: "rgba(151,187,205,1)",
                    pointHoverBackgroundColor: "#ccc",
                    pointHoverBorderColor: "rgba(151,187,205,1)",
                    data: data["dataChartBefore"],
                    borderDash: [5, 5], // Add borderDash to make it dashed
                },
                {
                    yAxisID: 'yAxes1',
                    label: "Hiện tại",
                    backgroundColor: "rgba(204,204,204,0.2)",
                    borderColor: "rgba(0,146,236,0.83)",
                    pointBackgroundColor: "rgba(0,146,236,1)",
                    pointBorderColor: "rgba(0,146,236,1)",
                    pointHoverBackgroundColor: "#fff",
                    data: data["dataChart"],
                    fill: 'origin',
                }
            ]
        };
        let chart_data = [];

        for (let i = 0; i < dataStatistics.datasets.length; i++) {
            chart_data.push({
                label: dataStatistics.datasets[i].label,
                lineTension: .15,
                backgroundColor: dataStatistics.datasets[i].backgroundColor,
                borderWidth: 1.8,
                borderColor: dataStatistics.datasets[i].borderColor,
                pointBorderColor: dataStatistics.datasets[i].pointBorderColor,
                pointBackgroundColor: dataStatistics.datasets[i].pointBackgroundColor,
                pointHoverBackgroundColor: dataStatistics.datasets[i].pointHoverBackgroundColor,
                pointHoverBorderColor: dataStatistics.datasets[i].pointHoverBorderColor,
                // pointBorderWidth: 1.5,
                // pointHoverRadius: 3,
                // pointHoverBorderWidth: 1,
                // dash: 0,
                borderDash: dataStatistics.datasets[i].borderDash ?? [],
                fill: dataStatistics.datasets[i].fill ?? false,
                // pointRadius: 3,
                // pointHitRadius: 3,
                data: dataStatistics.datasets[i].data,
            });
        }

        window.myChartOverview.data.labels = dataStatistics.labels;
        window.myChartOverview.data.datasets = chart_data;
        window.myChartOverview.update();
    },

    __fillDataTableOverview(data) {
        let dataCompare = data["dataCompare"];
        let trafficCurrent = dataCompare["totalCurrent"];
        let trafficBefore = dataCompare["totalBefore"];
        let growthRate = Number(dataCompare["growthRate"]);
        let arrowIconClass = (growthRate > 0) ? "fa fa-long-arrow-up" : "fa fa-long-arrow-down";
        let colorTextClass = (growthRate > 0) ? "text-success" : "text-danger";
        let html = `<span class="${colorTextClass}"><i class="${arrowIconClass}" style="margin-right: 6px"></i>${growthRate + '%'}</span>`;

        $('.js-data-table-now').text(trafficCurrent);
        $('.js-data-table-before').text(trafficBefore);
        $('.js-data-table-rate').html(html);
    },

};

$(function () {
    SITE_REPORT_JS_CORE.init();
    JS_TRAFFIC_DETAIL_OVERVIEW.init();
});
