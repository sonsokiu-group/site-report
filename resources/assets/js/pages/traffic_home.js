import SITE_REPORT_JS_CORE from "../core/index";
import {JS_CONFIG_CHART, JS_TRANSFORM_DATA_CHART} from "../core/config_chart";

const JS_REPORT_TRAFFIC_HOME = {
    init() {
        this.loadChartLineOverview();
        this.initChartAllSite();
    },

    loadChartLineOverview() {
        $('.js-dom-chart').each(function () {
            let $boxChart = $(this).closest('.box-chart');
            let site_id = $(this).data("id");
            let typeChart = $(this).data("type");
            let dayReport = $('.js-day').val();

            $.ajax({
                type: "GET",
                url: URL_GET_DATA_TRAFFIC_LINE_CHART,
                data: {
                    site_id: site_id,
                    day: dayReport,
                    type: typeChart
                }
            }).done(function (res) {
                if (res.code === 1) {
                    //1. Render chart
                    let chartCanvas = document.getElementById("chart-overview-site-" + site_id).getContext("2d");
                    let dataChart   = JS_TRANSFORM_DATA_CHART.renderDataLineChart(res.data);

                    new Chart(chartCanvas, {
                        type: "line",
                        data: {
                            labels: dataChart.labels,
                            datasets: dataChart.datasets
                        },
                        options: JS_CONFIG_CHART.chartLineHomeOption
                    });

                    //2. Handle data compare chart
                    JS_TRANSFORM_DATA_CHART.fillDataCompareChart($boxChart, res.data["dataCompare"]);
                }
            });
        });
    },

    initChartAllSite() {
        let _this = this;
        let dayReport = $('.js-day').val();

        // Init chart
        if (!window.myChartAllSite) {
            let chart = $('#chart-bar-all-site').get(0).getContext("2d");

            window.myChartAllSite = new Chart(chart, {
                type: 'bar',
                data: {},
                options: JS_CONFIG_CHART.chartBarHomeOption,
                plugins: [
                    JS_CONFIG_CHART.pluginChartColumnAllSite
                ]
            });
        }

        _this.__getDataChartAllSite(dayReport);
    },

    __getDataChartAllSite(dayReport) {
        let _this = this;
        let sites = {};

        $('.js-dom-chart').each(function () {
            let site_id = $(this).data("id");
            sites[site_id] = $(this).data("name");
        });

        $.ajax({
            type: "GET",
            url: URL_GET_DATA_CHART_TRAFFIC_ALL,
            data: {
                day: dayReport,
                sites: sites
            }
        }).done(function (res) {
            if (res.code === 1) {
                _this.__renderDataChartAllSite(res.data);
                _this.__renderTemplateTrafficRank(res.data);
            }
        }).fail(function () {

        });
    },

    __renderDataChartAllSite(data) {
        let dataStatistics = {
            labels: data.label,
            dataUnit: data.label,
            lineTension: 0.4,

            datasets: [
                {
                    yAxisID: 'yAxes1',
                    label: "Kì trước",
                    backgroundColor: "rgba(237,2,92,0.1)",
                    type: "bar",
                    stack: "Base",
                    hoverBackgroundColor: "rgba(236,0,98,1)",
                    data: data["dataChartBefore"],
                    fill: 'true',
                    categoryPercentage: 0.5,
                    barPercentage: 2,
                },
                {
                    yAxisID: 'yAxes1',
                    label: "Hiện tại",
                    backgroundColor: "rgba(7,157,255,0.13)",
                    type: "bar",
                    stack: "Base",
                    hoverBackgroundColor: "rgba(0,146,236,1)",
                    data: data["dataChart"],
                    fill: 'true',
                    categoryPercentage: 0.5,
                    barPercentage: 2,
                },
            ]
        };
        let chart_data = [];

        for (let i = 0; i < dataStatistics.datasets.length; i++) {
            chart_data.push({
                label: dataStatistics.datasets[i].label,
                lineTension: .15,
                backgroundColor: dataStatistics.datasets[i].backgroundColor,
                borderWidth: 1,
                borderColor: dataStatistics.datasets[i].borderColor,
                pointBorderColor: dataStatistics.datasets[i].pointBorderColor,
                pointBackgroundColor: dataStatistics.datasets[i].pointBackgroundColor,
                pointHoverBackgroundColor: dataStatistics.datasets[i].pointHoverBackgroundColor,
                pointHoverBorderColor: dataStatistics.datasets[i].pointHoverBorderColor,

                borderDash: dataStatistics.datasets[i].borderDash ?? [],
                fill: dataStatistics.datasets[i].fill ?? false,

                data: dataStatistics.datasets[i].data,
            });
        }

        window.myChartAllSite.data.labels = dataStatistics.labels;
        window.myChartAllSite.data.datasets = chart_data;
        window.myChartAllSite.update();
    },

    __renderTemplateTrafficRank(data) {
        let dataSort = data["dataSort"];
        let html = "";
        let rank = 1;

        $.each(dataSort, function (siteName, traffic, index) {
            html += `
                <div class="item">
                    <div class="item-name">
                        ${rank})  <span style="color:blue;font-size:16px">${siteName} </span>
                    </div>
                    <div class="item-traffic">
                        ${traffic} lượt traffic
                    </div>
                </div>
            `;

            rank++
        })
        $('.js-group-item-info').append(html);
    },
}

$(function () {
    SITE_REPORT_JS_CORE.init();
    JS_REPORT_TRAFFIC_HOME.init()
});
