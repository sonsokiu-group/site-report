import SITE_REPORT_JS_CORE from "../../core";
import JS_CONFIG_CHART_KEYWORD from "../../core/config_chart_keyword";
import {JS_TRANSFORM_DATA_CHART} from "../../core/config_chart";

const JS_KW_LEVER_HOME = {
    init() {
        this.renderChart();
        this.renderChartNew();
    },

    renderChart() {
        $('.js-dom-chart').each(function () {
            let site_id = $(this).data("id");
            let $boxChart = $(this).closest(".box-chart");
            let type = $(this).data("type");
            let options = (type === "bar") ? JS_CONFIG_CHART_KEYWORD.chartBarOption : JS_CONFIG_CHART_KEYWORD.chartPieOption;
            let plugins = (type === "bar") ? JS_CONFIG_CHART_KEYWORD.pluginChartColumnKw : JS_CONFIG_CHART_KEYWORD.pluginChartPie;

            let chartCanvas = $boxChart.find('.js-canvas').get(0).getContext("2d");

            $.ajax({
                type: "GET",
                url: URL_CHART_KW_LEVEL,
                data: {
                    site_id: site_id
                }
            }).done(function (res) {
                if (res.code === 1) {
                    //1. Render chart
                    let dataChart = transformDataChart(res.data);

                    new Chart(chartCanvas, {
                        type: type,
                        data: {
                            labels: dataChart.labels,
                            datasets: dataChart.datasets
                        },
                        options: options,
                        plugins: [plugins]
                    });

                    //2. Handle data compare chart
                    JS_TRANSFORM_DATA_CHART.fillDataCompareChart($boxChart, res.data["dataCompare"]);
                }
            });
        });

        function transformDataChart(data) {
            let arrayDataChart = data["dataChart"];
            let labels = [];
            let chartData = [];
            let background = [];
            let indexBackground = 0;

            $.each(arrayDataChart, function (key, value) {
                labels.push(key);
                chartData.push(value);
                background.push(JS_CONFIG_CHART_KEYWORD.background[indexBackground]);
                ++indexBackground;
            });

            let dataStatistics = {
                labels: labels,
                dataUnit: 'keyword',
                legend: false,
                datasets: [{
                    borderColor: "#fff",
                    background: background,
                    data: chartData
                }]
            };

            let chart_data = [];

            for (let i = 0; i < dataStatistics.datasets.length; i++) {
                chart_data.push({
                    backgroundColor: dataStatistics.datasets[i].background,
                    borderWidth: 2,
                    borderColor: dataStatistics.datasets[i].borderColor,
                    hoverBorderColor: dataStatistics.datasets[i].borderColor,
                    data: dataStatistics.datasets[i].data,
                });
            }

            return {
                labels: labels,
                datasets: chart_data,
            }
        }
    },

    renderChartNew() {
        $('.js-dom-chart-new').each(function () {
            let site_id = $(this).data("id");
            let $boxChart = $(this).closest(".box-chart");
            let type = $(this).data("type");
            let options = (type === "bar") ? JS_CONFIG_CHART_KEYWORD.chartBarOption : JS_CONFIG_CHART_KEYWORD.chartPieOption;
            let plugins = (type === "bar") ? JS_CONFIG_CHART_KEYWORD.pluginChartColumnKw : JS_CONFIG_CHART_KEYWORD.pluginChartPie;

            let chartCanvas = $boxChart.find('.js-canvas').get(0).getContext("2d");

            $.ajax({
                type: "GET",
                url: URL_CHART_KW_LEVEL,
                data: {
                    site_id: site_id
                }
            }).done(function (res) {
                if (res.code === 1) {
                    //1. Render chart
                    let dataChart = transformDataChart(res.data);

                    new Chart(chartCanvas, {
                        type: type,
                        data: {
                            labels: dataChart.labels,
                            datasets: dataChart.datasets
                        },
                        options: options,
                        plugins: [plugins]
                    });
                }
            });
        });

        function transformDataChart(data) {
            let arrayDataChart = data["dataChart"];
            let labels = [];
            let chartData = [];
            let background = [];
            let indexBackground = 0;

            $.each(arrayDataChart, function (key, value) {
                labels.push(key);
                chartData.push(value);
                background.push(JS_CONFIG_CHART_KEYWORD.background[indexBackground]);
                ++indexBackground;
            });

            let dataStatistics = {
                labels: labels,
                dataUnit: 'keyword',
                legend: false,
                datasets: [{
                    borderColor: "#fff",
                    background: background,
                    data: chartData
                }]
            };

            let chart_data = [];

            for (let i = 0; i < dataStatistics.datasets.length; i++) {
                chart_data.push({
                    backgroundColor: dataStatistics.datasets[i].background,
                    borderWidth: 2,
                    borderColor: dataStatistics.datasets[i].borderColor,
                    hoverBorderColor: dataStatistics.datasets[i].borderColor,
                    data: dataStatistics.datasets[i].data,
                });
            }

            return {
                labels: labels,
                datasets: chart_data,
            }
        }
    },
}

$(function () {
    SITE_REPORT_JS_CORE.init();
    JS_KW_LEVER_HOME.init();
});
