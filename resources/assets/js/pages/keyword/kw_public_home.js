import SITE_REPORT_JS_CORE from "../../core";
import {JS_CONFIG_CHART, JS_TRANSFORM_DATA_CHART} from "../../core/config_chart";

const JS_KW_PUBLIC_HOME = {
    init() {
        this.renderChart();
    },

    renderChart() {
        $('.js-dom-chart').each(function () {
            let $boxChart = $(this).closest('.box-chart');
            let site_id = $(this).data("id");
            let day = $('.js-day').val();

            let chartCanvas = $boxChart.find('.js-canvas').get(0).getContext("2d");

            $.ajax({
                type: "GET",
                url: URL_CHART_KW_PUBLIC,
                data: {
                    site_id: site_id,
                    day: day
                }
            }).done(function (res) {
                if (res.code === 1) {
                    //1. Render chart
                    let dataChart = JS_TRANSFORM_DATA_CHART.renderDataLineChart(res.data);

                    new Chart(chartCanvas, {
                        type: "line",
                        data: {
                            labels: dataChart.labels,
                            datasets: dataChart.datasets
                        },
                        options: JS_CONFIG_CHART.chartLineOption
                    });

                    //2. Fill data compare
                    JS_TRANSFORM_DATA_CHART.fillDataCompareChart($boxChart, res.data["dataCompare"])
                }
            });

        });
    }
}

$(function () {
    SITE_REPORT_JS_CORE.init();
    JS_KW_PUBLIC_HOME.init();
});