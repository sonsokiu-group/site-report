import {JS_CONFIG_CHART} from "../../core/config_chart";
import SITE_REPORT_JS_CORE from "../../core";

const JS_KW_EXTRACT_HOME = {
    init() {
        this.renderChartOverview();
    },

    renderChartOverview() {
        $('.js-dom-chart').each(function () {
            let $boxChart = $(this).closest('.box-chart');
            let site_id = $(this).data("id");
            let day = $('.js-day').val();

            let chartCanvas = $boxChart.find('.js-canvas').get(0).getContext("2d");

            $.ajax({
                type: "GET",
                url: CHART_OVERVIEW_EXTRACT,
                data: {
                    site_id: site_id,
                    day: day
                }
            }).done(function (res) {
                if (res.code === 1) {
                    //1. Render chart
                    let dataChart = transformDataChart(res.data);

                    new Chart(chartCanvas, {
                        type: "line",
                        data: {
                            labels: dataChart.labels,
                            datasets: dataChart.datasets
                        },
                        options: JS_CONFIG_CHART.chartLineOption,
                        plugins: [
                            JS_CONFIG_CHART.pluginChartLine
                        ]
                    });
                }
            });

        });

        function transformDataChart(data) {
            let labels = data["dates"];
            let dataStatistics = {
                labels: labels,
                dataUnit: "Tin",
                lineTension: .4,
                datasets: [
                    {
                        yAxisID: 'yAxes1',
                        label: "Số tin",
                        backgroundColor: "rgba(204,204,204,0.15)",
                        borderColor: "rgba(151,187,205,1)",
                        pointBackgroundColor: "#ccc",
                        pointBorderColor: "rgba(151,187,205,1)",
                        pointHoverBackgroundColor: "#ccc",
                        pointHoverBorderColor: "rgba(151,187,205,1)",
                        data: data["dataChart"]["count_jd"],
                        borderDash: [5, 5], // Add borderDash to make it dashed
                        fill: 'origin',
                    },
                    {
                        yAxisID: 'yAxes1',
                        label: "Keyword",
                        backgroundColor: "transparent",
                        borderColor: "rgba(0,146,236,0.83)",
                        pointBackgroundColor: "rgba(0,146,236,1)",
                        pointBorderColor: "rgba(0,146,236,1)",
                        pointHoverBackgroundColor: "#fff",
                        data: data["dataChart"]["extract_kw"],
                        fill: 'origin',
                    },
                    {
                        yAxisID: 'yAxes1',
                        label: "Location",
                        backgroundColor: "transparent",
                        borderColor: "rgba(225, 169, 206, 1)",
                        pointBackgroundColor: "rgba(225, 169, 206, 1)",
                        pointBorderColor: "rgba(225, 169, 206, 1)",
                        pointHoverBackgroundColor: "#fff",
                        data: data["dataChart"]["extract_location"],
                        fill: 'origin',
                    }
                ]
            };
            let chart_data = [];

            for (let i = 0; i < dataStatistics.datasets.length; i++) {
                chart_data.push({
                    label: dataStatistics.datasets[i].label,
                    lineTension: .15,
                    backgroundColor: dataStatistics.datasets[i].backgroundColor,
                    borderWidth: 1.8,
                    borderColor: dataStatistics.datasets[i].borderColor,
                    pointBorderColor: dataStatistics.datasets[i].pointBorderColor,
                    pointBackgroundColor: dataStatistics.datasets[i].pointBackgroundColor,
                    pointHoverBackgroundColor: dataStatistics.datasets[i].pointHoverBackgroundColor,
                    pointHoverBorderColor: dataStatistics.datasets[i].pointHoverBorderColor,
                    // pointBorderWidth: 1.5,
                    // pointHoverRadius: 3,
                    // pointHoverBorderWidth: 1,
                    // dash: 0,
                    borderDash: dataStatistics.datasets[i].borderDash ?? [],
                    fill: dataStatistics.datasets[i].fill ?? false,
                    // pointRadius: 3,
                    // pointHitRadius: 3,
                    data: dataStatistics.datasets[i].data,
                });
            }

            return {
                labels: dataStatistics.labels,
                datasets: chart_data,
            }
        }
    }
}

$(function () {
    SITE_REPORT_JS_CORE.init();
    JS_KW_EXTRACT_HOME.init();
});