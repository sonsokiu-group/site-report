import {JS_CONFIG_CHART, JS_TRANSFORM_DATA_CHART} from "../../core/config_chart";
import SITE_REPORT_JS_CORE from "../../core";

const JS_LOCATION_EXTRACT_HOME = {
    init() {
        this.renderChartOverview();
    },

    renderChartOverview() {
        $(document).ready(function () {
            $('.js-dom-chart').each(function () {
                let $boxChart = $(this).closest('.box-chart');
                let site_id = $(this).data("id");
                let dayReport = $('.js-day').val();

                let chartCanvas = $boxChart.find('.js-canvas').get(0).getContext("2d");

                $.ajax({
                    type: "GET",
                    url: CHART_LOCATION_EXTRACT,
                    data: {
                        site_id: site_id,
                        day: dayReport
                    }
                }).done(function (res) {

                    if (res.code === 1) {
                        //1. Render chart
                        let dataChart = JS_TRANSFORM_DATA_CHART.renderDataLineChartV2(res.data);

                        new Chart(chartCanvas, {
                            type: "line",
                            data: {
                                labels: dataChart.labels,
                                datasets: dataChart.datasets
                            },
                            options: JS_CONFIG_CHART.chartLineOption,
                            plugins: [JS_CONFIG_CHART.pluginChartLine]

                        });

                        //2. Fill data compare
                        let dataCompare = res.data.dataCompare;
                        let totalJd = dataCompare["totalJdExtract"];
                        let totalJdExtract = dataCompare["totalJd"];
                        let percentExtract = Number(dataCompare["percentExtract"]);
                        let arrowIconClass = (percentExtract > 0) ? "fa fa-long-arrow-up" : "fa fa-long-arrow-down";
                        let colorTextClass = (percentExtract > 0) ? "text-success" : "text-danger";
                        let html = `<span class="${colorTextClass}">${percentExtract + '%'}</span>`;

                        $boxChart.find('.js-total-current').text(totalJd);
                        $boxChart.find('.js-total-before').text(totalJdExtract);
                        $boxChart.find('.js-growthRate').html(html);
                    }
                });

            });
        })

    }

}

$(function () {
    SITE_REPORT_JS_CORE.init();
    JS_LOCATION_EXTRACT_HOME.init();
});
