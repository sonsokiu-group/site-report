import SITE_REPORT_JS_CORE from "../core/index";
import {JS_CONFIG_CHART, JS_TRANSFORM_DATA_CHART} from "../core/config_chart";

const JS_REPORT_TRAFFIC_PIE_CHART = {
    init() {
        this.loadChartPieGoogle();
        this.loadChartPieFacebook();
        this.filterChartPie();
    },

    loadChartPieGoogle(type = null) {
        let $domChart = $(`#chartPieGroupGoogle`);
        if ($domChart.length) {
            let _this = this;
            let dayReport = $('.js-day').val();
            let chartElement = $('.js-pie-chart-google');
            let site_id = chartElement.data('id');
            let typeChart = type ? type : chartElement.find('.js-type-report.active').data('type');
            let referer = chartElement.data('referer');

            if (!window.chartGroupGooglePie) {
                var canvasGroupGooglePie = $domChart.get(0).getContext("2d");

                window.chartGroupGooglePie = new Chart(canvasGroupGooglePie, {
                    type: 'pie',
                    data: {},
                    options: JS_CONFIG_CHART.chartPieOption
                });
            }

            $.ajax({
                url: URL_GET_DATA_PIE_CHART,
                type: "GET",
                data: {
                    referer: referer,
                    site_id: site_id,
                    day: dayReport,
                    type: typeChart
                }
            })
                .done(function (res) {
                    if (res.code === 1) {
                        //1. Render chart
                        // let dataChart = _this.__renderDataChartPie(res.data.data_chart)
                        let dataChart = JS_TRANSFORM_DATA_CHART.renderDataPieChart(res.data);

                        window.chartGroupGooglePie.data.labels = dataChart.labels;
                        window.chartGroupGooglePie.data.datasets = dataChart.datasets;
                        window.chartGroupGooglePie.update();

                        //2. Xử lý dữ liệu so sánh
                        JS_TRANSFORM_DATA_CHART.fillDataCompareChart(chartElement, res.data["dataCompare"]);
                    }
                })
                .fail(function (err) {
                    console.log(err)
                });
        }
    },

    loadChartPieFacebook(type = null) {
        let $domChart = $('#chartPieGroupFacebook');
        if ($domChart.length) {
            let _this = this;
            let dayReport = $('.js-day').val();
            let chartElement = $('.js-pie-chart-facebook');
            let site_id = chartElement.data('id');
            let typeChart = type ? type : chartElement.find('.js-type-report.active').data('type');
            let referer = chartElement.data('referer');

            if (!window.chartGroupFacebookPie) {
                var canvasGroupFacebookPie = $domChart.get(0).getContext("2d");

                window.chartGroupFacebookPie = new Chart(canvasGroupFacebookPie, {
                    type: 'pie',
                    data: {},
                    options: JS_CONFIG_CHART.chartPieOption
                });
            }

            $.ajax({
                url: URL_GET_DATA_PIE_CHART,
                type: "GET",
                data: {
                    referer: referer,
                    site_id: site_id,
                    day: dayReport,
                    type: typeChart
                }
            })
                .done(function (res) {
                    if (res.code === 1) {
                        // 1. Render chart
                        let dataChart = JS_TRANSFORM_DATA_CHART.renderDataPieChart(res.data);

                        window.chartGroupFacebookPie.data.labels = dataChart.labels;
                        window.chartGroupFacebookPie.data.datasets = dataChart.datasets;
                        window.chartGroupFacebookPie.update();

                        //2. Xử lý dữ liệu so sánh
                        JS_TRANSFORM_DATA_CHART.fillDataCompareChart(chartElement, res.data["dataCompare"]);

                    }
                })
                .fail(function (err) {
                    console.log(err)
                });
        }

    },

    filterChartPie() {
        let _this = this;

        $('.js-type-report').click(function (e) {
            e.stopPropagation();
            let typeReport = $(this).data("type");
            let $chartElement = $(this).closest('.box-chart');
            let referer = $chartElement.data("referer");
            $chartElement.find('.js-type-report').removeClass("active");
            $(this).addClass("active");

            if (referer === "google") {
                _this.loadChartPieGoogle(typeReport);
            } else {
                _this.loadChartPieFacebook(typeReport);
            }
        });
    }
}

const JS_REPORT_TRAFFIC_LINE_CHART = {
    init() {
        this.loadChart();
        this.filterChartLine();
    },

    loadChart() {
        let _this = this;

        $('.js-dom-chart').each(function () {
            let $boxChart = $(this).closest('.box-chart');
            let site_id = $(this).data("id");
            let typeChart = $(this).data("type");
            let dayReport = $('.js-day').val();
            let chartCanvas = $(this).find('.js-canvas').get(0).getContext("2d");
            let referer = $boxChart.find('.js-referer-report.active').data('referer');
            let $chart = "chartLineType_" + typeChart

            $.ajax({
                type: "GET",
                url: URL_GET_DATA_TRAFFIC_LINE_CHART,
                data: {
                    referer: referer,
                    site_id: site_id,
                    day: dayReport,
                    type: typeChart
                }
            }).done(function (res) {
                if (res.code === 1) {
                    //1. Render chart
                    let dataChart = JS_TRANSFORM_DATA_CHART.renderDataLineChart(res.data);

                    window[$chart] = new Chart(chartCanvas, {
                        type: "line",
                        data: {
                            labels: dataChart.labels,
                            datasets: dataChart.datasets
                        },
                        options: JS_CONFIG_CHART.chartLineOption
                    });

                    //2. Xử lý dữ liệu so sánh
                    JS_TRANSFORM_DATA_CHART.fillDataCompareChart($boxChart, res.data["dataCompare"]);
                }
            });
        });

    },

    filterChartLine() {
        let _this = this;

        $('.js-referer-report').click(function (e) {
            e.stopPropagation();
            let $boxChart = $(this).closest(".box-chart");
            $boxChart.find(".js-referer-report").removeClass("active");
            $(this).addClass("active");
            let referer = $(this).data("referer");
            let site_id = $boxChart.find(".js-dom-chart").data("id");
            let typeChart = $boxChart.find(".js-dom-chart").data("type");
            let dayReport = $('.js-day').val();
            let $chart = "chartLineType_" + typeChart

            $.ajax({
                type: "GET",
                url: URL_GET_DATA_TRAFFIC_LINE_CHART,
                data: {
                    referer: referer,
                    site_id: site_id,
                    day: dayReport,
                    type: typeChart
                }
            }).done(function (res) {
                if (res.code === 1) {
                    //1. Render chart
                    let dataChart = JS_TRANSFORM_DATA_CHART.renderDataLineChart(res.data);

                    window[$chart].data.labels = dataChart.labels;
                    window[$chart].data.datasets = dataChart.datasets;
                    window[$chart].update();

                    //2. Xử lý dữ liệu so sánh
                    JS_TRANSFORM_DATA_CHART.fillDataCompareChart($boxChart, res.data["dataCompare"]);
                }
            });

        });
    }
}

const JS_REPORT_BAR_CHART_REFERER = {
    init() {
        this.loadChart();
    },

    loadChart() {
        let $boxChart = $('.js-bar-chart-referer');
        let site_id = $boxChart.data("id");
        let dayReport = $('.js-day').val();
        let typeReport = $boxChart.data("type");

        $.ajax({
            url: URL_GET_DATA_BAR_CHART,
            type: "GET",
            data: {
                site_id: site_id,
                day: dayReport,
                type: typeReport
            }
        }).done(function (res) {
            if (res.code === 1) {
                // 1. Render chart
                let dataChart = JS_TRANSFORM_DATA_CHART.renderDataBarChart(res.data)
                let canvasChartBarTrafficReferer = $(`#chartBarTrafficReferer`).get(0).getContext("2d");
                console.log(dataChart)
                new Chart(canvasChartBarTrafficReferer, {
                    type: 'bar',
                    data: {
                        labels: dataChart.labels,
                        datasets: dataChart.datasets
                    },
                    options: JS_CONFIG_CHART.chartBarOption
                });

                //2. Xử lý dữ liệu so sánh
                JS_TRANSFORM_DATA_CHART.fillDataCompareChart($boxChart, res.data["dataCompare"]);
            }
        }).fail(function (err) {
            console.log(err)
        });
    }
}

$(function () {
    SITE_REPORT_JS_CORE.init();
    // JS_REPORT_TRAFFIC_PIE_CHART.init();
    JS_REPORT_TRAFFIC_LINE_CHART.init();
    JS_REPORT_BAR_CHART_REFERER.init();
});
