import SITE_REPORT_JS_CORE from "../core/index";
import {JS_CONFIG_CHART, JS_TRANSFORM_DATA_CHART} from "../core/config_chart";

const JS_REPORT_TRAFFIC_PIE_CHART = {
    init() {
        this.loadPieChart();
        this.filterPieChart();
    },

    loadPieChart() {
        let _this = this;

        $('.js-pie-chart').each( function () {
            let $boxChart = $(this).closest('.box-chart');
            let dayReport = $('.js-day').val();
            let typeChart = $(this).data("type");
            let nameChart = $(this).data("name");
            let $chart = "chartPie_" + nameChart;
            let dataSent = {
                site_id: SITE_ID,
                day: dayReport,
                type: typeChart
            };

            // Init chart
            if (!window[$chart]) {
                let canvasChart = $(this).find('.js-canvas-pie-chart').get(0).getContext("2d");
                let tooltipPie = JS_CONFIG_CHART.pluginChartPie;

                window[$chart] = new Chart(canvasChart, {
                    type: 'pie',
                    data: {},
                    options: JS_CONFIG_CHART.chartPieOption,
                    plugins: [tooltipPie]
                });
            }

            // Get data chart
            _this.__getDataPieChart($chart, $boxChart, dataSent);
        });
    },

    __getDataPieChart($chart, $boxChart, dataSent = {}) {
        $.ajax({
            url: URL_GET_DATA_PIE_CHART,
            type: "GET",
            data: dataSent
        })
            .done(function (res) {
                if (res.code === 1) {
                    // 1. Render chart
                    let dataChart = JS_TRANSFORM_DATA_CHART.renderDataPieChart(res.data);

                    window[$chart].data.labels = dataChart.labels;
                    window[$chart].data.datasets = dataChart.datasets;
                    window[$chart].update();

                    //2. Xử lý dữ liệu so sánh
                    JS_TRANSFORM_DATA_CHART.fillDataCompareChart($boxChart, res.data["dataCompare"]);
                }
            })
            .fail(function (err) {
                console.log(err)
            });
    },

    filterPieChart() {
        let _this = this;

        $('.js-type-report').click(function (e) {
            e.stopPropagation();
            $('.js-type-report').removeClass("active");
            $(this).addClass("active");

            let $boxChart = $(this).closest('.box-chart');
            let typeChart = $(this).data("type");
            let dayReport = $('.js-day').val();
            let nameChart = $boxChart.find('.js-pie-chart').data("name");
            let $chart = "chartPie_" + nameChart;
            let dataSent = {
                site_id: SITE_ID,
                day: dayReport,
                type: typeChart
            };

            _this.__getDataPieChart($chart, $boxChart, dataSent)
        });
    }
}

const JS_REPORT_TRAFFIC_LINE_CHART = {
    init() {
        this.loadChart();
    },

    loadChart() {
        let _this = this;
        $('.js-dom-chart').each(function () {
            let $boxChart = $(this).closest('.box-chart');
            let site_id = $(this).data("id");
            let typeChart = $(this).data("type");
            let dayReport = $('.js-day').val();
            let chartCanvas = $(this).find('.js-canvas').get(0).getContext("2d");

            $.ajax({
                type: "GET",
                url: URL_GET_DATA_TRAFFIC_LINE_CHART,
                data: {
                    site_id: site_id,
                    day: dayReport,
                    type: typeChart
                }
            }).done(function (res) {
                if (res.code === 1) {
                    //1. Render chart
                    let dataChart = JS_TRANSFORM_DATA_CHART.renderDataLineChart(res.data);

                    new Chart(chartCanvas, {
                        type: "line",
                        data: {
                            labels: dataChart.labels,
                            datasets: dataChart.datasets
                        },
                        options: JS_CONFIG_CHART.chartLineOption
                    });

                    //2. Xử lý dữ liệu so sánh
                    JS_TRANSFORM_DATA_CHART.fillDataCompareChart($boxChart, res.data["dataCompare"]);
                }
            });

        });
    }
}

$(function () {
    SITE_REPORT_JS_CORE.init();
    JS_REPORT_TRAFFIC_PIE_CHART.init();
    JS_REPORT_TRAFFIC_LINE_CHART.init();
});
