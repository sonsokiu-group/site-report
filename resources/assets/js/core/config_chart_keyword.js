const JS_CONFIG_CHART_KEYWORD = {
    background: ["#9cabff", "#f9db7b", "#ffa9ce", "#0f9618", "#ff9901", "#36c", "#dc3911"],

    chartBarOption: {
        legend: {
            display: false,
            position: "top",
            rtl: true,
            labels: {
                boxWidth: 12,
                padding: 20,
                fontColor: '#6783b8',
            }
        },
        title: {
            display: false,
            text: 'Tiêu đề biểu đồ ở đây'
        },
        interaction: {
            mode: 'index',
            intersect: false,
        },
        stacked: false,
        maintainAspectRatio: false,
        responsive: true,
        tooltips: {
            intersect: false,
            mode: "index",
            position: "nearest",
            callbacks: {},
            rtl: true,
            enabled: true,
            backgroundColor: '#1c2b46',
            titleFontSize: 13,
            titleFontColor: '#fff',
            titleMarginBottom: 6,
            bodyFontColor: '#fff',
            bodyFontSize: 12,
            bodySpacing: 4,
            yPadding: 10,
            xPadding: 10,
            footerMarginTop: 0,
            displayColors: true
        },
        scales: {
            yAxes: [{
                display: true,
                stacked: false,
                position: "left",
                ticks: {
                    beginAtZero: true,
                    fontSize: 11,
                    fontColor: '#9eaecf',
                    padding: 10,
                    callback: function (value, index, values) {
                        return value;
                    },
                    min: 0,
                    // stepSize: 2000
                },
                gridLines: {
                    color: "rgba(82,100,132,0.2)",
                    tickMarkLength: 0,
                    zeroLineColor: "rgba(82,100,132,0.2)"
                },

            }],
            xAxes: [{
                display: true,
                stacked: false,
                ticks: {
                    fontSize: 11,
                    fontColor: '#9eaecf',
                    source: 'auto',
                    padding: 10,
                    reverse: true,
                    autoSkip: true,
                    maxTicksLimit: 10,
                    maxRotation: 0,
                    minRotation: 0,
                },
                gridLines: {
                    color: "transparent",
                    tickMarkLength: 10,
                    zeroLineColor: 'transparent',
                },
            }]
        }
    },

    chartPieOption: {
        legend: {
            display: true,
            position: "bottom",
            rtl: true,
            labels: {
                boxWidth: 12,
                padding: 20,
                fontColor: '#6783b8',
            }
        },
        title: {
            display: false,
            text: 'Cấu hình tên của biểu đồ tại đây'
        },
        maintainAspectRatio: false,
        tooltips: {
            enabled: true,
            rtl: true,
            callbacks: {
                title: function (tooltipItem, data) {
                    return data['labels'][tooltipItem[0]['index']];
                },
                label: function (tooltipItem, data) {
                    return data.datasets[tooltipItem.datasetIndex]['data'][tooltipItem['index']] + " lượt";
                }

            },
            backgroundColor: '#1c2b46',
            titleFontSize: 13,
            titleFontColor: '#fff',
            titleMarginBottom: 6,
            bodyFontColor: '#fff',
            bodyFontSize: 12,
            bodySpacing: 4,
            yPadding: 10,
            xPadding: 10,
            footerMarginTop: 0,
            displayColors: false
        },
    },

    pluginChartColumnKw: {
        id: 'tooltipLine',
        afterDatasetsDraw: chart => {
            let ctx = chart.chart.ctx;
            chart.data.datasets.forEach((dataset, datasetIndex) => {
                let meta = chart.getDatasetMeta(datasetIndex);
                if (!meta.hidden) {
                    meta.data.forEach((bar, index) => {
                        let data = dataset.data[index];
                        if (data !== null) {
                            ctx.save();
                            ctx.fillStyle = '#000000'; // Màu nền của chữ
                            ctx.strokeStyle = '#ffffff'; // Màu border của chữ
                            ctx.lineWidth = 1; // Độ dày của border
                            ctx.font = '14px Arial'; // Font của chữ
                            ctx.fontWeight = 'bold'
                            // Tính toán vị trí của văn bản
                            let xPos = bar._model.x;
                            let yPos = bar._model.y + 20; // Khoảng cách giữa cột và văn bản
                            // Canh lề văn bản
                            ctx.textAlign = 'center';
                            // Vẽ văn bản
                            ctx.strokeText(data, xPos, yPos);
                            ctx.fillText(data, xPos, yPos);
                            ctx.restore();
                        }
                    });
                }
            });
        }
    },

    pluginChartPie: {
        id: 'tooltipPie',
        afterDatasetsDraw: chart => {
            let ctx = chart.chart.ctx;
            if( chart.config.data.datasets[0] !== undefined){
                let dataset = chart.config.data.datasets[0];
                let total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                    return previousValue + currentValue;
                });

                let meta = chart.getDatasetMeta(0);
                let text = [];
                let textX = [];
                let textY = [];
                let offset = -Math.PI / 2;
                meta.data.forEach((element, index) => {
                    let currentValue = dataset.data[index];
                    let percentage = Math.round(((currentValue / total) * 100));
                    if(percentage !== 0) {
                        let angle = (Math.PI * 2 * currentValue) / total;
                        let x = element._model.x + Math.cos(offset + angle / 2) * element._model.outerRadius * 0.5;
                        let y = element._model.y + Math.sin(offset + angle / 2) * element._model.outerRadius * 0.5;
                        text.push(percentage + '%');
                        textX.push(x);
                        textY.push(y);
                        offset += angle;
                    }
                });
                ctx.fillStyle = 'white';
                ctx.textBaseline = 'middle';
                ctx.textAlign = 'center';
                let fontStyle = 'Arial';
                let fontSize = 15;
                ctx.font = fontSize + 'px ' + fontStyle;
                ctx.fontWeight = 'bold';
                text.forEach((label, index) => {
                    ctx.fillText(label, textX[index], textY[index]);
                });
            }
        },
    },
}

export default JS_CONFIG_CHART_KEYWORD;