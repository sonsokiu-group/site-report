import {JS_CONFIG_CHART, JS_TRANSFORM_DATA_CHART} from "./config_chart";

const SITE_REPORT_JS_CORE = {
    init() {
        this.clickToTop();
        this.fixNavbar();
        this.fastChangeSite();
        this.changeDayReport();
        this.handleModalDetail();
    },

    clickToTop() {
        let $buttonScrollTop = $('.js-scroll-top');
        $(window).scroll(function () {
            if ($(window).scrollTop() > 300) {
                $buttonScrollTop.fadeIn();
            } else {
                $buttonScrollTop.fadeOut();
            }
        });

        $buttonScrollTop.on('click', function (e) {
            e.preventDefault();
            $('html, body').animate({scrollTop: 0}, '300');
        });
    },

    fixNavbar() {
        $(window).scroll(function () {
            if ($(window).scrollTop() > 300) {
                $('.js-header-fix').addClass("fixed")
            } else {
                $('.js-header-fix').removeClass("fixed")
            }
        });
    },

    fastChangeSite() {
        $('.js-select-change-site').on('change', function () {
            window.location.href = $(this).val();
        });

    },

    changeDayReport(){
        $('.js-change-day-report').on('change', function () {
            $('.js-form-filter-report').submit();
        });
    },

    handleModalDetail() {
        let _this = this;
        let $boxModalReportDetail = $("#modal-report-detail");
        let $boxChart = $boxModalReportDetail.find('.box-chart');
        let urlSent = $boxModalReportDetail.data("url");
        let modalType = $boxModalReportDetail.data("modal-type");

        // CLick show modal => get data chart
        $('.js-show-modal-detail').click(function () {
            let site_id = SITE_ID;
            let dayReport = $('.js-day').val();
            let redirect = $(this).data('type');
            let dataSent = {
                site_id: site_id,
                day: dayReport,
                redirect: redirect
            }

            $boxModalReportDetail.find('.js-name-chart').text(redirect);
            $boxModalReportDetail.find('.js-option-day-report').val(dayReport).data("type", redirect);

            // Init chart
            if (!window.chartReportDetail) {
                let tooltipLine = JS_CONFIG_CHART.pluginChartLine;
                let chartCanvas = $('#chart-report-detail').get(0).getContext("2d");

                window.chartReportDetail = new Chart(chartCanvas, {
                    type: "line",
                    data: {},
                    options: JS_CONFIG_CHART.chartLineOption,
                    plugins: [tooltipLine]
                });
            }

            __getDataReportDetail(urlSent, dataSent, modalType);
        });

        function __getDataReportDetail(urlSent, dataSent = {}, modalType) {
            $.ajax({
                type: "GET",
                url: urlSent,
                data: dataSent
            }).done(function (res) {
                if (res.code === 1) {
                    //1. Render chart
                    let dataChart;
                    if (modalType === "reportRedirect")
                    {
                        dataChart = __renderDataChartRedirect(res.data);
                    }
                    else
                    {
                        dataChart = JS_TRANSFORM_DATA_CHART.renderDataLineChart(res.data);
                    }

                    window.chartReportDetail.data.labels = dataChart.labels;
                    window.chartReportDetail.data.datasets = dataChart.datasets;
                    window.chartReportDetail.update();

                    //2. Xử lý dữ liệu so sánh
                    JS_TRANSFORM_DATA_CHART.fillDataCompareChart($boxChart, res.data["dataCompare"]);
                    $boxModalReportDetail.show();
                }
            }).fail(function (err) {
                console.log(err)
            });
        }

        function __renderDataChartRedirect(data) {
            let dataStatistics = {
                labels: data.dates,
                dataUnit: "lượt",
                lineTension: .4,
                datasets: [
                    {
                        yAxisID: 'yAxes1',
                        label: "Cũ",
                        backgroundColor: "rgba(151,187,205,0.2)",
                        borderColor: "rgba(151,187,205,1)",
                        pointBackgroundColor: "#ccc",
                        pointBorderColor: "rgba(151,187,205,1)",
                        pointHoverBackgroundColor: "#ccc",
                        pointHoverBorderColor: "rgba(151,187,205,1)",
                        data: data.dataChartBefore,
                        borderDash: [5, 5], // Add borderDash to make it dashed

                    },
                    {
                        yAxisID: 'yAxes1',
                        label: "Hiện tại",
                        backgroundColor: "rgba(204,204,204,0.2)",
                        borderColor: "rgba(0,146,236,0.83)",
                        pointBackgroundColor: "rgba(0,146,236,1)",
                        pointBorderColor: "rgba(0,146,236,1)",
                        pointHoverBackgroundColor: "#fff",
                        data: data.dataChart,
                        fill: 'origin',
                    }
                ]
            };
            let chart_data = [];

            for (let i = 0; i < dataStatistics.datasets.length; i++) {
                chart_data.push({
                    label: dataStatistics.datasets[i].label,
                    lineTension: .15,
                    backgroundColor: dataStatistics.datasets[i].backgroundColor,
                    borderWidth: 1.8,
                    borderColor: dataStatistics.datasets[i].borderColor,
                    pointBorderColor: dataStatistics.datasets[i].pointBorderColor,
                    pointBackgroundColor: dataStatistics.datasets[i].pointBackgroundColor,
                    pointHoverBackgroundColor: dataStatistics.datasets[i].pointHoverBackgroundColor,
                    pointHoverBorderColor: dataStatistics.datasets[i].pointHoverBorderColor,
                    // pointBorderWidth: 1.5,
                    // pointHoverRadius: 3,
                    // pointHoverBorderWidth: 1,
                    // dash: 0,
                    borderDash: dataStatistics.datasets[i].borderDash ?? [],
                    fill: dataStatistics.datasets[i].fill ?? false,
                    // pointRadius: 3,
                    // pointHitRadius: 3,
                    data: dataStatistics.datasets[i].data,
                });
            }

            return {
                labels: dataStatistics.labels,
                datasets: chart_data,
            }
        }

        // Filter data modal report detail
        $boxModalReportDetail.on('change', '.js-option-day-report', function(){
            let site_id = SITE_ID;
            let dayReport = $(this).val();
            let redirect = $(this).data('type');

            let dataSent = {
                site_id: site_id,
                day: dayReport,
                redirect: redirect
            }

           __getDataReportDetail(urlSent, dataSent, modalType);
        });

        // Close modal
        $('.btn-close-modal').click(function () {
            $boxModalReportDetail.hide();
        });

        $(window).click(function (event) {
            if (event.target.id === "modal-report-detail") {
                $boxModalReportDetail.hide();
            }
        });
    }

};


export default SITE_REPORT_JS_CORE;
