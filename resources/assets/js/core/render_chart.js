import SITE_REPORT_JS_CORE from "../core/index";
import {JS_CONFIG_CHART, JS_TRANSFORM_DATA_CHART} from "../core/config_chart";

const JS_TRAFFIC_DETAIL_OVERVIEW = {
    init() {
        // this.loadLineChartGroup();
        this.render();
    },

    render() {
        $('.render_chart').each(function () {
            let domChart = $(this);
            let day = $('.js-day').val() ?? 30;
            let siteId  = $('#js-site-id').val();
            let dataChart = $(this).data('chart');
            let chartConfig = dataChart.chartConfig;
            let nameService = dataChart.service;
            $.ajax({
                type: 'get',
                url: URL_RENDER_CHART,
                data: {
                    nameService: nameService,
                    day: day,
                    site_id: siteId,
                    query: dataChart.query
                }
            }).done(function (response) {
                if (response.code === 1) {
                    let numberChart = chartConfig.length;
                    if (numberChart > 1) {
                        let $i = 0;
                        $.each(chartConfig, function (key, item) {

                            let id = item.id;
                            let name = item.name;
                            let data = response.data.dataChart[id];
                            if (!data) {
                                return;
                            }
                            let htmlOption = '';

                            if (item.option) {
                                let dataCompare = response.data.dataCompare[id];
                                let totalCurrent = dataCompare["totalCurrent"] ?? 0;
                                let totalBefore = dataCompare["totalBefore"] ?? 0;
                                let growthRate = Number(dataCompare["growthRate"]) ?? 0;
                                let arrowIconClass = (growthRate > 0) ? "fa fa-long-arrow-up" : "fa fa-long-arrow-down";
                                let colorTextClass = (growthRate > 0) ? "text-success" : "text-danger";
                                let html = `<span class="${colorTextClass}"><i class="${arrowIconClass}" style="margin-right: 6px"></i>${growthRate + '%'}</span>`;
                                if( item.optionType == 1){
                                    let totalAll = dataCompare["totalAll"];
                                    htmlOption = `
                                                <div class="row" style="padding: 0 15px;margin-bottom:20px">
                                                    <div class="col-md-4">
                                                        Lũy kế: <b class="js-total-before">${totalAll}</b>
                                                    </div>
                                                    <div class="col-md-4">
                                                        Hiện tại: <b class="js-total-current">${totalCurrent}</b>
                                                    </div>
                                                </div>
                                `;
                                }else {
                                    htmlOption = `
                                                <div class="row" style="padding: 0 15px;margin-bottom:20px">
                                                    <div class="col-md-4">
                                                        Hiện tại: <b class="js-total-current">${totalCurrent}</b>
                                                    </div>
                                                     <div class="col-md-4">
                                                        Kì trước: <b class="js-total-before">${totalBefore}</b>
                                                    </div>
                                                    <div class="col-md-4">
                                                        Tăng trưởng : <b class="js-growthRate">${html}</b>
                                                    </div>
                                                </div>
                                `;
                                }
                            }
                            domChart.append(`
                                    <div class="col ${item.col}">
                                            <div class="box-chart">
                                                <div class="chart-header">
                                                    <div class="box-title">
                                                        <a target="_blank" href="${item.url}">
                                                            <b>${item.name}</b>
                                                        </a>
                                                    </div>

                                                </div>
                                                ${htmlOption}
                                                <div class="chart-main js-dom-chart" style="height: 280px">
                                                    <canvas class="chart-site-${id} js-canvas"></canvas>
                                                </div>
                                            </div>
                                    </div>
                                `);

                            let dataChart;
                            if (!isObject(data)) {
                                dataChart = {
                                    'dates': response.data.dates,
                                    'dataChart': {[name]: data}
                                }
                            } else {
                                dataChart = {
                                    'dates': response.data.dates,
                                    'dataChart': data
                                }
                            }

                            if (item.type == 'line') {
                                let chartCanvas = domChart.find(".js-canvas").get($i).getContext("2d");
                                $i++;
                                renderChartLine(chartCanvas, dataChart, $i);
                            } else if (item.type == 'bar') {

                            }


                        })
                    } else {
                        let dataCompare = response.data.dataCompare;
                        let totalCurrent = dataCompare["totalCurrent"];
                        let totalBefore = dataCompare["totalBefore"];
                        let growthRate = Number(dataCompare["growthRate"]);
                        let arrowIconClass = (growthRate > 0) ? "fa fa-long-arrow-up" : "fa fa-long-arrow-down";
                        let colorTextClass = (growthRate > 0) ? "text-success" : "text-danger";
                        let html = `<span class="${colorTextClass}"><i class="${arrowIconClass}" style="margin-right: 6px"></i>${growthRate + '%'}</span>`;
                        let htmlOption = '';
                        let item = chartConfig[0];
                        let data = response.data.dataChart;
                        if (item.option) {
                            if( item.optionType == 1){
                                let totalAll = dataCompare["totalAll"];
                                htmlOption = `
                                                <div class="row" style="padding: 0 15px;margin-bottom:20px">
                                                    <div class="col-md-4">
                                                        Lũy kế: <b class="js-total-before">${totalAll}</b>
                                                    </div>
                                                    <div class="col-md-4">
                                                        Hiện tại: <b class="js-total-current">${totalCurrent}</b>
                                                    </div>
                                                </div>
                                `;
                            }else{
                                htmlOption = `
                                                <div class="row" style="padding: 0 15px;margin-bottom:20px">
                                                    <div class="col-md-4">
                                                        Hiện tại: <b class="js-total-current">${totalCurrent}</b>
                                                    </div>
                                                     <div class="col-md-4">
                                                        Kì trước: <b class="js-total-before">${totalBefore}</b>
                                                    </div>
                                                    <div class="col-md-4">
                                                        Tăng trưởng : <b class="js-growthRate">${html}</b>
                                                    </div>
                                                </div>
                                `;
                            }

                        }else{
                            htmlOption = `
                                                <div class="row" style="padding: 0 15px;margin-bottom:20px">

                                                </div>
                                `;
                        }
                        domChart.append(`
                                    <div class="col ${item.col}">
                                            <div class="box-chart">
                                                <div class="chart-header">
                                                    <div class="box-title">
                                                        <a target="_blank" href="${item.url}">
                                                            <b>${item.name}</b>
                                                        </a>
                                                    </div>

                                                </div>
                                                ${htmlOption}
                                                <div class="chart-main js-dom-chart" style="height: 400px">
                                                    <canvas class="chart-site js-canvas" style="height:100%"></canvas>
                                                </div>
                                            </div>
                                    </div>
                                `);

                        let dataChart;
                        if (!isObject(data)) {
                            dataChart = {
                                'dates': response.data.dates,
                                'dataChart': {[name]: data}
                            }
                        } else {
                            dataChart = {
                                'dates': response.data.dates,
                                'dataChart': data
                            }
                        }

                        let chartCanvas = domChart.find(".js-canvas").get(0).getContext("2d");
                        if (item.type == 'line') {
                            renderChartLineV2(chartCanvas, dataChart);
                        } else if (item.type == 'bar') {
                            renderChartBarV2(chartCanvas, response.data);

                        }
                    }
                } else {

                }
            })

        })

        function renderChartLineV2(chartCanvas, dataChart) {
            dataChart = JS_TRANSFORM_DATA_CHART.renderDataLineChartV2(dataChart);
            new Chart(chartCanvas, {
                type: "line",
                data: {
                    labels: dataChart.labels,
                    datasets: dataChart.datasets
                },
                options: JS_CONFIG_CHART.chartLineOption,
                plugins: [JS_CONFIG_CHART.pluginChartLine]
            });
        }

        function renderChartBarV2(chartCanvas, dataChart) {
            dataChart = JS_TRANSFORM_DATA_CHART.renderDataBarChart(dataChart);
            window.chartBar =  new Chart(chartCanvas, {
                type: "bar",
                data: {

                },
                options: JS_CONFIG_CHART.chartBarHomeOption,
                plugins: [
                    JS_CONFIG_CHART.pluginChartColumnAllSite
                ]
            });

            setTimeout(() => {
                window.chartBar.data.labels = dataChart.labels
                window.chartBar.data.datasets = dataChart.datasets
                window.chartBar.update()
            },200);
        }


        function renderChartLine(chartCanvas, dataChart) {
            dataChart = JS_TRANSFORM_DATA_CHART.renderDataLineChart(dataChart);
            new Chart(chartCanvas, {
                type: "line",
                data: {
                    labels: dataChart.labels,
                    datasets: dataChart.datasets
                },
                options: JS_CONFIG_CHART.chartLineOption,
                plugins: [JS_CONFIG_CHART.pluginChartLine]
            });
        }

        function isObject(item) {
            return (typeof item === 'object' && !Array.isArray(item) && item !== null);
        }
    },

    loadLineChartGroup() {
        // $('.js-dom-chart').each(function () {
        //     let domChart = $(this);
        //     let url = domChart.data("url");
        //     let method = domChart.data("method");
        //     let type = domChart.data("type");
        //     let process = domChart.data("process");
        //     let dayReport = $('.js-day').val();
        //     let modeId  = domChart.data("modeId")
        //
        //
        //     $.ajax({
        //         type: method,
        //         url: URL_RENDER_CHART,
        //         data: {
        //             process: process,
        //             day: dayReport,
        //             type: type
        //         }
        //     }).done(function (res) {
        //         if (res.code === 1) {
        //             let chartCanvas = domChart.find(".js-canvas").get(0).getContext("2d");
        //             if(type == "line"){
        //                 let dataChart   = JS_TRANSFORM_DATA_CHART.renderDataLineChart(res.data);
        //                 new Chart(chartCanvas, {
        //                     type: "line",
        //                     data: {
        //                         labels: dataChart.labels,
        //                         datasets: dataChart.datasets
        //                     },
        //                     options: JS_CONFIG_CHART.chartLineOption
        //                 });
        //             }
        //         }
        //     });
        // });
    },


};

$(function () {
    SITE_REPORT_JS_CORE.init();
    JS_TRAFFIC_DETAIL_OVERVIEW.init();
});
