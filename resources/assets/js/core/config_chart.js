const Line2ChartConfig = {
    backgroundColor: ["rgba(151,187,205,0.2)", "rgba(204,204,204,0.2)", "transparent", "transparent", "transparent"],
    borderColor: ["rgba(151,187,205,1)", "rgba(0,146,236,0.83)", "#b29a3f", "#d37070", "#72a94c"],
    pointBackgroundColor: ["#ccc", "rgba(0,146,236,1)", "#b29a3f", "#d37070", "#72a94c"],
    pointBorderColor: ["rgba(151,187,205,1)", "rgba(0,146,236,1)", "#b29a3f", "#d37070", "#72a94c"],
    pointHoverBackgroundColor: ["#ccc", "#fff", "#b29a3f", "#d37070", "#72a94c"],
    pointHoverBorderColor: ["rgba(151,187,205,1)", "rgba(0,146,236,1)", "#b29a3f", "#d37070", "#72a94c"],
    borderDash: [[5, 5], [], [], [], []],
    fill: [false, 'origin', false, false, false]
};

const LabelMapChartConfig = {
    'count_jd': 'Tổng JD',
    'count_jd_done': 'Bóc được',
    'state': 'State',
    'city': "City",
    'county': "County"
}

const JS_CONFIG_CHART = {
    background: ["#9cabff", "#f9db7b", "#ffa9ce", "#0f9618", "#ff9901", "#36c", "#dc3911"],

    chartPieOption: {
        legend: {
            display: true,
            position: "bottom",
            rtl: true,
            labels: {
                boxWidth: 12,
                padding: 20,
                fontColor: '#6783b8',
            }
        },
        title: {
            display: false,
            text: 'Cấu hình tên của biểu đồ tại đây'
        },
        maintainAspectRatio: false,
        tooltips: {
            enabled: true,
            rtl: true,
            callbacks: {
                title: function (tooltipItem, data) {
                    return data['labels'][tooltipItem[0]['index']];
                },
                label: function (tooltipItem, data) {
                    return data.datasets[tooltipItem.datasetIndex]['data'][tooltipItem['index']] + " lượt";
                }

            },
            backgroundColor: '#1c2b46',
            titleFontSize: 13,
            titleFontColor: '#fff',
            titleMarginBottom: 6,
            bodyFontColor: '#fff',
            bodyFontSize: 12,
            bodySpacing: 4,
            yPadding: 10,
            xPadding: 10,
            footerMarginTop: 0,
            displayColors: false
        },
    },

    chartLineOption: {
        legend: {
            display: true,
            position: "top",
            rtl: true,
            labels: {
                boxWidth: 12,
                padding: 20,
                fontColor: '#6783b8',
            }
        },
        title: {
            display: false,
            text: 'Tiêu đề biểu đồ ở đây'
        },
        interaction: {
            mode: 'index',
            intersect: false,
        },
        stacked: false,
        maintainAspectRatio: false,
        tooltips: {
            intersect: false,
            mode: "index",
            position: "nearest",
            callbacks: {},
            rtl: true,
            enabled: true,
            backgroundColor: '#1c2b46',
            titleFontSize: 13,
            titleFontColor: '#fff',
            titleMarginBottom: 6,
            bodyFontColor: '#fff',
            bodyFontSize: 12,
            bodySpacing: 4,
            yPadding: 10,
            xPadding: 10,
            footerMarginTop: 0,
            displayColors: true
        },
        responsive: true,
        scales: {
            yAxes: [{
                display: true,
                stacked: false,
                position: "left",
                ticks: {
                    beginAtZero: true,
                    fontSize: 11,
                    fontColor: '#9eaecf',
                    padding: 10,
                    callback: function (value, index, values) {
                        return value;
                    },
                    min: 0,
                    // stepSize: 2000
                },
                gridLines: {
                    color: "rgba(82,100,132,0.2)",
                    tickMarkLength: 0,
                    zeroLineColor: "rgba(82,100,132,0.2)"
                },

            }],
            xAxes: [{
                display: true,
                stacked: false,
                ticks: {
                    fontSize: 11,
                    fontColor: '#9eaecf',
                    source: 'auto',
                    padding: 10,
                    reverse: true,
                    autoSkip: true,
                    maxTicksLimit: 10,
                    maxRotation: 0,
                    minRotation: 0,
                },
                gridLines: {
                    color: "transparent",
                    tickMarkLength: 10,
                    zeroLineColor: 'transparent',
                },
            }]
        }

    },

    chartBarOption: {
        legend: {
            display: true,
            position: "top",
            rtl: true,
            labels: {
                boxWidth: 12,
                padding: 20,
                fontColor: '#6783b8',
            }
        },
        title: {
            display: false,
            text: 'Tiêu đề biểu đồ ở đây'
        },
        interaction: {
            mode: 'index',
            intersect: false,
        },
        stacked: false,
        maintainAspectRatio: false,
        responsive: true,
        tooltips: {
            intersect: false,
            mode: "index",
            position: "nearest",
            callbacks: {
                label: function (tooltipItem, data) {
                    return data.labels[tooltipItem.index] + ': ' + tooltipItem.yLabel;
                }
            },
            rtl: true,
            enabled: true,
            backgroundColor: '#1c2b46',
            titleFontSize: 13,
            titleFontColor: '#fff',
            titleMarginBottom: 6,
            bodyFontColor: '#fff',
            bodyFontSize: 12,
            bodySpacing: 4,
            yPadding: 10,
            xPadding: 10,
            footerMarginTop: 0,
            displayColors: true
        },
        scales: {
            yAxes: [{
                display: true,
                stacked: false,
                position: "left",
                ticks: {
                    beginAtZero: true,
                    fontSize: 11,
                    fontColor: '#9eaecf',
                    padding: 10,
                    callback: function (value, index, values) {
                        return value;
                    },
                    min: 0,
                    // stepSize: 2000
                },
                gridLines: {
                    color: "rgba(82,100,132,0.2)",
                    tickMarkLength: 0,
                    zeroLineColor: "rgba(82,100,132,0.2)"
                },

            }],
            xAxes: [{
                display: true,
                stacked: false,
                ticks: {
                    fontSize: 11,
                    fontColor: '#9eaecf',
                    source: 'auto',
                    padding: 10,
                    reverse: true,
                    autoSkip: true,
                    maxTicksLimit: 10,
                    maxRotation: 0,
                    minRotation: 0,
                },
                gridLines: {
                    color: "transparent",
                    tickMarkLength: 10,
                    zeroLineColor: 'transparent',
                },
            }]
        }
    },

    chartLineHomeOption: {
        legend: {
            display: false,
            position: "top",
            rtl: true,
            labels: {
                boxWidth: 12,
                padding: 20,
                fontColor: '#6783b8',
            }
        },
        title: {
            display: false,
            text: 'Biểu đồ tổng quan traffic'
        },
        interaction: {
            mode: 'index',
            intersect: false,
        },
        stacked: false,
        maintainAspectRatio: false,
        tooltips: {
            intersect: false,
            mode: "index",
            position: "nearest",
            callbacks: {},
            rtl: true,
            enabled: true,
            backgroundColor: '#1c2b46',
            titleFontSize: 13,
            titleFontColor: '#fff',
            titleMarginBottom: 6,
            bodyFontColor: '#fff',
            bodyFontSize: 12,
            bodySpacing: 4,
            yPadding: 10,
            xPadding: 10,
            footerMarginTop: 0,
            displayColors: true
        },
        responsive: true,
        scales: {
            yAxes: [{
                display: true,
                stacked: false,
                position: "left",
                ticks: {
                    beginAtZero: true,
                    fontSize: 11,
                    fontColor: '#9eaecf',
                    padding: 10,
                    callback: function (value, index, values) {
                        return value;
                    },
                    min: 0,
                    // stepSize: 5000
                },
                gridLines: {
                    color: "rgba(82,100,132,0.2)",
                    tickMarkLength: 0,
                    zeroLineColor: "rgba(82,100,132,0.2)"
                },

            }],
            xAxes: [{
                display: true,
                stacked: false,
                ticks: {
                    fontSize: 11,
                    fontColor: '#9eaecf',
                    source: 'auto',
                    padding: 10,
                    reverse: true,
                    autoSkip: true,
                    maxTicksLimit: 10,
                    maxRotation: 0,
                    minRotation: 0,
                },
                gridLines: {
                    color: "transparent",
                    tickMarkLength: 10,
                    zeroLineColor: 'transparent',
                },
            }]
        }
    },

    chartBarHomeOption: {
        legend: {
            display: true, position: "top", rtl: true, labels: {
                boxWidth: 12, padding: 20, fontColor: '#6783b8',
            }
        },
        responsive: true,
        interaction: {
            mode: 'index',
            intersect: false,
        },
        // stacked: true,
        maintainAspectRatio: false,
        tooltips: {
            intersect: true,
            mode: "index",
            position: "nearest",
            callbacks: {
                label: function (tooltipItem, data) {
                    let datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
                    let value = tooltipItem.yLabel;
                    if (value < 0) {
                        return datasetLabel + ': ' + Math.abs(value);
                    } else {
                        return datasetLabel + ': ' + value;
                    }
                },
            },
            rtl: false,
            enabled: true,
            backgroundColor: '#1c2b46',
            titleFontSize: 13,
            titleFontColor: '#fff',
            titleMarginBottom: 6,
            bodyFontColor: '#fff',
            bodyFontSize: 12,
            bodySpacing: 4,
            yPadding: 10,
            xPadding: 10,
            footerMarginTop: 0,
            displayColors: true
        },

        scales: {
            yAxes: [
                {
                    id: 'yAxes1',
                    type: 'linear',
                    position: 'left',
                    stacked: true,
                    ticks: {
                        callback: function (value, index, values) {
                            return Math.abs(value); // Ví dụ: Thêm 5 đơn vị vào giá trị trên trục y
                        },
                        beginAtZero: true,
                        fontSize: 11,
                        fontColor: '#9eaecf',
                        source: 'auto',
                        padding: 10,
                        // autoSkip: true,

                        maxRotation: 0,
                        minRotation: 0,
                        step: 100,
                        // min: 0,
                    },
                    gridLines: {
                        tickMarkLength: 5, zeroLineColor: 'black',
                    },

                },

            ],
            xAxes: [{
                display: true,
                stacked: true,
                ticks: {
                    fontSize: 10,
                    fontColor: '#638ee8',
                    source: 'auto',
                    // padding: 10,
                    reverse: true,
                    autoSkip: true,
                    // maxTicksLimit: 12,
                    maxRotation: 0,
                    minRotation: 0,
                }, gridLines: {
                    color: "transparent", tickMarkLength: 10, zeroLineColor: 'transparent',
                },
            }]
        }
    },

    pluginChartLine: {
        id: 'tooltipLine',
        beforeDraw: chart => {
            if (chart.tooltip._active && chart.tooltip._active.length) {
                let ctx = chart.chart.ctx;
                ctx.save()
                let activePoint = chart.tooltip._active[0];
                ctx.beginPath();
                ctx.setLineDash([5, 7]);
                ctx.moveTo(activePoint._model.x, chart.chartArea.top + 20);
                ctx.lineTo(activePoint._model.x, activePoint.tooltipPosition().y);
                ctx.lineWidth = 1;
                ctx.strokeStyle = 'grey';
                ctx.stroke();
                ctx.restore()

                ctx.beginPath();
                ctx.moveTo(activePoint._model.x, activePoint._model.y);
                ctx.lineTo(activePoint._model.x, chart.chartArea.bottom);
                ctx.lineWidth = 1;
                ctx.strokeStyle = 'grey';
                ctx.stroke();
                ctx.restore()
            }
        }
    },

    pluginChartPie: {
        id: 'tooltipPie',
        afterDatasetsDraw: chart => {
            let ctx = chart.chart.ctx;
            if (chart.config.data.datasets[0] !== undefined) {
                let dataset = chart.config.data.datasets[0];
                let total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                    return previousValue + currentValue;
                });

                let meta = chart.getDatasetMeta(0);
                let text = [];
                let textX = [];
                let textY = [];
                let offset = -Math.PI / 2;
                meta.data.forEach((element, index) => {
                    let currentValue = dataset.data[index];
                    let percentage = Math.round(((currentValue / total) * 100));
                    if (percentage !== 0) {
                        let angle = (Math.PI * 2 * currentValue) / total;
                        let x = element._model.x + Math.cos(offset + angle / 2) * element._model.outerRadius * 0.5;
                        let y = element._model.y + Math.sin(offset + angle / 2) * element._model.outerRadius * 0.5;
                        text.push(percentage + '%');
                        textX.push(x);
                        textY.push(y);
                        offset += angle;
                    }
                });
                ctx.fillStyle = 'white';
                ctx.textBaseline = 'middle';
                ctx.textAlign = 'center';
                let fontStyle = 'Arial';
                let fontSize = 15;
                ctx.font = fontSize + 'px ' + fontStyle;
                ctx.fontWeight = 'bold';
                text.forEach((label, index) => {
                    ctx.fillText(label, textX[index], textY[index]);
                });
            }
        },
    },

    pluginChartColumnAllSite: {
        id: 'tooltipLine',
        beforeDraw: chart => {
            if (chart.tooltip._active && chart.tooltip._active.length) {
                let ctx = chart.chart.ctx;
                ctx.save()
                let activePoint = chart.tooltip._active[0];
                ctx.beginPath();
                ctx.setLineDash([5, 7]);
                ctx.moveTo(activePoint._model.x, chart.chartArea.top + 20);
                ctx.lineTo(activePoint._model.x, activePoint.tooltipPosition().y);
                ctx.lineWidth = 1;
                ctx.strokeStyle = 'grey';
                ctx.stroke();
                ctx.restore()

                ctx.beginPath();
                ctx.moveTo(activePoint._model.x, activePoint._model.y);
                ctx.lineTo(activePoint._model.x, chart.chartArea.bottom);
                ctx.lineWidth = 1;
                ctx.strokeStyle = 'grey';
                ctx.stroke();
                ctx.restore()
            }
        },
        afterDatasetsDraw: function (chart) {
            let ctx = chart.chart.ctx;

            // Assuming there are at least two datasets
            let firstDataset = chart.data.datasets[0];
            let secondDataset = chart.data.datasets[1];
            if (firstDataset && secondDataset) {
                let firstMeta = chart.getDatasetMeta(0);
                let secondMeta = chart.getDatasetMeta(1);

                if (!firstMeta.hidden && !secondMeta.hidden) {
                    secondMeta.data.forEach(function (element, index) {
                        let valueDataset1 = firstDataset.data[index];
                        let valueDataset2 = secondDataset.data[index];

                        if (valueDataset1 === 0 && valueDataset2 === 0) {
                            return;
                        }
                        if (valueDataset1 === 0 && valueDataset2 !== 0) {
                            if (ctx) {
                                ctx.fillStyle = '#2d79e8';
                                ctx.font = '13px';
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'bottom';
                                ctx.fillText('100%', element._model.x, element._model.y + 20);
                            }
                            return;
                        }


                        let percentage = ((Math.abs(valueDataset2) - Math.abs(valueDataset1)) / Math.abs(valueDataset1) * 100).toFixed(2);
                        if (percentage > 0) {
                            if (ctx) {
                                ctx.fillStyle = '#2d79e8';
                                ctx.font = '13px';
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'bottom';
                                ctx.fillText(percentage + '%', element._model.x, element._model.y + 20);
                            }
                        }
                    });
                    firstMeta.data.forEach(function (element, index) {
                        let valueDataset1 = firstDataset.data[index];
                        let valueDataset2 = secondDataset.data[index];

                        if (valueDataset1 === 0 && valueDataset2 === 0) {
                            return;
                        }
                        if (valueDataset1 !== 0 && valueDataset2 === 0) {
                            if (ctx) {
                                ctx.fillStyle = '#f32f4c';
                                ctx.font = '13px';
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'top';
                                ctx.fillText('-100%', element._model.x, element._model.y - 20);
                            }
                            return;
                        }
                        let percentage = ((Math.abs(valueDataset2) - Math.abs(valueDataset1)) / Math.abs(valueDataset1) * 100).toFixed(2);

                        if (percentage < 0) {
                            if (ctx) {
                                ctx.fillStyle = "#f32f4c";
                                ctx.font = '13px';
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'top';
                                ctx.fillText(percentage + '%', element._model.x, element._model.y - 20);
                            }
                        }
                    });
                }
            }
        }
    },

}

const JS_TRANSFORM_DATA_CHART = {
    renderDataLineChart(data) {
        let arrayDataChart = data["dataChart"];
        let labels = data["dates"];
        let datasets = [];
        let indexBackground = 0;

        $.each(arrayDataChart, function (keyReport, arrayValue) {
            datasets.push({
                label: keyReport,
                color: JS_CONFIG_CHART.background[indexBackground],
                dash: 0,
                background: 'transparent',
                data: arrayValue
            });
            ++indexBackground;
        });

        let dataStatistics = {
            labels: labels,
            dataUnit: 'lượt',
            lineTension: 0.2,
            datasets: datasets
        };
        let chart_data = [];

        for (let i = 0; i < dataStatistics.datasets.length; i++) {
            chart_data.push({
                label: dataStatistics.datasets[i].label,
                tension: dataStatistics.lineTension,
                backgroundColor: dataStatistics.datasets[i].background,
                borderWidth: 2,
                borderColor: dataStatistics.datasets[i].color,
                // pointBorderColor: 'transparent',
                // pointBackgroundColor: 'transparent',
                pointHoverBackgroundColor: "#333",
                pointHoverBorderColor: dataStatistics.datasets[i].color,
                // pointBorderWidth: 2,
                // pointHoverRadius: 4,
                // pointHoverBorderWidth: 2,
                // pointRadius: 4,
                // pointHitRadius: 4,
                data: dataStatistics.datasets[i].data,
            });
        }

        return {
            labels: dataStatistics.labels,
            datasets: chart_data,
        }
    },

    renderDataLineChartV2(data) {
        let arrayDataChart = data["dataChart"];
        let labels = data["dates"];
        let datasets = [];
        let indexBackground = 0;
        $.each(arrayDataChart, function (keyReport, arrayValue) {
            datasets.push({
                yAxisID: 'yAxes1',
                label: LabelMapChartConfig[keyReport] ?? keyReport,
                lineTension: .15,
                borderWidth: 1.8,
                // background: 'transparent',
                data: arrayValue,
                // dash: Line2ChartConfig.borderDash[indexBackground],
                backgroundColor: Line2ChartConfig.backgroundColor[indexBackground],
                borderColor: Line2ChartConfig.borderColor[indexBackground],
                pointBackgroundColor: Line2ChartConfig.pointBackgroundColor[indexBackground],
                pointBorderColor: Line2ChartConfig.pointBorderColor[indexBackground],
                pointHoverBackgroundColor: Line2ChartConfig.pointHoverBackgroundColor[indexBackground],
                pointHoverBorderColor: Line2ChartConfig.pointHoverBorderColor[indexBackground],
                borderDash: Line2ChartConfig.borderDash[indexBackground],
                fill: Line2ChartConfig.fill[indexBackground],
            });
            ++indexBackground;
        });

        let dataStatistics = {
            labels: labels,
            dataUnit: 'lượt',
            lineTension: .4,
            datasets: datasets
        };
        let chart_data = [];

        for (let i = 0; i < dataStatistics.datasets.length; i++) {
            chart_data.push({
                label: dataStatistics.datasets[i].label,
                lineTension: .15,
                backgroundColor: dataStatistics.datasets[i].backgroundColor,
                borderWidth: 1.8,
                borderColor: dataStatistics.datasets[i].borderColor,
                pointBorderColor: dataStatistics.datasets[i].pointBorderColor,
                pointBackgroundColor: dataStatistics.datasets[i].pointBackgroundColor,
                pointHoverBackgroundColor: dataStatistics.datasets[i].pointHoverBackgroundColor,
                pointHoverBorderColor: dataStatistics.datasets[i].pointHoverBorderColor,

                borderDash: dataStatistics.datasets[i].borderDash ?? [],
                fill: dataStatistics.datasets[i].fill ?? false,

                data: dataStatistics.datasets[i].data,
            });
        }

        return {
            labels: dataStatistics.labels,
            datasets: chart_data,
        }
    },


    renderDataPieChart(data) {
        let arrayDataChart = data["dataChart"];
        let labels = [];
        let chartData = [];
        let background = [];
        let indexBackground = 0;

        $.each(arrayDataChart, function (key, value) {
            labels.push(key);
            chartData.push(value);
            background.push(JS_CONFIG_CHART.background[indexBackground]);
            ++indexBackground;
        });

        let dataStatistics = {
            labels: labels,
            dataUnit: 'lượt',
            legend: false,
            datasets: [{
                borderColor: "#fff",
                background: background,
                data: chartData
            }]
        };

        let chart_data = [];

        for (let i = 0; i < dataStatistics.datasets.length; i++) {
            chart_data.push({
                backgroundColor: dataStatistics.datasets[i].background,
                borderWidth: 2,
                borderColor: dataStatistics.datasets[i].borderColor,
                hoverBorderColor: dataStatistics.datasets[i].borderColor,
                data: dataStatistics.datasets[i].data,
            });
        }

        return {
            labels: dataStatistics.labels,
            datasets: chart_data,
        }
    },

    renderDataBarChart(data) {
        let dataChart = data["dataChart"];
        let dataChartBefore = data["dataChartBefore"];
        let labels = [];
        let dataCurrentPeriod = []
        let dataPreviousPeriod = []

        $.each(dataChart, function (keyReport, value) {
            labels.push(keyReport)
            dataCurrentPeriod.push(value)
        });

        $.each(dataChartBefore, function (keyReport, value) {
            dataPreviousPeriod.push(value)
        });

        let datasets = [
            {
                label: "Kì này",
                backgroundColor: "#9cabff",
                data: dataCurrentPeriod
            },
            {
                label: "Kì trước",
                backgroundColor: "#f9db7b",
                data: dataPreviousPeriod
            }
        ];

        return {
            labels: labels,
            datasets: datasets,
        }
    },

    fillDataCompareChart($boxChart, dataCompare) {
        let totalCurrent = dataCompare["totalCurrent"];
        let totalBefore = dataCompare["totalBefore"];
        let growthRate = Number(dataCompare["growthRate"]);
        let arrowIconClass = (growthRate > 0) ? "fa fa-long-arrow-up" : "fa fa-long-arrow-down";
        let colorTextClass = (growthRate > 0) ? "text-success" : "text-danger";
        let html = `<span class="${colorTextClass}"><i class="${arrowIconClass}" style="margin-right: 6px"></i>${growthRate + '%'}</span>`;

        $boxChart.find('.js-total-current').text(totalCurrent);
        $boxChart.find('.js-total-before').text(totalBefore);
        $boxChart.find('.js-growth-rate').html(html);
    }
}

export {JS_CONFIG_CHART, JS_TRANSFORM_DATA_CHART}
