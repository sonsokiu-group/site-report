<?php

namespace Workable\SiteReport\Tests;

use Workable\SiteReport\Facades\SiteReport;
use Workable\SiteReport\SiteReportServiceProvider;

use PHPUnit\Framework\TestCase;

class SiteReportTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [SiteReportServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'site-report' => SiteReport::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
