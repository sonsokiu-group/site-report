<?php

if (!function_exists("__layout_master_core"))
{
    function __layout_master_core($name = "")
    {
        return config("packages.site-report.config.layout");
    }
}

if (!function_exists("__render_chart_item"))
{
    function __render_chart_item($config)
    {
        list($jsonFile, $key) = explode("@", $config);
        $pathRoot = "platform/packages/site-report/src/Database/json/";
        $json     = file_get_contents(base_path($pathRoot . $jsonFile . ".json"));
        $json     = json_decode($json, true);
        $data     = $json[$key];

        return '
            <div class="render_chart" data-chart="' . htmlspecialchars(json_encode($data)) . '">
            </div>
        ';
    }
}

if (!function_exists('__render_chart_page'))
{
    function __render_chart_page($fileJson)
    {
        $pathRoot = "platform/packages/site-report/src/Database/json/";
        $json     = file_get_contents(base_path($pathRoot . $fileJson . ".json"));
        $json     = json_decode($json, true);
        $html     = "";

        foreach ($json as $key => $value)
        {
            $data    = $value;
            $domHtml = '
                <div class="render_chart" data-chart="' . htmlspecialchars(json_encode($data)) . '">
                </div>
            ';
            $html    .= $domHtml;
        }

        return $html;
    }
}


